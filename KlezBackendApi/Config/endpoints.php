<?php

################## ACL

$acl = [];
$acl['session'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'session'
];

$acl['no_session'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'no_session'
];

$acl['none'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'none'
];

################### Endpoints

$config = [];

$config['Endpoint.Config'] = [
    'component' => 'ConfigComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'fetch',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.Login'] = [
    'component' => 'PagesComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'login',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'Admin',
    'acl' => $acl['no_session']
];

$config['Endpoint.LoginConfig'] = [
    'component' => 'ConfigComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'fetchLogin',
    'input' => 'blank',
    'verb' => [ 'get' ],
    'auth' => 'Admin',
    'acl' => $acl['no_session']
];

$config['Endpoint.Home'] = [
    'component' => 'PagesComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'home',
    'input' => 'blank',
    'verb' => [ 'get' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.Logout'] = [
    'component' => 'PagesComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'logout',
    'input' => 'blank',
    'verb' => [ 'get' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.Profile'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'profile',
    'input' => 'json_payload',
    'verb' => [ 'get','post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.ProfileEdit'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'profile_edit',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

########################################################### KLEZKAFFOLD

$config['Endpoint.Dashboard'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'dashboard',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.Show'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'show',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.Detail'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'detail',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.RequestForm'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'requestForm',
    'input' => 'json_payload',
    'verb' => [ 'get','post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.Add'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'add',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.Edit'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'edit',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.RequestDelete'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'requestDelete',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.Delete'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'delete',
    'input' => 'json_payload',
    'verb' => [ 'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.Report'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'report',
    'input' => 'json_payload',
    'verb' => [ 'get', 'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.Image'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'image',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.ProfilePhoto'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'profile_photo',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.Massive'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'massive',
    'input' => 'json_payload',
    'verb' => [ 'post','put' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];

$config['Endpoint.RequestMassive'] = [
    'component' => 'KlezkaffoldApiComponent',
    'path' => 'KlezBackendApi.Controller/Endpoint',
    'method' => 'requestMassive',
    'input' => 'json_payload',
    'verb' => [ 'get', 'post' ],
    'auth' => 'Admin',
    'acl' => $acl['session']
];