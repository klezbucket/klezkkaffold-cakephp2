<div class="input-group m-t-10">
    <input 
        type="password"
        placeholder="<?=$label?>" 
        class="form-control"
        value="<?=$value?>"
        name="<?=$field?>">
    
    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
</div>