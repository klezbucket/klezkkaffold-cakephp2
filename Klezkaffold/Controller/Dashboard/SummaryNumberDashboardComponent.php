<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class SummaryNumberDashboardComponent extends KlezkaffoldComponent{
    private $number = 0;
    
    public function output() {
        return [
            'number' => $this->number,
        ];
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
    }
    
    public function process(){
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
        
        if(empty($this->query) === false){
        	$data = $this->getModel()->rawFind('first',$this->query);

        	if(isset($data[0]['number'])){
            	$this->number = $data[0]['number'];
        	}
        }
    }
    
    public function parseConfig($config){
        if(isset($config['type']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:type> in Klezkaffold Config");
        }

        if(isset($config['data']['query']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:data.query> in Klezkaffold Config");
        }
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        $this->query = $config['data']['query'];
        $this->loadModel($config['data']);
    }
}