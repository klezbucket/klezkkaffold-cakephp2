<?php

App::uses('BaseAclComponent', 'KlezApi.Controller/Acl');
App::uses('BaseComponent', 'KlezApi.Controller/Component');

class AclComponent extends BaseComponent {
    private $Controller;
    private $Component;
    private $Config;
    
    public function initialize(\Controller $controller) {
        $this->Controller = $controller;
        $this->initializeLog();
        
        return parent::initialize($controller);
    }
    
    private function initializeLog(){
        $file = 'klezauth';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'auth' ],
            'file' => $file
        ));    
    }
    
    private function raiseForbiddenException($message){
        $this->logacl($message);
        throw new ForbiddenException($message);
    }
    
    private function raiseConfigureException($message){
        $this->logacl($message);
        throw new ConfigureException($message);
    }
    
    private function logacl($message){
        $fullMessage = "[{$this->getLogHash()}] {$message}";
        CakeLog::write('debug', $fullMessage, 'auth');
    }
    
    public function load($conf){
        if(isset($conf['component']) === false){
            $this->raiseConfigureException("No Conf<Acl.component> in Acl Config");
        }
        
        if(isset($conf['path']) === false){
            $this->raiseConfigureException("No Conf<Acl.path> in Acl Config");
        }
        
        if(isset($conf['control']) === false){
            $this->raiseConfigureException("No Conf<Acl.control> in Acl Config");
        }
        
        $this->Config = $conf;
        $this->loadComponent();
    }
    
    private function loadComponent(){
        $component = $this->Config['component'];
        $path = $this->Config['path'];
        
        App::uses($component, $path);
        
        if(class_exists($component) === false){
            $this->raiseConfigureException("No Class<$component> for Acl");
        }

        $collection = new ComponentCollection();
        $this->Component = new $component($collection);
        
        if(($this->Component instanceof BaseAclComponent) === false){
            $this->raiseConfigureException("Not an BaseAclComponent<Class:$component> in Acl Config");
        }
        
        $this->Component->initialize($this->Controller);
    }
    
    public function aclFiltersData($filters){
        return $this->Component->aclFiltersData($filters);
    }
    
    public function filterEndpoint($Component,$input,$config){
        $this->Component->loadFilters();
        $endpoint = $this->Controller->params['endpoint'];
        $endpointCase = Inflector::camelize($endpoint);
        $key = "Filter.{$endpointCase}";        
        $filterConf = Configure::read($key);
        
        if(is_null($filterConf)){
            $this->logacl('Acl<endpointFilter:null>');
            return;
        }
        
        if(isset($filterConf['filters']['data'])){
            if($this->Component->aclFiltersData($filterConf['filters']) === false){
                $this->raiseForbiddenException('Acl<forbidden:dataFilter>');
            }
        }
        
        if(isset($filterConf['filters']['custom'])){
            if(is_array($filterConf['filters']['custom']) === false){
                $filterConf['filters']['custom'] = [ $filterConf['filters']['custom'] ];
            }
            
            foreach($filterConf['filters']['custom'] as $method){
                if(method_exists($Component, $method) === false){
                    $class = get_class($Component);
                    $this->raiseConfigureException("Method not found<Class:$class,method:$method> in Acl Config");

                }
                
                if($Component->{$method}($input,$config) === false){
                    $this->raiseForbiddenException("Acl<forbidden:customFilter,method:$method>");
                }
            }
        }
    }
    
    public function control(){        
        $conf = $this->Config;
        $control = $conf['control'];
        $this->logacl("Acl<control:$control>");
        $this->Component->control($control);
    }
    
    public function isUrlAllowed($url) {
        return $this->Component->isUrlAllowed($url);
    }
}