<a class="dashboard-link" href="<?=$this->Dashboard->feedUrl('url')?>">
    <div class="col-lg-3 col-md-6">
        <div class="card-box widget-user">
            <div class="text-center">
                <h2 class="text-<?=$this->Dashboard->feed('class')?>">
                    <i class="fa fa-<?=$this->Dashboard->feed('icon')?>"></i>
                </h2>
                <h5><?=$this->Dashboard->feed('title')?></h5>
            </div>
        </div>
    </div>
</a>