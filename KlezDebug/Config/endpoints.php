<?php

################## ACL

$acl = [];
$acl['session'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'session'
];

$acl['no_session'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'no_session'
];

$acl['none'] = [
    'component' => 'BackendAclComponent',
    'path' => 'KlezBackendApi.Controller/Acl',
    'control' => 'none'
];

################### Endpoints

$config = [];

$config['Endpoint.DebugConsole'] = [
    'component' => 'KlezDebugConsoleComponent',
    'path' => 'KlezDebug.Controller/Endpoint',
    'method' => 'main',
    'input' => 'json_payload',
    'verb' => [ 'get','post' ],
    'auth' => 'Admin',
    'acl' => $acl[ 'session' ],
];

$config['Endpoint.DebugFacebook'] = [
    'component' => 'KlezDebugFacebookComponent',
    'path' => 'KlezDebug.Controller/Endpoint',
    'method' => 'main',
    'input' => 'json_payload',
    'verb' => [ 'get' ],
    'auth' => 'Admin',
    'acl' => $acl[ 'session' ],
];

$config['Endpoint.DebugFcm'] = [
    'component' => 'KlezDebugFcmComponent',
    'path' => 'KlezDebug.Controller/Endpoint',
    'method' => 'main',
    'input' => 'json_payload',
    'verb' => [ 'get','post' ],
    'auth' => 'Admin',
    'acl' => $acl[ 'session' ],
];