<?php

App::uses('KlezBackendHelper', 'KlezBackend.View/Helper');

if( !defined('KLEZKAFFOLD_DATA')){
    define('KLEZKAFFOLD_DATA','klezkaffold_data_' . substr(md5(uniqid()),0,8));
}

abstract class KlezkaffoldHelper extends KlezBackendHelper {
    public function provideVarname() {
        return KLEZKAFFOLD_DATA;
    }
    
    protected function isReadable($meta){
        $readable = true;
        
        if(isset($meta['readable'])){
            $readable = $meta['readable'];
        }
        
        if($readable === true && isset($meta['hidden'])){
            $readable = ! $meta['hidden'];
        }
        
        return $readable;
    }
    
    protected function isGroup($meta){
        $grouped = false;
        
        if(isset($meta['group'])){
            $grouped = is_array($meta['group']);
        }
        
        return $grouped;
    }
    
    
    protected function isListable($meta){
        $listable = true;
        
        if(isset($meta['listable'])){
            $listable = $meta['listable'];
        }
        
        if($listable === true && isset($meta['hidden'])){
            $listable = ! $meta['hidden'];
        }
        
        return $listable;
    }
    
    protected function element($name,$vars = []){
        $module = str_replace('Helper','',get_class($this));
        return $this->_View->element("Klezkaffold.{$module}/{$name}",$vars);
    }
    
    public function dataLinks($metalinks){
        foreach($metalinks as $metalink){
            echo $this->_View->element('Klezkaffold.Scaffold/data_link',$metalink);      
        }
    }
    
    protected function resolvMetalinks($links){
        if(empty($links)){
            return;
        }
        
        $current = $this->resolvCurrentUrl();
        $metalinks = [];
        
        foreach($links as $link){
            $controller = $link['controller'];
            $action = $link['action'];
            $sitemap = $this->resolvSitemap($controller, $action);
            $url = $this->resolvUrl($link);
            
            if(strcmp($current, $url) === 0){
                continue;
            }
            
            $metalinks[] = [
                'title' => $sitemap['h1'],
                'icon' => $sitemap['icon'],
                'url' => $url
            ];
        }
        
        return $metalinks;
    }
    
    public function resolvWeight($meta){
        if(isset($meta['weight'])){
            return (int) $meta['weight'];
        }
        
        return 4;
    }
    
    public function actions($links){
        $metalinks = $this->resolvMetalinks($links);
        
        if(empty($metalinks) === false){
            echo $this->element('data_links', [
                'metalinks' => $metalinks
            ]);      
        }
    }
    
    public function feed($path,$throwable = true,$cache = false){
        return parent::feed($path, $throwable, $cache);
    }
    
    public function feedUrl($path,$throwable = true,$full = true,$cache = false){
        return parent::feedUrl($path, $throwable, $full, $cache);
    }
    
    public function resolv($meta,$key){
        if(isset($meta[$key]) === false){
            $this->raiseInternalServerError("Invalid Schema<missing.key:$key> in KlezkaffoldHelper");
        }
        
        $value = $meta[$key];
        return $value;
    }
    
    public function resolvLabel($meta){
        if(isset($meta['label']) === false){
            $this->raiseInternalServerError("Invalid Schema<missing.key:label> in KlezkaffoldHelper");
        }
        
        $label = $meta['label'];
        return $label;
    }
    
    public function resolvValue($field,$meta,$row){
        $value = null;
        $type = null;
        
        if(isset($meta['type']) === true){
            $type = $meta['type'];
        }
        
        if(is_null($type) === false){
            $value = $this->resolvValueByType($meta,$row,$field);
        }
        
        if(is_null($value) === true){
            $value = $this->resolvNullValue($meta);            
        }
        
        if($value === ''){
            $value = $this->resolvBlankValue($meta);            
        }
        
        return $value;
    }
    
    public function resolvBlankValue($meta){
        $value = '';
        
        if(isset($meta['blank'])){
            $value = $meta['blank'];
        }
        
        return $value;
    }
    
    
    public function resolvInputNullValue($meta){
        $value = '';
        
        if(isset($meta['default'])){
            $value = $meta['default'];
        }
        
        return $value;
    }
    
    public function resolvNullValue($meta){
        $value = 'No definido';
        
        if(isset($meta['null'])){
            $value = $meta['null'];
        }
        
        return $value;
    }
    
    public function resolvInputValueByType($meta,$raw){
        $type = $meta['type'];

        switch($type){
            case 'text':
                return $this->resolvInputTextValue($meta,$raw);
            case 'datetime':
                return $this->resolvInputDatetimeValue($meta,$raw);
            case 'options':
                return $this->resolvInputOptionsValue($meta,$raw);
            case 'int':
                return $this->resolvInputIntValue($meta,$raw);
            case 'double':
                return $this->resolvInputDoubleValue($meta,$raw);
            case 'boolean':
                return $this->resolvInputBooleanValue($meta,$raw);
            case 'foreign':
                return $this->resolvInputForeignValue($meta,$raw);
            case 'date':
                return $this->resolvInputDateValue($meta,$raw);
            case 'gmap':
                return $this->resolvInputGmapValue($meta,$raw);
            case 'time':
                return $this->resolvInputTimeValue($meta,$raw);
        }
                    
        $this->raiseInternalServerError("Invalid Schema<invalid.type:{$type}> in KlezkaffoldHelper");
    }
    
    public function resolvInputGmapValue($meta,$raw){
        return $raw;
    }
    
    public function resolvValueByType($meta,$row,$field){
        $type = $meta['type'];
        
        if($type === 'gmap'){
            return $this->resolvGmapValue($meta,$row);
        }
        
        if($type === 'foreign'){
            return $this->resolvForeignValue($row,$field);
        }
        
        if($type === 'file'){
            return $this->resolvFileValue($meta,$row,$field);
        }
        
        $raw = $this->resolvRawValue($field, $row);
        
        if(is_null($raw) === true){
            return null;
        }

        switch($type){
            case 'text':
                return $this->resolvTextValue($meta,$raw);
            case 'datetime':
                return $this->resolvDatetimeValue($meta,$raw);
            case 'options':
                return $this->resolvOptionsValue($meta,$raw);
            case 'int':
                return $this->resolvIntValue($meta,$raw);
            case 'double':
                return $this->resolvDoubleValue($meta,$raw);
            case 'boolean':
                return $this->resolvBooleanValue($meta,$raw);
            case 'date':
                return $this->resolvDateValue($meta,$raw);
            case 'float':
                return $this->resolvFloatValue($meta,$raw);
            case 'gmap':
                return $this->resolvGmapValue($meta,$row);
            case 'time':
                return $this->resolvTimeValue($meta,$raw);
        }
                    
        $this->raiseInternalServerError("Invalid Schema<invalid.type:{$type}> in KlezkaffoldHelper");
    }
    
    public function resolvGmapValue($meta,$row){
        $fields = $this->resolvGmapFields($meta);
        $zoom = $this->resolvZoom($meta);
        $latfield = $fields[0];
        $lonfield = $fields[1];
        $lat = '';
        $lon = '';
        
        if(isset($row[$latfield])){
            $lat = $row[$latfield];
        }
        else{
            return null;
        }
        
        if(isset($row[$lonfield])){
            $lon = $row[$lonfield];
        }
        else{
            return null;
        }
        
        return "<a target='_blank' href='http://maps.google.com/maps?z={$zoom}&q=loc:{$lat}+{$lon}'>[ <i class='fa fa-map'></i> ]</a>";
    }
    
    public function resolvFloatValue($meta,$raw){
        return $raw;
    }
    
    public function resolvGmapFields($meta){
        if(isset($meta['fields'][0]) === false){
            $this->raiseInternalServerError('Invalid Schema<missing.key:fields[0]>');
        }
        
        if(isset($meta['fields'][1]) === false){
            $this->raiseInternalServerError('Invalid Schema<missing.key:fields[1]>');
        }
        
        return $meta['fields'];
    }
    
    public function resolvKey($meta){
        if(isset($meta['key']) === false){
            $this->raiseInternalServerError('Invalid Schema<missing.key:key>');
        }    
        
        return $meta['key'];
    }
    
    public function resolvZoom($meta){
        if(isset($meta['zoom']) === false){
            $this->raiseInternalServerError('Invalid Schema<missing.key:zoom>');
        }    
        
        return $meta['zoom'];
    }
    
    public function resolvGmapDefaults($meta){
        if(isset($meta['defaults'][0]) === false){
            $this->raiseInternalServerError('Invalid Schema<missing.key:defaults[0]>');
        }
        
        if(isset($meta['defaults'][1]) === false){
            $this->raiseInternalServerError('Invalid Schema<missing.key:defaults[1]>');
        }
        
        return $meta['defaults'];
    }
    
    
    public function resolvFileValue($meta,$row,$field){
        $value = null;
        
        if(isset($row['***files***'][$field])){
            $metadata = $row['***files***'][$field];
            
            if($metadata['exists']){
                return $this->resolvFileUrl($meta,$metadata['url']);
            }
        }
        
        return $value;
    }
    
    private function resolvFileUrl($meta,$url){
        $label = $this->resolvFileUrlLabel($meta);
        return "<a target='_blank' href='{$url}'>[ {$label} ]</a>";
    }
    
    private function resolvFileUrlLabel($meta){
        $value = '<i class="fa fa-download"></i>';
        
        if(isset($meta['url-label'])){
            $value = $meta['url-label'];
        }
        
        return $value;
    }
    
    public function resolvInputTextValue($meta,$raw){
        return $raw;
    }
    
    public function resolvInputDateValue($meta,$raw){
        if($raw == ''){
            return '';
        }
        
        $time = strtotime(str_replace('/', '-', $raw));
        
        if($time === false){
            return '';
        }
        
        $date = date('d/m/Y',$time);
        return $date;
    }
    
    public function resolvInputDatetimeValue($meta,$raw){
        $time = strtotime($raw);
        $date = date('d/m/Y H:i:s',$time);
        return $date;
    }
    
    public function resolvForeignValue($row,$field){
        $value = null;
        
        if(isset($row['***foreign***'][$field])){
            $value = $row['***foreign***'][$field];
        }
        
        return $value;
    }
    
    public function resolvInputBooleanValue($meta,$raw){
        $intBool = (int) $raw;
        return $intBool;
    }
    
    public function resolvInputForeignValue($meta,$raw){
        switch($meta['subtype']){
            case 'habtm':
                $csv = (string) @$raw[0];
                return $csv;
            case 'tags':
                $csv = (string) @$raw[0];
                return $csv;
        }
        
        $int = (int) $raw;
        return $int;
    }
    
    public function resolvInputOptionsValue($meta,$raw){
        return $raw;
    }
    
    public function resolvInputIntValue($meta,$raw){
        return $raw;
    }
    
    public function resolvInputTimeValue($meta,$raw){
        return $raw;
    }
    
    public function resolvInputDoubleValue($meta,$raw){
        return $raw;
    }

    public function resolvTextValue($meta,$raw){
        return nl2br($raw);
    }
    
    public function resolvDatetimeValue($meta,$raw){
        $time = strtotime($raw);
        $date = strtoupper(strftime('%d/%b/%Y %H:%M',$time));
        return $date;
    }
    
    public function resolvDateValue($meta,$raw){
        $time = strtotime($raw);
        
        if($time === false){
            return $raw;
        }
        
        $date = strtoupper(strftime('%d/%b/%Y',$time));
        return $date;
    }
    
    public function resolvBooleanValue($meta,$raw){
        $intBool = (int) $raw;
        
        if(isset($meta['boolean'][$intBool]) === false){
            $this->raiseInternalServerError("Invalid Schema<missing:options.$intBool}> in KlezkaffoldHelper");
        }
        
        $label = $meta['boolean'][$intBool];
        return $label;
    }
    
    public function resolvOptionsValue($meta,$raw){
        if(isset($meta['options'][$raw]) === false){
            $this->raiseInternalServerError("Invalid Schema<missing:options.$raw}> in KlezkaffoldHelper");
        }
        
        $label = $meta['options'][$raw];
        return $label;
    }
    
    public function resolvIntValue($meta,$raw){
        if(is_numeric($raw) === false){
            return $raw;
        }
        
        return number_format($raw, 0, ',', '.');
    }
    
    public function resolvTimeValue($meta,$raw){        
        $split = preg_split('/:/',$raw);
        $hh = (int) $split[0];
        $mm = (int) $split[1];
        
        return
            str_pad($hh, 2, 0, STR_PAD_LEFT) .
                ':' .
            str_pad($mm, 2, 0, STR_PAD_LEFT);
    }
    
    public function resolvDoubleValue($meta,$raw){
        return floatval($raw);
    }
    
    public function resolvRawValue($field,$row){
        if(array_key_exists($field,$row) === false){
            $this->raiseInternalServerError("Invalid Data<missing.field:{$field}> in KlezkaffoldHelper");
        }
        
        $raw = $row[$field];
        return $raw;
    }
    
    public function buildUrl($purl){
        if(isset($purl['scheme']) === false){
            $purl['scheme'] = 'http';
        }
        
        if(isset($purl['port']) === true){
            $purl['port'] = ':' . $purl['port'];
        }
        else{
            $purl['port'] = '';
        }
        
        if(isset($purl['query']) === true){
            $purl['query'] = '?' . http_build_query($purl['query']);
        }
        else{
            $purl['query'] = '';
        }
                
        $scheme = $purl['scheme'];
        $host = $purl['host'];
        $path = $purl['path'];
        $port = $purl['port'];
        $query = $purl['query'];
        
        $url = "{$scheme}://{$host}{$port}{$path}{$query}";
        return $url;
    }
    
    public function resolvCurrentUrl(){
        $here = $this->_View->here;
        $url = Router::url($here,true);
        return $url;
    }
    
    public function resolvSitemap($controller,$action){
        $sitemap = Configure::read("Sitemap.{$controller}.{$action}");

        if(is_null($sitemap)){
            $this->raiseInternalServerError("No Conf<Sitemap.{$controller}.{$action}> in Show");
        }

        return $sitemap;
    }
    
    public function resolvUrl($link){
        $url = Router::url($link,true);
        return $url;
    }
    
    public function resolvType($meta){
        if(isset($meta['type']) === false){
            return null;
        }
        
        return $meta['type'];
    }
    
    public function resolvSubtype($meta){
        if(isset($meta['subtype']) === false){
            return null;
        }
        
        return $meta['subtype'];
    }
    
    public function resolvPlaceholder($schema){
        if(isset($schema['placeholder']) === false){
            return $this->resolvLabel($schema);
        }
        
        $placeholder = $schema['placeholder'];
        return $placeholder;
    }
    
    public function resolvOptions($schema){
        if(isset($schema['options']) === false){
            $this->raiseInternalServerError('Invalid Schema<missing.key:options>');
        }
        
        $options = $schema['options'];
        return $options;
    }
    
    protected function resolvAutocompleteOptions($options,$meta){
        if(isset($meta['placeholder']) === false){
            $this->raiseInternalServerError('Invalid Schema<missing.key:placeholder>');
        }
        
        $placeholder = $meta['placeholder'];
        $options = [ '' => $placeholder ] + $options;
        return $options;
    }
    
    public function resolvInputValue($field, $meta, $data){
        $raw = null;
        
        if(isset($data[$field])){
            $raw = $data[$field];
        }
        
        $value = $raw;
        $type = null;
        $null = false;
        
        if(is_null($raw)){
            $null = true;
        }
        else if(isset($meta['type'])){
            $type = $meta['type'];
        }
        
        if(is_null($type) === false){
            $value = $this->resolvInputValueByType($meta,$raw);
        }
        else if($null){
            $value = $this->resolvInputNullValue($meta);            
        }
        
        return $value;
    }
}