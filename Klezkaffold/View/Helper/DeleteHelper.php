<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class DeleteHelper extends KlezkaffoldHelper {
    public function deploy($config){
        echo $this->element('main',[
            'config' => $config,
        ]);
    }
    
    public function submit(){
        echo $this->element('submit');
    }
    
    public function main($config){
        if(isset($config['data']) === false){
            $this->raiseInternalServerError("No Conf<Delete:data> in DeleteHelper");
        }
        
        if(isset($config['schema']) === false){
            $this->raiseInternalServerError("No Conf<Delete:schema> in DeleteHelper");
        }
        
        echo $this->element('delete', $config);
    }
    
    public function dataCards($schema,$data){
        foreach($schema as $field => $meta){
            if($this->isReadable($meta) === false){
                continue;
            }
            
            $value = $this->resolvValue($field, $meta, $data);
            $label = $this->resolvLabel($meta);
            $weight = $this->resolvWeight($meta);
            
            echo $this->element('data_card',[
                'meta' => $meta,
                'value' => $value,
                'label' => $label,
                'weight' => $weight
            ]);            
        }
    }

    public function provideLogtag() {
        return "Delete";
    }
}