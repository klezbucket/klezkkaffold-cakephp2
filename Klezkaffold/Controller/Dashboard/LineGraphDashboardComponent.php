<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class LineGraphDashboardComponent extends KlezkaffoldComponent{
    private $series = [];
    private $data = [];
    
    public function output() {
        return $this->data;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
    }
    
    public function process(){
        $this->data['series'] = [];
        $this->data['labels'] = $this->labels;
        
        foreach($this->series as $name => $serie){
            if(isset($serie['data']) === false){
                $this->raiseConfigureException("No Conf <Serie:data> in Klezkaffold Config");
            }
            
            $this->loadModel($serie['data']);
            
            $this->data['series'][$name] = [
                'label' => $serie['label'],
                'points' => []
            ];
            
            foreach($serie['points'] as $point){
                $this->data['series'][$name]['points'][] = $this->processPoint($point);
            }
        }
    }
    
    private function processPoint($point){
        if(isset($point['query']) === false){
            $this->raiseConfigureException("No Conf <Serie:query> in Klezkaffold Config");
        }
        
        if(isset($point['prequery']) === true){
            $this->prequery = $point['prequery'];
        }
        
        $this->query = $point['query'];
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
        
        return (int) $this->getModel()->find('count',$this->query);
    }
    
    public function parseConfig($config){
        if(isset($config['data']['series']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:data.series> in Klezkaffold Config");
        }
        
        if(isset($config['data']['labels']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:data.labels> in Klezkaffold Config");
        }
        
        $this->series = $config['data']['series'];
        $this->labels = $config['data']['labels'];
    }
}