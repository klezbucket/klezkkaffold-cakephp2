<div align="center">
    <input 
        type="checkbox" 
        name="<?=$field?>"
        <?=$checked?>
        data-plugin="switchery"
        data-color="#00b19d"/>
</div>

<script>
    $(function(){
        var field = <?=  json_encode($field); ?>;
        var label = <?=  json_encode($label); ?>;
        var boolean = <?=  json_encode($boolean); ?>;
        var input = $('input[name="' + field + '"]');
        var title = $('#holder_for_' + field + ' h3');
        
        input.on('change',function(){
            var checked = $(this).is(':checked');
            var caption = label + ': ';
            
            if(checked){
                caption += boolean[1];
            }
            else{
                caption += boolean[0];
            }
            
            title.html(caption);
        });
        
        input.trigger('change');
    });
</script>