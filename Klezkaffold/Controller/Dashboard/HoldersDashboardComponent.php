<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class HoldersDashboardComponent extends KlezkaffoldComponent{
    private $data = [];
    private $url = [];
    
    public function output() {
        return $this->data;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
    }
    
    public function process(){
        if($this->isUrlAllowed($this->url) === false){
            $this->data = [];
        }
    }
    
    public function parseConfig($config){
        if(isset($config['url']) === false){
            $this->raiseConfigureException("No Conf <Dashboard:url> in Klezkaffold Config");
        }
        
        $this->url = $config['url'];
        $this->data = $config;
    }
}