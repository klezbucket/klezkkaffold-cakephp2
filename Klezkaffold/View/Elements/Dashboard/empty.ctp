<div class="col-sm-12">
    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info fade in m-b-0">
                    <h4><?=$this->Backend->feed('empty.title')?></h4>
                    <p>
                        <?=$this->Backend->feed('empty.text')?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>