<?php

App::uses('PHPExcel', 'KlezData.phpexcel/Classes');

class ExcelComponent extends Component{
    private $PHPExcel;
    private $SheetPointer;
    private $Sheet;
    private $Styles = [];
    private $I = 1;
    private $J = 1;
    
    public function initialize(\Controller $controller) {
        $this->PHPExcel = new PHPExcel();
        $this->SheetPointer = $this->PHPExcel->getActiveSheetIndex();
        $random = md5(time() . 'ExcelComponent') . '.xlsx';
        $this->File = "/tmp/{$random}";
        
        return parent::initialize($controller);
    }

    public function getHighestRow(){
        $sheet = $this->PHPExcel->getSheet($this->SheetPointer);
        return $sheet->getHighestRow();
    }

    public function getHighestColumn(){
        $sheet = $this->PHPExcel->getSheet($this->SheetPointer);
        return $sheet->getHighestColumn();
    }

    public function loadFromString($data = null){
        file_put_contents($this->File, $data);
        $this->PHPExcel = PHPExcel_IOFactory::load($this->File);
        $this->Sheet = $this->PHPExcel->getActiveSheet();
        $this->SheetPointer = $this->PHPExcel->getActiveSheetIndex();
    }
    
    public function output(){
        $this->PHPExcel->setActiveSheetIndex(0);
        
        $objWriter = PHPExcel_IOFactory::createWriter($this->PHPExcel, 'Excel2007');
        $objWriter->save($this->File);
        $output = file_get_contents($this->File);
        $type = mime_content_type($this->File);
        unlink($this->File);
        
        return [
            'type' => $type,
            'blob' => $output,
            'size' => strlen($output)
        ];
    }
    
    public function newSheet(){
        $this->PHPExcel->createSheet();
    }
    
    public function sheet($name,$pointer = null){
        $pointer = $this->resolvSheetPointer($pointer);
        $this->PHPExcel->setActiveSheetIndex($pointer);
        $this->Sheet = $this->PHPExcel->getActiveSheet();
        $this->Sheet->setTitle($name);
        $this->I = 1;
        $this->J = 1;
    }
    
    private function resolvSheetPointer($pointer = null){
        if(is_null($pointer)){
            $pointer = $this->SheetPointer;
            $this->SheetPointer++;  
        }
        
        return $pointer;
    }
    
    private function resolvPos($i = null,$j = null){
        if(is_null($i) === true){
            $i = $this->I;
        }
        
        if(is_null($j) === true){
            $j = $this->J;
        }
        
        return $this->getPos($i,$j);
    }
    
    public function align($h = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, $v = PHPExcel_Style_Alignment::VERTICAL_CENTER, $i = null,$j = null){
        $pos = $this->resolvPos($i, $j);
        $this->Sheet->getStyle($pos)->getAlignment()->setHorizontal($h)->setVertical($v);
    }
    
    public function style($style, $i = null,$j = null){
        $pos = $this->resolvPos($i, $j);
        $starray = $this->getStyle($style);
        $this->Sheet->getStyle($pos)->applyFromArray($starray);
    }
    
    public function columnFit($autofit = true, $j = null){
        if(is_null($j) === true){
            $j = $this->J;
        }
        
        $col = $this->getCol($j);
        $this->Sheet->getColumnDimension($col)->setAutoSize($autofit); 
    }
    
    public function cell($text, $i = null,$j = null){  
        $pos = $this->resolvPos($i, $j);
        $this->Sheet->SetCellValue($pos, $text);
    }
    
    private function resolvCellLabel($meta){
        if(isset($meta['label'])){
            return $meta['label'];
        }

        return '';
    }
    
    private function resolvCellVisibility($meta){
        if(@$meta['readable'] === false){
            return false;
        }

        if(@$meta['listable'] === false){
            return false;
        }
        
        return true;
    }

    public function read($row,$col){
        $cell = $this->Sheet->getCellByColumnAndRow($col, $row);
        $val = $cell->getValue();

        return $val;
    }
    
    public function table($schema,$data){
        foreach($schema as $meta){
            if($this->resolvCellVisibility($meta) === false){
                continue;
            }
            
            $label = $this->resolvCellLabel($meta);
            
            $this->cell($label);
            $this->style('head');
            $this->columnFit(true);
            $this->align(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->colJump();
        }
        
        if(empty($data) === true){
            return;
        }
        
        foreach($data as $row){
            $this->rowJump();
            $this->col();
            
            foreach($schema as $field => $meta){
                if($this->resolvCellVisibility($meta) === false){
                    continue;
                }
                
                $value = $this->resolvValue($row,$field);
                $this->cell($value);
                $this->style('value');
                $this->columnFit(true);
                $this->colJump();
            }
        }
    }
    
    private function resolvValue($row,$field){
        if(isset($row['***foreign***'][$field])){
            return $row['***foreign***'][$field];
        }
        
        return $row[$field];
    }
    
    public function col($val = 1){
        $this->J = $val;
    }
    
    public function row($val = 1){
        $this->I = $val;
    }
    
    public function colJump($by = 1){
        $this->J += $by;
    }
    
    public function rowJump($by = 1){
        $this->I += $by;
    }
    
    public function getStyle($key){
        if(isset($this->Styles[$key]) === false){
            return [];
        }
        
        return $this->Styles[$key];
    }
    
    public function setStyles($styles){
        $this->Styles = $styles;
    }
    
    public function setStyle($key,$style){
        $this->Styles[$key] = $style;
    }
    
    private function getPos($row,$col){
        $letter_col = $this->getCol($col);
        
        return "{$letter_col}{$row}";
    }
    
    private function getCol($col){
        $letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $count = strlen($letters);
        $str = '';     
        
        while($col > $count){
            $mod = $col % $count;
            $col = (int)($col / $count);
            $str = substr($letters,$mod - 1,1) . $str;
        }
        
        $str = substr($letters,$col - 1,1). $str;        
        return $str;
    }
}