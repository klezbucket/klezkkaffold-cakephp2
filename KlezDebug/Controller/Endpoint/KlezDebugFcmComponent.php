<?php

App::uses('FcmComponent', 'KlezMobile.Controller/Component');
App::uses('BackendComponent', 'KlezBackendApi.Controller/Endpoint');
App::uses('Controller', 'Controller');

class KlezDebugFcmComponent extends BackendComponent{
    private $Fcm;
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
    }
    
    public function main_get($payload){
        $serverKey = Configure::read('Fcm.server_key');
        $to = Configure::read('Fcm.to');
        $this->write('serverkey', $serverKey);
        $this->write('to', $to);
    }
    public function main_post($payload){
        $this->setSimple(true);
        $collection = new ComponentCollection();
        $this->Fcm = new FcmComponent($collection);
        $this->Fcm->push($payload['serverkey'],$payload['payload']);
        $payloadResponse = $this->Fcm->getResponse('json');
        
        $response = [
            'response' => [
                'payload' => $payloadResponse,
                'headers' => $this->Fcm->getResponseHeaders(),
                'status' => $this->Fcm->getStatusCode(),
            ],
            'request' => [
                'payload' => $payload['payload'],
                'verb' => $this->Fcm->getVerb(),
                'url' => $this->Fcm->getUrl(),
                'headers' => $this->Fcm->getHeaders()
            ]
        ];
        
        $this->overwrite($response);
    }

    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        throw new NotImplementedException("Unknown format:$format");
    }
}