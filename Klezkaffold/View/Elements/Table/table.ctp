<div class="col-sm-12">
    <div class="card-box table-responsive">
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">            
            <?php $this->Table->tableHead($schema); ?>
            <?php $this->Table->tableBody($schema,$data); ?>
        </table>
    </div>
</div>