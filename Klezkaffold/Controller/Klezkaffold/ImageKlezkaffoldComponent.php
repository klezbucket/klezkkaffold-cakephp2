<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class ImageKlezkaffoldComponent extends KlezkaffoldComponent{
    private $schema = [];
    private $data = [];
    private $notFound = false;
    private $field;
    private $blob;
    private $type;
    private $file;
    private $size;
    private $name;
    private $timestamp;
    private $format = null;
    private $cache = false;
    private $since;
    private $params;
    
    public function output() {
        $data = [
            'blob' => $this->blob,
            'type' => $this->type,
            'file' => $this->file,
            'size' => $this->size,
            'timestamp' => $this->timestamp
        ];
        
        if($this->cache === true){
            $data['exception'] = 304;
        }
        
        if($this->notFound === true){
            $data['exception'] = 404;
        }
        
        return $data;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
        $this->parsePayload($payload);
    }
    
    public function cache(){
        $lastModified = gmdate('D, d M Y H:i:s ', $this->timestamp) . 'GMT';
        return strcmp($lastModified, $this->since) === 0;
    }
    
    public function process(){
        $this->resolvQuery();
        $this->resolvData();
        
        if($this->cache()){
            $this->cache = true;
            return;
        }
        
        $this->resolvBinary();
    }
    
    private function resolvQuery(){
        $this->query = [];
        $alias = $this->getModel()->alias;
        $pkey = $this->getModel()->primaryKey;
        
        $conditions = [
            "{$alias}.{$pkey}" => $this->id
        ];
            
        $this->query['conditions'] = $conditions;
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
    }
    
    private function resolvData(){
        if(is_null($this->query)){
            $this->notFound = true;
            return;
        }
        
        $this->data = $this->getModel()->findReadable('first',$this->query,true);
        $this->schema = $this->getModel()->provideReadableSchema();
        
        if(empty($this->data)){
            $this->notFound = true;
            return;
        }
        
        $this->timestamp = $this->data[$this->field];
    }
    
    private function resolvBinary(){
        $data = $this->getModel()->resolvBinary($this->field);
        
        if($data === false){
            $this->notFound = true;
            return;
        }
        
        $blob = $data['blob'];
        $this->processBlob($blob);
        
        if(is_null($this->format) === false){
            $this->resolvFormat();
        }
    }
    
    private function processBlob($blob){
        $this->blob = $this->transformBlob($blob);
        $this->type = $this->resolvImageType();
        $this->file = $this->resolvImageFile();
        $this->size = $this->resolvImageSize();
    }
    
    private function transformBlob($blob){
        if(isset($this->params['transform'])){
            $transform = $this->params['transform'];
            
            $class = Inflector::camelize($transform) . 'ImageTransformComponent';
            $path = 'Klezkaffold.Controller/ImageTransform';

            App::uses($class, $path);

            if(class_exists($class) === false){
                $this->raiseConfigureException("No Such Class <class:$class,path:$path> in ImageTransform Config");
            }
            
            $Transformer = new $class(new \ComponentCollection());
            
            if($Transformer->validateConfig($this->params) === false){
                $this->raiseConfigureException("Invalid params in ImageTransform Config");
            }
            
            $Transformer->process($blob,$this->format);
            return $Transformer->provideBlob();
        }
        
        return $blob;
    }
    
    public function proxy($file){
        $this->cache = false;
        $this->notFound = false;
        
        $binary = file_get_contents($file);
        $blob = base64_encode($binary);
        $this->processBlob($blob);
    }
    
    private function resolvFormat(){
        if($this->format === $this->type){
            return;
        }
        
        $bin = base64_decode($this->blob);
        $res = imagecreatefromstring($bin);
        
        if($res === false){
            $this->notFound = true;
            return;
        }
        
        ob_end_clean();
        ob_start();
        
        switch ($this->format){
            case 'image/jpeg':
                imagejpeg($res); 
                break;
            case 'image/png':
                imagepalettetotruecolor($res);
                imagealphablending($res, false);
                imagesavealpha($res, false);
                imagepng($res); 
                break;
        }
        
        imagedestroy($res);
        $binary = ob_get_contents(); 
        ob_end_clean();
        ob_start();
        
        if($binary === false || $binary === ''){
            $this->notFound = true;
            return;
        }
        
        $blob = base64_encode($binary);
        $this->processBlob($blob);
    }
    
    private function resolvImageSize(){
        $size = strlen(base64_decode($this->blob));
        return $size;
    }
    
    private function resolvImageType(){
        $size = getimagesizefromstring(base64_decode($this->blob));
        
        if($size === false){
            return '';
        }
        
        return $size['mime'];
    }
    
    private function resolvImageFile(){
        $ext = null;
        
        switch ($this->type){
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'image/png':
                $ext = 'png';
                break;
        }
        
        if(is_null($ext) === true){
            return $this->name;
        }
        
        return "{$this->name}.{$ext}";
    }
    
    private function parsePayload($payload){
        if(isset($payload['id'])){
            $this->id = $payload['id'];
        }
        
        if(isset($payload['params'])){
            $this->params = $payload['params'];
        }
        
        if(isset($payload['cache']['If-Modified-Since']) === true){
            $this->since = $payload['cache']['If-Modified-Since'];
        }
    }
    
    private function parseConfig($config){
        $this->loadModel($config['data']);
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        if(isset($config['data']['field']) === false){
            $this->raiseConfigureException("Invalid Config <missing.key:data.field> in ImageKlezkaffold");            
        }
        
        if(isset($config['data']['file']) === false){
            $this->raiseConfigureException("Invalid Config <missing.key:data.file> in ImageKlezkaffold");            
        }
        
        if(isset($config['data']['format']) === true){
            $this->format = $config['data']['format'];
        }
        
        $this->field = $config['data']['field'];
        $this->name = $config['data']['file'];
        $this->id = $this->resolver($config);
    }
}