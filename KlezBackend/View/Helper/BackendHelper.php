<?php

App::uses('KlezBackendHelper', 'KlezBackend.View/Helper');

class BackendHelper extends KlezBackendHelper {
    public function provideLogtag() {
        return 'Backend Feed';
    }

    public function provideVarname() {
        return BACKEND_DATA;
    }
}