<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class ShowKlezkaffoldComponent extends KlezkaffoldComponent{
    private $page;
    private $limit;
    private $count = 0;
    private $schema = [];
    private $data = [];
    private $dataLink = [];
    private $links = [];
    
    public function output() {
        if(is_null($this->query) === true){
            return [ 
                'exception' => 404 
            ];
        }
        
        return [ 
            'data' => $this->dataLink,
            'count' => $this->count,
            'page' => $this->page,
            'schema' => $this->schema,
            'limit' => $this->limit,
            'links' => $this->links,
        ];
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
        $this->parseActions($config);
        $this->parsePayload($payload);
    }
    
    public function process(){
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }

        $this->resolvCount();
        $this->resolvData();
//        error_log($this->getModel()->getLastQuery());
        $this->resolvDataLinks();
        
        $this->links = $this->resolvShowLinks();
    }
    
    private function resolvShowLinks(){
        $links = [];
        
        foreach($this->links as $link){
            if($this->isUrlAllowed($link)){
                $links[] = $link;
            }
        }
        
        return $links;
    }
    
    private function resolvDataLinks(){
        if(is_array($this->data) === false){
            return;
        }
        
        foreach($this->data as $row){
            $this->dataLink[] = [
                'data' => $row,
                'links' => $this->resolvLinks($row)
            ];
        }
    }
    
    private function resolvData(){
        if(is_null($this->query) === false){   
            if(isset($this->query['order']) === false){
                $alias = $this->getModel()->alias;
                $pkey = $this->getModel()->primaryKey;
                $this->query['order'] = "{$alias}.{$pkey} DESC";
            }
            
            $offset = ($this->page - 1) * $this->limit;
            $query = $this->query;
            $query['offset'] = $offset;   

            $this->data = $this->getModel()->findReadable('all', $query, true);
//            error_log($this->getModel()->getLastQuery());
            $this->schema = $this->getModel()->provideReadableSchema();
        }
    }
    
    private function resolvCount(){
        $query = $this->query;
        unset($query['limit']);

        try{
            $this->count = $this->getModel()->find('count', $query);
        }
        catch(\Exception $e){
            error_log($e->getMessage());
        }
    }
    
    private function parsePayload($payload){
        $this->setPayload($payload);
        
        if(isset($payload['page']) === false){
            $this->raiseConfigureException("No Data <Show:payload.page> in Klezkaffold");
        }
        
        $this->page = (int) $payload['page'];
        
        if($this->page < 1){
            $this->raiseBadRequestException("Invalid Data <Show:payload.page LESSER THAN 1> in Klezkaffold");            
        }
    }
    
    private function parseConfig($config){
        if(isset($config['data']['query']) === false){
            $this->raiseConfigureException("No Conf <Show:data.query> in Klezkaffold Config");
        }
        
        if(isset($config['data']['query']['limit']) === false){
            $this->raiseConfigureException("No Conf <Show:data.query.limit> in Klezkaffold Config");
        }
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        if(isset($config['links']) === true){
            $this->links = $config['links'];
        }
        
        $this->limit = $config['data']['query']['limit'];
        $this->query = $config['data']['query'];
        $this->loadModel($config['data']);
    }
}