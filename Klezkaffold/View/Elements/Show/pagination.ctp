<div class="col-sm-3">
    <div class="dataTables_info" id="datatable-responsive_info" role="status" aria-live="polite">
        <?=$this->Show->resolvPaginationSummary($count,$c)?>
    </div>            
</div>
<div class="col-sm-9">
    <div class="dataTables_paginate paging_simple_numbers" id="datatable-responsive_paginate">
        <ul class="pagination">
            <?php $this->Show->backButton($page)?>
            <?php $this->Show->numbers($page,$count,$limit)?>
            <?php $this->Show->nextButton($page,$count,$limit)?>
        </ul>
    </div>
</div>