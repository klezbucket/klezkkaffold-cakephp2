<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class MassiveHelper extends KlezkaffoldHelper {
    public function deploy($config){
        $massive = [];
        
        if(isset($config['massive'])){
            $massive = $config['massive'];
        }
        
        if(empty($massive)){
            $this->_View->Form->deploy($config);
        }
        else{
            $data = $config['data'];
            $map = $config['map'];
            
            echo $this->element('main',[
                'data' => $data,
                'massive' => $massive,
                'map' => $map
            ]);
        }
    }

    public function provideLogtag() {
        return 'Massive';
    }
    
    public function thead($map){
        foreach($map as $schema){
            echo $this->element('th', $schema);
        }
    }
    
    public function rows($massive,$map){
        foreach($massive as $data){
            echo $this->element('tr',[
                'map' => $map,
                'data' => $data
            ]);
        }
    }
    
    public function cells($data,$map){
        foreach($map as $field => $schema){
            $validations = $data['validation'];
            $messages = [];
            
            if(isset($validations[$field])){
                $messages = $validations[$field];
            }
            
            echo $this->element('td', [
                'value' => $this->resolvValue($field, $schema, $data['data']),
                'field' => $field,
                'validations' => $messages,
            ]);
        }
    }
    
    public function resolvValue($field,$schema,$row){     
        switch($schema['type']){
            case 'foreign':
                $id = $row[$field];
                
                if(is_null($id)){
                    return $row['***foreignraw***'][$field];
                }
                
                break;
        }
        
        return parent::resolvValue($field, $schema, $row);
    }
    
    public function validations($messages){
        foreach($messages as $message){
            echo $this->element('validation', [
                'message' => $message
            ]);
        }
    }
    
    public function validity($data){
        if($data['valid']){
            echo $this->element('valid');
        }
        else{
            echo $this->element('invalid');
        }
    }
    
    public function confirm(){
        echo $this->element('confirm');
    }
}