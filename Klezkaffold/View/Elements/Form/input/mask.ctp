<div class="input-group m-t-10">
    <input 
        type="text"
        placeholder="<?=$label?>" 
        class="form-control"
        value="<?=$value?>"
        data-mask="<?=$mask?>"
        name="<?=$field?>">
    
    <span class="input-group-addon"><i class="fa fa-magic"></i></span>
</div>