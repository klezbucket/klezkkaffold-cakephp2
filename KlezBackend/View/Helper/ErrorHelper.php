<?php

App::uses('AppHelper', 'View/Helper');

class ErrorHelper extends AppHelper {
    public function deploy(){
        if(Configure::read('debug') > 0){
            echo $this->element('main');
        }
    }
    
    private function element($name,$vars = []){
        return $this->_View->element("KlezBackend.Error/{$name}",$vars);
    }
    
    public function main(){
        $error = $this->_View->viewVars['error'];
        
        if(($error instanceof Exception) === false){
            return;
        }
        
        echo $this->element('explain',[
            'message' => $error->getMessage(),
            'line' => $error->getLine(),
            'file' => $error->getFile(),
        ]);
    }
    
    public function trace(){
        $error = $this->_View->viewVars['error'];
        
        if(($error instanceof Exception) === false){
            return;
        }
        
        $trace = $error->getTrace();
        
        if(empty($trace)){
            return;
        }
        
        echo $this->element('trace',[
            'trace' => $trace,
        ]);
    }
    
    public function traceExplain($trace){
        if(isset($trace['line']) && isset($trace['file'])){        
            echo $this->element('traceExplain',[
                'message' => '',
                'line' => $trace['line'],
                'file' => $trace['file'],
            ]);            
        }
        else{    
            echo $this->element('traceExplain',[
                'message' => '',
                'line' => $trace['function'] . '()',
                'file' => $trace['class'] . ':',
            ]);
        }
    }
}