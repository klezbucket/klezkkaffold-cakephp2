<?php

App::uses('KlezJsonComponent', 'KlezJson.Controller/Endpoint');

class KlezJsonGenerateComponent extends KlezJsonComponent{
    private $data;
    private $query = [];
    private $Model;
    
    private function initializeLog(){
        $file = 'json';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'json' ],
            'file' => $file
        ));    
    }
    
    protected function logjson($message){
        $fullMessage = "[{$this->getLogHash()}] {$message}";
        CakeLog::write('debug', $fullMessage, 'json');
    }
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        
        $this->setSimple(true);
        $this->initializeLog();
        $this->loadConfig();
    }
    
    public function main($payload){
        set_time_limit(0);
        ini_set('memory_limit','-1');
        
        $jsonData = Configure::read('JsonData');
        $this->logjson('############################ JSON GEN ############################');
        
        foreach($jsonData as $data){
            $this->process($data);
        }
        
        $this->storage();
    }
    
    private function storage(){
        $this->getJson()->begin();
        
        try{
            $json = json_encode($this->data);
            unset($this->data);
            
            $data = [
                'created' => date('Y-m-d H:i:s'),
                'size' => strlen($json),
                'etag' => md5($json),
                'master' => true,
                'administrator_id' => $this->getAuth()->getId()
            ];
            
            if($this->getJson()->updateAll([ 'Json.master' => false ]) === false){
                throw new Exception("Json::updateAll() === false");
            }
            
            $this->getJson()->prepareForStore($data);
            
            if($this->getJson()->saveData() === false){
                throw new Exception("Json::saveData() === false");
            }
            
            if($this->processHooks() === false){
                throw new Exception("JsonGenerate::processHooks() === false");
            }
            
            if($this->getJson()->storeThisFile($json,'document') === false){
                throw new Exception("Json::storeThisFile() === false");
            }
            
            unset($json);
            $this->getJson()->commit();
            
            $this->overwrite([ 
                'success' => true,
                'json' => [
                    'created' => date('d/m/Y H:i:s',strtotime($this->getJson()->readField('created'))),
                    'etag' => $this->getJson()->readField('etag'),
                    'size' => number_format($data['size'],0,',','.') . ' bytes',
                ],         
                'url' => Router::url([ 'controller' => 'jsons', 'action' => 'master','plugin' => 'KlezJson'],true)
            ]);
        } 
        catch (Exception $e) {
            error_log("Exception @ KlezJsonGenerateComponent::storage() - {$e->getMessage()}");
            $this->getJson()->rollback();
            $this->overwrite([ 'success' => false ]);
        }
    }
    
    private function processHooks(){
        if(method_exists($this->getJson(), 'beforeGenerate') === false){
            return true;
        }
        
        return $this->getJson()->beforeGenerate($this);
    }
    
    private function process($data){
        $this->loadModel($data);
        $table = $this->Model->useTable;
        $this->logjson("{$this->Model->alias}@{$table}");
        $dump = $this->Model->findReadable('all',$this->query);
        $this->logjson("SQL: {$this->Model->getLastQuery()}");        
        
        if($dump){
            $c = count($dump);
            $this->logjson("COUNT: {$c}");
            $this->data[$table] = $dump;
        }
        else{
            $this->logjson("COUNT: <empty>");
            $this->data[$table] = [];
        }
    }
    
    private $recursive = [];
    
    public function getRecursive(){
        return $this->recursive;
    }
    
    public function recursive(){
        set_time_limit(0);
        ini_set('memory_limit','-1');
        
        $jsonData = Configure::read('JsonRecur');
        $this->logjson('############################ JSON RECUR GEN ############################');
        
        foreach($jsonData as $data){
            if(isset($data['base']) === false){
                continue;
            }
            
            $baseconf = "JsonData.{$data['base']}";
            $base = Configure::read($baseconf);
            $this->processBase($this->recursive,$base,$data);
        }
    }
    
    private $currentForeign;
    
    private function processBase(&$stockpile,$base,$current){
        $this->loadModel($base);
        $table = $this->Model->useTable;
        $this->logjson("{$this->Model->alias}@{$table}");
        $dump = $this->Model->findReadable('all',$this->query);
        $this->logjson("SQL: {$this->Model->getLastQuery()}");        
        
        if($dump){
            $c = count($dump);
            $this->logjson("COUNT: {$c}");
            
            if(isset($current['foreign'])){
                $this->currentForeign = $current['foreign'];
                
                $dump = array_map(function($row){
                    unset($row[$this->currentForeign]);
                    
                    if(count($row) === 1){
                        return end($row);
                    }
                    
                    return $row;
                }, $dump);
            }
            
            $stockpile[$table] = $dump;
            
            if(isset($current['recursive']) === false){
                return;
            }
            
            foreach($dump as $i => $raw){
                foreach($current['recursive'] as $recurConf){
                    $baseconf = "JsonData.{$recurConf['base']}";
                    $base = Configure::read($baseconf);
                    $foreign = $recurConf['foreign'];
                    $class = $base['class'];
                    $base['query']['conditions']["{$class}.{$foreign}"] = $raw['id'];
                    $this->processBase($stockpile[$table][$i], $base, $recurConf);
                }
            }
        }
        else{
            $this->logjson("COUNT: <empty>");
            $stockpile[$table] = [];
        }
    }
    
    private function loadModel($config){
        $this->query = [];
        
        if(isset($config['class']) === false){
            throw new ConfigureException("No Conf <KlezJsonGenerate:class> in KlezJson Config");
        }
        
        if(isset($config['path']) === false){
            throw new ConfigureException("No Conf <KlezJsonGenerate:path> in KlezJson Config");
        }
        
        if(isset($config['query']) === true){
            $this->query = $config['query'];  
        }
                
        $class = $config['class'];
        $path = $config['path']; 
        
        App::uses($class, $path);
        
        if(class_exists($class) === false){
            throw new ConfigureException("No Class<$class> for KlezJson");
        }
        
        $this->Model = new $class();
        
        if(($this->Model instanceof KlezModel) === false){
            throw new ConfigureException("Not an KlezModel<Class:$class> in KlezJson");
        }
        
        if(isset($config['schema']) === true){
            $this->Model->shapeshift($config['schema']);
        }
    }
    
    private function loadConfig(){
        Configure::load('klezjson');
        
        $confiles = Configure::read('Json.confiles');
        
        foreach($confiles as $file){
            Configure::load($file);
        }
    }

    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
      
        throw new NotImplementedException("Unknown format:$format");
    }
}