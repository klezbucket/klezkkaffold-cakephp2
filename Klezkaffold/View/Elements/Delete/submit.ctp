<form method="post">
    <button class="btn btn-success waves-effect waves-light" type="submit">
        <i class="fa fa-<?=$this->Backend->feed('submit.icon')?> m-r-5"></i> <span><?=$this->Backend->feed('submit.label')?></span>
    </button>
</form>