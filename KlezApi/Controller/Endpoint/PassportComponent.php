<?php

App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');

class PassportComponent extends EndpointComponent{
    private $Auth;
    private $data = [];
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        
        $this->Auth = $controller->Auth;
    }
    
    public function login($data){
        $this->data = [
            'auth' => $this->Auth->authentication($data)
        ];
    }
    
    public function json(){
        return json_encode($this->data);
    }
    
    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        return 'text/plain';
    }
}
