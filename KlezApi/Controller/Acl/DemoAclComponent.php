<?php

App::uses('BaseAclComponent', 'KlezApi.Controller/Acl');

class DemoAclComponent extends BaseAclComponent {
    public function control($ctrl) {        
        $control = Inflector::camelize($ctrl);
        $method  = "acl{$control}";
        
        if(method_exists($this, $method) === false){
            throw new ConfigureException("No Acl<control:{$control}> in Acl Config");
        }
        
        $this->{$method}();
    }
    
    private function aclNoSession(){
        if($this->getAuth()->hasSession()){
            throw new ForbiddenException();
        }
    }
    
    private function aclSession(){
        if($this->getAuth()->hasSession() === false){
            throw new ForbiddenException();
        }
    }
    
    private function aclNone(){
        
    }

    public function isUrlAllowed($url) {
        return false;
    }
}