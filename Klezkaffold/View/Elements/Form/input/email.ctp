<div class="input-group m-t-10">
    <input 
        type="email"
        placeholder="<?=$label?>" 
        class="form-control"
        value="<?=$value?>"
        name="<?=$field?>">
    
    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
</div>