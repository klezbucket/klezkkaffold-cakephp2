<div class="input-group m-t-10">
    <input 
        name="<?=$field?>"
        class="form-control"
        multiple="multiple" 
        multiple 
        data-placeholder="<?=$placeholder?>">
    
    <span class="input-group-addon"><i class="fa fa-<?=$icon?>"></i></span>
</div>
        
<script>
    $(function(){
        var field = <?=json_encode($field)?>;
        var url = <?=json_encode($url)?>;
        var id = <?=json_encode($value)?>;
        var label = <?=json_encode($label)?>;
        var dep = <?=json_encode($dep)?>;
        var input = $('input[name="' + field + '"]');
        var depInput;
        var placeholder;
        
        
        if(dep !== null){
            var depLabel = $('#holder_for_' + dep + ' h3');
            depInput = $("[name='" + dep + "']");
            placeholder = input.attr('data-placeholder');
            
            depInput.off('change').on('change',function(){
                var v = $(this).val();
                
                if(v !== ''){
                    input.removeAttr('disabled');
                    input.attr('placeholder',placeholder);
                }
                else{
                    input.val('');
                    input.attr('disabled','disabled');
                    input.attr('placeholder','Debe especificar ' + depLabel.html());
                }
                
                select2();
            }).trigger('change');
        }
        
        function select2(){
            input.select2({
                maximumSelectionLength: 1,
                maximumSelectionSize: 1,
                formatNoMatches: function() {
                    return 'No hay resultados';
                },
                formatSelectionTooBig: function (limit) {
                    return 'Seleccion completa';
                },
                ajax: {
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: function (term) {
                        var data =  {
                            field : field,
                            term: term
                        };

                        if(dep !== null){
                            data.dep = depInput.val();
                        }

                        return data;
                    },
                    results: function (data) {
                        var results = [];

                        $.each(data, function(id, label){
                            results.push({
                                id : id,
                                text : label
                            });
                        });

                        return {
                            results: results
                        };
                    }
                }
            });

            if(label !== null){
                input.select2('data', {id : id , text: label});
            }
        }
        
        select2();
    });
</script>

<style>
    .select2-searching{
        display: none !important;
    }
</style>