<?php

$config = [];

#################################################### DEBUG

$config['Sitemap.debug.console'] = [
    'h1' => 'Debug Console',
];

$config['Sitemap.debug.facebook'] = [
    'h1' => 'Debug Facebook Access Token',
];

$config['Sitemap.debug.fcm'] = [
    'h1' => 'Debug FCM',
];