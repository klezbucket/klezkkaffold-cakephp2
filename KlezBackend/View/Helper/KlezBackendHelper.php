<?php


App::uses('AppHelper', 'View/Helper');

abstract class KlezBackendHelper extends AppHelper {
    private $cache = [];
    
    abstract function provideVarname();
    abstract function provideLogtag();

    public function __construct(\View $View, $settings = array()) {
        parent::__construct($View, $settings);
        
        $file = 'klezweb';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'web' ],
            'file' => $file
        ));
    }
    
    private function logweb($message){
        $logHash = $this->_View->viewVars[LOG_HASH];
        $fullMessage = "[{$logHash}] {$message}";
        CakeLog::write('debug', $fullMessage, 'web');
    }
    
    private function readCache($path){
        if(isset($this->cache[$path]) === false){
            return null;
        }
        
        return $this->cache[$path];
    }
    
    private function writeCache($path,$val){
        $this->cache[$path] = $val;
    }

    public function feed($path,$throwable = true,$cache = true){
        if($cache){
            $cached = $this->readCache($path);

            if(is_null($cached) === false){
                $vstring = is_array($cached) ? serialize($cached) : $cached;
                $this->logweb("{$this->provideLogtag()}<path:{$path},val:{$vstring}>");
                return $cached;
            }
        }
        
        $vars = $this->_View->viewVars[$this->provideVarname()];
        $indexes = explode('.', $path);
        $ptr = $vars;
        $queue = [];
        
        foreach($indexes as  $index){
            if(array_key_exists($index,$ptr) === false){
                return $this->traceFail($throwable,$queue,$index);
            }
            
            $queue[] = $index;
            $ptr = $ptr[$index];
        }
        
        $this->writeCache($path,$ptr);
        $vstring = is_array($ptr) ? serialize($ptr) : $ptr;
        $this->logweb("{$this->provideLogtag()}<path:{$path},val:{$vstring}>");
        return $ptr;
    }
    
    private function traceFail($throwable,$queue,$index){
        $qstring = implode('.', $queue);
        $this->logweb("{$this->provideLogtag()} Fail<lookup:{$index},queue:{$qstring}>");
        
        if($throwable === true){
            $this->raiseInternalServerError("{$this->provideLogtag()} Throwable<TRUE:raise exception>");
        }
        else{
            $this->logweb("{$this->provideLogtag()} Throwable<FALSE:returning null>");
            return null;
        }
    }
    
    protected function raiseInternalServerError($message){
        $this->logweb($message);
        throw new InternalErrorException($message);
    }
    
    public function feedUrl($path,$throwable = true,$full = true,$cache = true){
        $url = $this->feed($path, $throwable, $cache);
        return Router::url($url, $full);
    }
    
}
