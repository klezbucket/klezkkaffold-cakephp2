<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class AutocompleteRequestFormComponent extends KlezkaffoldComponent{
    private $data = [];
    private $field;
    private $term;
    private $dep = false;
    
    public function output() {
        return $this->data;
    }

    public function input($config,$payload = null) {
        $this->setPayload($payload);
        $this->parseConfig($config);
        
        if(isset($payload['data']['term']) === false){
            $this->raiseConfigureException("Invalid Payload <missingkey:data.term> in Klezkaffold Config");
        }
        
        if(isset($payload['data']['field']) === false){
            $this->raiseConfigureException("Invalid Payload <missingkey:data.field> in Klezkaffold Config");
        }
        
        $this->term = $payload['data']['term'];
        $this->field = $payload['data']['field'];
        
        if(isset($payload['data']['dep'])){
            $this->dep = $payload['data']['dep'];
        }
    }
    
    public function process(){
        $schema = $this->getModel()->provideSchema();
        $meta = $schema[$this->field];
        $this->query = [];
        
        if(isset($meta['autocomplete']['class']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.class> in AutocompleteRequestForm');
        }
        
        if(isset($meta['autocomplete']['path']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.path> in AutocompleteRequestForm');
        }
        
        if(isset($meta['autocomplete']['label']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.label> in AutocompleteRequestForm');
        }
        
        if(isset($meta['autocomplete']['identifier']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.identifier> in AutocompleteRequestForm');
        }
        
        if(isset($meta['autocomplete']['query'])){
            $this->query = $meta['autocomplete']['query'];
        }
        
        $class = $meta['autocomplete']['class'];
        $path = $meta['autocomplete']['path'];
        $label = $meta['autocomplete']['label'];
        $identifier = $meta['autocomplete']['identifier'];
        
        App::uses($class,$path);
        
        if(class_exists($class) === false){
            $this->raiseBadRequestException("Model not found<class:$class> in AutocompleteRequestForm");
        }
        
        $Child = new $class();
        
        if($this->term !== ''){
            $this->query['conditions']["{$Child->alias}.{$label} LIKE "] = "{$this->term}%";
        }
        
        if($this->dep !== false){
            if(isset($meta['autocomplete_dep_condition'])){
                $this->query['conditions']["{$meta['autocomplete_dep_condition']}"] = $this->dep;
            }
        }
        
        if(isset($this->query['limit']) === false){
            $this->query['limit'] = 10;
        }
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
        
        if(is_null($this->query)){
            return;
        }
        
        $rawdata = $Child->find('all', $this->query);
        $options = [];
        
        if($rawdata === false){
            return;
        }
        
        foreach($rawdata as $data){
            $id = $data[$identifier];
            $lbl = $data[$label];
            $options[$id] = $lbl;
        }
        
        $this->data = $options;
    }
    
    public function parseConfig($config){
        $this->loadModel($config['data']);
        
        if(isset($config['data']['autocomplete']['prequery']) === true){
            $this->prequery = $config['data']['autocomplete']['prequery'];
        }
        
    }
}