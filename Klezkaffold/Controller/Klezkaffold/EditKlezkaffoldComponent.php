<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class EditKlezkaffoldComponent extends KlezkaffoldComponent{
    private $success = false;
    private $schema = [];
    private $data = [];
    private $id = null;
    private $redirect = null;
    private $notFound = false;
    private $url;
    
    public function output() {
        $schema = $this->resolvFormSchema();
        
        $data = [
            'schema' => $schema,
            'data' => $this->getModel()->getData(),
            'id' => $this->id,
            'success' => $this->success,
            'messages' => $this->getModel()->validationErrors,
        ];
        
        if($this->notFound === true){
            $data = [
                'exception' => 404
            ];
        }
        
        
        return $data;
    }
    
    public function isSuccess() {
        return $this->success;
    }
    
    public function getRedirect(){
        return $this->redirect;
    }
    
    public function input($config,$payload = null) {
        $this->parseConfig($config);
        
        if(array_key_exists('formdata',$payload['data']) === false){
            $this->raiseBadRequestException("Invalid Payload <missing.key:data.formdata> in EditKlezkaffold");
        }
        
        if(array_key_exists('redirect',$payload['data']) === false){
            $this->raiseBadRequestException("Invalid Payload <missing.key:data.redirect> in EditKlezkaffold");
        }
        
        if(array_key_exists('id',$payload['data'])){
            $this->id = $payload['data']['id'];
        }
        
        $this->url = $payload['data']['redirect'];
        $this->data = $this->formData($payload['data']['formdata']);
    }
    
    private function resolvQuery(){
        $this->query = [];
        $alias = $this->getModel()->alias;
        $pkey = $this->getModel()->primaryKey;
        
        $conditions = [
            "{$alias}.{$pkey}" => $this->id
        ];
            
        $this->query['conditions'] = $conditions;
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
    }
    
    private function loadData(){
        $this->resolvQuery();
        
        if(is_null($this->query)){
            $this->notFound = true;
            return false;
        }
        
        if($this->getModel()->findWritable('first',$this->query,false) === false){
            return false;
        }
        
        return true;
    }
    
    public function process(){
        if($this->loadData() === false){
            return;
        }
        
        $this->schema = $this->getModel()->provideWritableSchema();
        $this->getModel()->prepareForStore($this->data,$this->id);
        
        if($this->getModel()->validateData() === false){
            $this->failureCallback();
            return;
        }
        
        if($this->saveData() === false){
            $this->failureCallback();
            return;
        }
        
        $this->successCallback();
    }
    
    private function successCallback(){
        $this->success = true;
        $this->getModel()->loadById($this->id);
        $this->redirect = $this->resolvRedirect($this->getModel()->getData(), $this->url);
    }
    
    private function failureCallback(){
        $this->success = false;
        $this->redirect = null;
    }
    
    public function parseConfig($config){
        $this->loadModel($config['data']);
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        $this->id = $this->resolver($config);
    }
}