<?php

App::uses('KlezJsonComponent', 'KlezJson.Controller/Endpoint');

class KlezJsonMasterComponent extends KlezJsonComponent{
    private $Json;
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
    }
    
    private function checkCache($lastModified,$etag){
        if(@$_SERVER['HTTP_IF_NONE_MATCH'] === $etag || @$_SERVER['HTTP_IF_LAST_MODIFIED'] === $lastModified){
            header('HTTP/ 304 Not Modified');
            die();
        }
    }
    
    public function recursive($payload){
        if($this->getJson()->fetchMaster()){
            $lastModified = gmdate("D, d M Y H:i:s",  strtotime($this->getJson()->readField('created'))) . " GMT";
            $etag = $this->getJson()->readField('etag');
            $this->checkCache($lastModified,$etag);
        
            header('Last-Modified: ' . $lastModified);
            header('ETag: ' . $etag);
            $file = $this->getJson()->getThatFile('recursive');
            
            if(file_exists($file)){
                $data = file_get_contents($file);  
                $this->rawJson($data);
                return;
            }
        }
        
        throw new NotFoundException();
    }
    
    public function main($payload){
        if($this->getJson()->fetchMaster()){
            $lastModified = gmdate("D, d M Y H:i:s",  strtotime($this->getJson()->readField('created'))) . " GMT";
            $etag = $this->getJson()->readField('etag');
            $this->checkCache($lastModified,$etag);
        
            header('Last-Modified: ' . $lastModified);
            header('ETag: ' . $etag);
            $file = $this->getJson()->getThatFile('document');
            
            if(file_exists($file)){
                $data = file_get_contents($file);  
                $this->rawJson($data);
                return;
            }
        }
        
        throw new NotFoundException();
    }

    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        throw new NotImplementedException("Unknown format:$format");
    }
}