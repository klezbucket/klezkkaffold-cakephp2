<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class ReportKlezkaffoldComponent extends KlezkaffoldComponent{
    private $Report;
    private $type;
    private $links = [];
    
    public function output() {
        $data = $this->Report->output();
        
        if(is_array($data)){
            $data['links'] = $this->links;
        }
        
        return $data;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
        $this->loadReport();
        
        return $this->Report->input($config,$payload);
    }
    
    private function loadReport(){
        $class = Inflector::camelize($this->type) . 'ReportComponent';
        $path = 'Klezkaffold.Controller/Report';
        
        App::uses($class, $path);
        
        if(class_exists($class) === false){
            $this->raiseConfigureException("No Such Class <class:$class,path:$path> in Report Config");
        }
        
        $collection = new ComponentCollection();
        $this->Report = new $class($collection);
        
        if(($this->Report instanceof KlezkaffoldComponent) === false){
            $this->raiseConfigureException("Invalid Class <super.needed:KlezkaffoldComponent> in Report Config");
        }
        
        $this->Report->setAuth($this->getAuth());
        $this->Report->setAcl($this->getAcl());
    }
    
    public function process(){
        $this->links = $this->resolvReportLinks();
        $this->Report->process();
    }
    
    private function resolvReportLinks(){
        $links = [];
        
        foreach($this->links as $link){
            if($this->isUrlAllowed($link)){
                $links[] = $link;
            }
        }
        
        return $links;
    }
    
    public function parseConfig($config){
        if(isset($config['type']) === false){
            $this->raiseConfigureException("No Conf <Report:type> in Klezkaffold Config");
        }
        
        if(isset($config['links']) === true){
            $this->links = $config['links'];
        }
        
        $this->type = $config['type'];
    }
}