<?php

App::uses('KlezApiAppController', 'KlezApi.Controller');

class ServerController extends KlezApiAppController {
    public $components = [ 'KlezApi.Server', 'KlezApi.Auth', 'KlezApi.Acl' ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function index(){
        if(Configure::read('debug') > 0){
            $alice = microtime(true);
            header('X-Benchmark-Alice: ' . $alice);
        }
        
        try{
            $this->Server->load();
            $this->Server->gatherData();
            
            $config = $this->Server->getConfig();
            $authConf = $config['auth'];
            $aclConf = $config['acl'];
                    
            $this->Auth->load($authConf);
            $this->Acl->load($aclConf);
            $this->Acl->control();
            
            $component = $this->Server->getComponent();
            $data = $this->Server->getData();
            $this->Acl->filterEndpoint($component,$data,$config);
            $this->Server->dispatch();
        }
        catch (ConfigureException $e){
            $this->debugException($e);
            $this->Server->notImplemented($e);
        }
        catch (BadRequestException $e){
            $this->debugException($e);
            $this->Server->badRequest($e);
        }
        catch (ForbiddenException $e){
            $this->debugException($e);
            $this->Server->forbidden($e);
        }
        catch (UnauthorizedException $e){
            $this->debugException($e);
            $this->Server->unathorized($e);
        }
        catch (InternalErrorException $e){
            $this->debugException($e);
            $this->Server->internalServerError($e);
        }
        catch (Exception $e){
            $this->debugException($e);
            $this->Server->proxyStatusCode($e);
        }
    }
    
    private function debugException($e){
        $class = get_class($e);
        CakeLog::debug("[{$class}] {$e->getMessage()}");
    }
}