<?php

App::uses('AuthMechanismComponent', 'KlezApi.Controller/AuthMechanism');

class DummyAuthMechanismComponent extends AuthMechanismComponent{
    public function delete($id) {
        
    }

    public function parseConf() {
        
    }

    public function read($id = null) {
        
    }

    public function renew($id = null) {
        
    }

    public function write($id, $data) {
        
    }

}