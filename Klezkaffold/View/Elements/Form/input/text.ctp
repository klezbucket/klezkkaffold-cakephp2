<div class="input-group m-t-10">
    <input 
        type="text"
        placeholder="<?=$label?>" 
        class="form-control"
        value="<?=$value?>"
        name="<?=$field?>">
    
    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
</div>