<div class="input-group m-t-10">
    <input 
        style="cursor: pointer;"
        type="text"
        readonly="readonly"
        placeholder="<?=$placeholder?>"
        class="form-control"
        value=""
        name="fake_<?=$field?>">
    
    <span class="input-group-addon"><i class="fa fa-file"></i></span>
</div>

<input 
    style="position:absolute;left:-1000px;top:-1000px;"
    type="file"
    class="form-control"
    name="trigger_<?=$field?>">

<input 
    style="display:none"
    type="text"
    readonly="readonly"
    class="form-control"
    name="<?=$field?>">

<script>
    $(function(){
        var field = <?=json_encode($field)?>;
        var message = <?=json_encode($message)?>;
        var mimes = <?=json_encode($mimes)?>;
        
        var fake = $('input[name="fake_' + field + '"]');
        var content = $('input[name="' + field + '"]');
        var file = $('input[name="trigger_' + field + '"]');
        
        file.off('change').on('change',function(){
            var file = document.getElementsByName('trigger_' + field);
            getBase64(file[0].files[0]);
        });
        
        fake.off('click').on('click',function(){
            file.trigger('click');
        });
        
        content.val('');
        fake.val('');
        
        function getBase64(file){
            if(validateFile(file,mimes)){
                var reader = new FileReader();
                reader.readAsDataURL(file);

                reader.onload = function () {
                    content.val(reader.result);
                    fake.val(file.name + " (" + file.type + ")");
                };

                reader.onerror = function (error) {
                    var kind = 'error';
                    var message = 'Error al recuperar Archivo';
                    toastr[kind](message);
                    content.val('');
                    fake.val('');
                };
            }
            else{
                var kind = 'warning';
                toastr[kind](message);
                content.val('');
                fake.val('');
            }
        }
        
        function validateFile(file,mimes){
            var type = file.type;
            
            for(var i in mimes){
                if(mimes[i] == type){
                    return true;
                }
            }
            
            return false;
        }
    });
</script>