<?php

App::uses('KlezBackendAppModel', 'KlezBackend.Model');

class Administrator extends KlezBackendAppModel{
    private $schema = [
        'email' => [
            'type' => 'text',
            'subtype' => 'email',
            'required' => true,
            'unique' => true,
            'writable' => true,
            'readable' => true,
            'label' => 'E-mail',
            'unique-message' => 'E-mail en uso',
            'required-message' => 'Debe especificar un E-mail',
        ],
        'password' => [
            'type' => 'text',
            'subtype' => 'password',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => false,
            'label' => 'Password',
            'hash' => [
                'algorithm' => 'md5',
                'salt' => [
                    'entropy' => 'tEFEPFEM3543EefemeFE(%s)SNF3434',
                    'function' => 'sprintf'
                ]
            ],
            'required-message' => 'Debe especificar un Password',
        ],
        'status' => [
            'type' => 'boolean',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Estado',
            'boolean' => [
                0 => 'Suspendido',
                1 => 'Activo'
            ],
            'default' => 1,
        ],
        'kind' => [
            'type' => 'options',
            'options' => [
                'superadmin' => 'Super Admin',
                'stationadmin' => 'Encargado'
            ],
            'default' => 'stationadmin',
            'required' => true,
            'unique' => false,
            'writable' => true,
            'readable' => true,
            'label' => 'Clase de Admin',
            'required-message' => 'Debe especificar una Clase de Admin',
        ],
        'last_login' => [
            'type' => 'datetime',
            'required' => false,
            'unique' => false,
            'writable' => false,
            'readable' => true,
            'null' => 'Nunca se ha logueado',
            'label' => 'Último Login',
        ],
    ];
    
    public function provideSchema() {
        return $this->schema;
    }

    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'ADMIN-ID'
        ];
    }
}