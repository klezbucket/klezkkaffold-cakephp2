<div class="row form-main">
    <?php $this->Form->main($config); ?>
</div>

<script>
    $(function(){
        var holders = $('.form-holder').not('.col-lg-12');
        var mh = 0;
        
        holders.each(function(){
            var h = $(this).height()
            
            if(h > mh){
                mh = h;
            }
        });
        
        holders.each(function(){
            $(this).height(mh);
        });
    });
</script>