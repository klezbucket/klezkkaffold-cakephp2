<?php

$config = [];
$config['KlezkaffoldWrapper.dashboard'] = [
    'component' => 'DashboardKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.show'] = [
    'component' => 'ShowKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.detail'] = [
    'component' => 'DetailKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.request_form'] = [
    'component' => 'RequestFormKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.profile'] = [
    'component' => 'RequestFormKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.add'] = [
    'component' => 'AddKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.edit'] = [
    'component' => 'EditKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.request_delete'] = [
    'component' => 'RequestDeleteKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.delete'] = [
    'component' => 'DeleteKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.report'] = [
    'component' => 'ReportKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.image'] = [
    'component' => 'ImageKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.massive'] = [
    'component' => 'MassiveKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];

$config['KlezkaffoldWrapper.request_massive'] = [
    'component' => 'RequestMassiveKlezkaffoldComponent',
    'path' => 'Klezkaffold.Controller/Klezkaffold'
];