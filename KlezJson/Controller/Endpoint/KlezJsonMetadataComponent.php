<?php

App::uses('KlezJsonComponent', 'KlezJson.Controller/Endpoint');

class KlezJsonMetadataComponent extends KlezJsonComponent{
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
    }
    
    public function main($payload){
        if($this->getJson()->fetchMaster()){
            $payload = [
                'json' => $this->getJson()->getData(),
                'url' => Router::url([ 'controller' => 'jsons', 'action' => 'master','plugin' => 'KlezJson'],true)
            ];
            
            $this->overwrite($payload);
        }
    }

    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        throw new NotImplementedException("Unknown format:$format");
    }
}