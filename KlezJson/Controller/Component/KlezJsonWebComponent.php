<?php

App::uses('WebComponent', 'KlezBackend.Controller/Component');

class KlezJsonWebComponent extends WebComponent{
    private $Controller;
    private $lastModified;
    private $etag;
    
    public function initialize(\Controller $controller) {
        $this->Controller = $controller;
        parent::initialize($controller);
    }
    
    public function metadata(){
        $this->setUrl($this->getApi('json_metadata'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function recursive(){
        $this->putHeader('If-None-Match', @$_SERVER['HTTP_IF_NONE_MATCH']);
        $this->putHeader('If-Modified-Since', @$_SERVER['HTTP_IF_MODIFIED_SINCE']);
        $this->setUrl($this->getApi('json_recursive'));
        $this->setVerb('GET');
        $this->putHeaderHook('caching');
        $this->jsonPayload();
        
        header('Last-Modified: ' . $this->lastModified);
        header('ETag: ' . $this->etag);
        
        if($this->getStatusCode() === 304){      
            header('HTTP/ 304 Not Modified');
            die();
        }
        
        $this->dump('json');
    }
    
    public function master(){
        $this->putHeader('If-None-Match', @$_SERVER['HTTP_IF_NONE_MATCH']);
        $this->putHeader('If-Modified-Since', @$_SERVER['HTTP_IF_MODIFIED_SINCE']);
        $this->setUrl($this->getApi('json_master'));
        $this->setVerb('GET');
        $this->putHeaderHook('caching');
        $this->jsonPayload();
        
        header('Last-Modified: ' . $this->lastModified);
        header('ETag: ' . $this->etag);
        
        if($this->getStatusCode() === 304){      
            header('HTTP/ 304 Not Modified');
            die();
        }
        
        $this->dump('json');
    }
    
    public function curlHeaderHookCaching($ch,$line){
        $etag = [];
        $lastModified = [];
        
        if (preg_match('/^ETag:\s*([^;]*)/mi', $line, $etag) === 1) {
            $this->etag = trim($etag[1]);
            $this->logapicli("Etag<{$this->etag}>");
        }       
        
        if (preg_match('/^Last-Modified:\s*([^;]*)/mi', $line, $lastModified) === 1) {
            $this->lastModified = trim($lastModified[1]);
            $this->logapicli("LastModified<{$this->lastModified}>");
        }         
    }
    
    public function generate(){
        $this->setUrl($this->getApi('json_generate'));
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->jsonPayload();
        $this->dump('json');
    }
}
