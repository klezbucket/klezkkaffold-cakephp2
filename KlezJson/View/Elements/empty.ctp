<script>
    $(function(){
        toastr["warning"]("No hay Master JSON generado");

        toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "0",
          "hideDuration": "0",
          "timeOut": "0",
          "extendedTimeOut": "0",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        };
    });
</script>
<div id="json-master-empty" class="col-sm-12">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-color panel-inverse">
                <div class="panel-heading">
                    <h3 class="panel-title">No hay Master JSON</h3>
                </div>
                <div class="panel-body">
                    <p>
                        Aún no se ha generado ningún JSON.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>