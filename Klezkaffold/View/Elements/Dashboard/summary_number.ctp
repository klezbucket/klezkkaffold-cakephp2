    <div class="col-lg-3 col-md-6">
        <div class="card-box">
            <div class="widget-chart-1">
                <h2 align='center' class="p-t-10 m-b-0" data-plugin="counterup"> 
                    <?=$this->Dashboard->feed('data.number')?> </h2>

                <p align='center' class="text-muted"><?=$this->Dashboard->feed('title')?></p>
            </div>
        </div>
    </div>