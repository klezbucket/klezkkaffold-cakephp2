<?php

App::uses('KlezDataAppModel', 'KlezData.Model');

abstract class KlezModel extends KlezDataAppModel{
    private $Data = [];
    private $shape = 'default';
    
    abstract public function provideSchema();
    abstract public function providePrimaryKeyMetadata();
    
    public function shapeshift($shape){
        $this->shape = $shape;
    }
    
    public function getShape(){
        return $this->shape;
    }
    
    public function getLastQuery() {
        $dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        $lastLog = end($logs['log']);
        return $lastLog['query'];
    }
    
    public function provideSanitizedSchema(){
        $schema = $this->provideSchema();
        
        foreach($schema as $field => $meta){
            if(isset($meta['hash'])){
                unset($schema[$field]['hash']);
            }
        }
        
        return $schema;
    }
    
    public function commit(){
        $dataSource = $this->getDataSource();
        $dataSource->commit();
    }
    
    public function rollback(){
        $dataSource = $this->getDataSource();
        $dataSource->rollback();
    }
    
    public function begin(){
        $dataSource = $this->getDataSource();
        $dataSource->begin();
    }
    
    public function loadArray($array){
        $this->Data = $array;
        $pkey = $this->primaryKey;
        
        if(isset($array[$pkey])){
            $this->id = $array[$pkey];
        }
    }
    
    public function getReadableData(){
        $data = [];
        $schema = $this->provideSchema();
        
        foreach($schema as $field => $meta){
            if(isset($meta['readable']) === false){
                continue;
            }
            
            if(array_key_exists($field,$this->Data) === false){
                continue;
            }
            
            if($meta['readable'] === true){
                $data[$field] = $this->Data[$field];
            }
        }
        
        return $data;
    }
    
    public function deleteData(){
        $success = false;
        
        if($this->delete($this->id)){
            $success = true;
        }
        
        return $success;
    }
    
    private function resolvWritableData($data){
        $schema = $this->provideWritableSchema();
        
        foreach($schema as $field => $meta){
            if(isset($meta['type']) === false){
                continue;
            }
            
            if($meta['type'] === 'file'){
                $data[$field] = 0;
                
                if(in_array($field,$this->FileIgnore)){
                    if(isset($this->Stored[$field])){
                        $data[$field] = $this->Stored[$field];
                    }
                }
            }
        }
        
        return $data;
    }
    
    public function saveData(){
        $data = $this->getWritableData();
        $success = false;
        $this->set($data);
        
        if($this->validates() === false){
            return false;
        }
        
        $writableData = $this->getWritableData();
        $resolvData = $this->resolvWritableData($writableData);
        
        if($this->save($resolvData,false)){
            $this->writeField($this->primaryKey, $this->id);
            $success = true;
        }
        
        if($this->storeFiles($data) === false){
            return false;
        }
        
        return $success;
    }
    
    private function storeFiles($data){
        $schema = $this->provideWritableSchema();
        
        foreach($schema as $field => $meta){
            if(isset($meta['type']) === false){
                continue;
            }
            
            if($meta['type'] !== 'file'){
                continue;
            }
            
            if($this->storeFile($data,$field) === false){
                return false;
            }
        }
        
        return true;
    }
    
    public function resolvBinary($field){
        $path = $this->resolvStorePath($field);
        $name = $this->resolvStoreName();
        
        $filename = $path . '/' . $name;
        
        if(file_exists($filename) === false){
            return false;
        }
        
        if(is_readable($filename) === false){
            return false;
        }
        
        $binary = file_get_contents($filename);
        $size = strlen($binary);
        
        return [
            'size' => $size,
            'blob' => base64_encode($binary)
        ];
    }
    
    private $Files = [];
    
    public function getFile($field){
        return $this->Files[$field];
    }
    
    public function getThatFile($field){
        $path = $this->resolvStorePath($field);
        $name = $this->resolvStoreName();
        $filename = $path . '/' . $name;
        
        return $filename;
    }
    
    public function storeThisFile($blob,$field){
        $path = $this->resolvStorePath($field);
        $name = $this->resolvStoreName();
        
        if(file_exists($path) === false){
            if(mkdir($path,0775,true) === false){
                error_log("Cannot create path $path");
            }
        }
        
        if(is_dir($path) === false){
            error_log("Path inexistent $path");
            return false;
        }
        
        $filename = $path . '/' . $name;
        
        if(file_put_contents($filename, $blob) === false){
            error_log("Cannot put file at $filename");
            return false;
        }
        
        return true;
    }
    
    private function storeFile($data,$field){
        if(in_array($field,$this->FileIgnore)){
            return true;
        }
        
        if(isset($this->Files[$field]) === false){
            return true;
        }
        
        $path = $this->resolvStorePath($field);
        $name = $this->resolvStoreName();
        
        if(file_exists($path) === false){
            if(mkdir($path,0775,true) === false){
                error_log("Cannot create path $path");
            }
        }
        
        if(is_dir($path) === false){
            error_log("Path inexistent $path");
            return false;
        }
        
        $filename = $path . '/' . $name;
        $base64 = $this->resolvMimelessBase64($data,$field);
        $blob = base64_decode($base64);
        
        if($blob === ''){
            return true;
        }
        if(file_put_contents($filename, $blob) === false){
            error_log("Cannot put file at $filename");
            return false;
        }
        
        return $this->saveField($field, time());
    }
    
    private function resolvStoreName(){
        $salt = Configure::read('Upload.salt');
        $file = md5(sprintf($salt,$this->id));
        return $file;
    }
    
    private function resolvStorePath($field){
        Configure::load('upload');
        $folder = Configure::read('Upload.root');
        $weight = Configure::read('Upload.weight');
        
        $alias = $this->alias;
        $slot = floor($this->id / $weight) + 1;
        $path = $folder . '/' . $alias . '/' . $field . '/' . $slot;
        
        return $path;
    }
    
    private $Stored = [];
    
    public function prepareForStore($data = [],$id = null){
        if(is_null($id) == false){
            $this->Stored = $this->Data;
        }
        else{
            $this->Stored = [];
        }
        
        $this->flush();
        $this->writeData($data,$id);
        $this->generateValidations();
    }
    
    public function getStored(){
        return $this->Stored;
    }
    
    private function isHidden($meta){
        if(isset($meta['hidden']) === false){
            return false;
        }
        
        return $meta['hidden'] === true;
    }
    
    private $validationRequiredIgnorees = [];
    
    private function isGroup($meta){
        $grouped = false;
        
        if(isset($meta['group'])){
            $grouped = is_array($meta['group']);
        }
        
        return $grouped;
    }
    
    private function isDep($meta){
        $grouped = false;
        
        if(isset($meta['group']['dep'])){
            $grouped = is_array($meta['group']['dep']);
        }
        
        return $grouped;
    }
    
    private function depValidations($master,$meta){
        $dep = $meta['group']['dep'];
        $v = $this->readField($master);
        $masterDep = $dep[$v];
        unset($dep[$v]);
        
        foreach($dep as $g => $fields){
            foreach($fields as $field){
                if(in_array($field, $masterDep)){
                    continue;
                }
                
                unset($this->validate[$field]['required']);
                $this->validationRequiredIgnorees[] = $field;
            }
        }
    }
    
    private function generateValidations(){
        Configure::load('KlezData.messages');
        
        $schema = $this->provideWritableSchema();
        
        foreach($schema as $field => $meta){
            if(in_array($field, $this->validationRequiredIgnorees)){
                $meta['required'] = false;
            }
            
            if($this->isHidden($meta)){
                continue;
            }
            
            if($this->isGroup($meta)){
                if($this->isDep($meta)){
                    $this->depValidations($field,$meta);
                }
            }
            
            $cancelTypeValidations = false;
            
            if(@$meta['required'] === false){
                $value = $this->readField($field);
                
                if(is_array($value) === false){
                    $data = trim($value);
                    $cancelTypeValidations = $data === '';
                }
            }
            
            if($cancelTypeValidations === false){
                $this->generateTypeValidations($field,$meta);
                $this->generateSubtypeValidations($field,$meta);
            }
            
            $this->generateGeneralValidations($field,$meta);
        }
    }
    
    private function generateGeneralValidations($field,$meta){
        $this->generateRequiredValidation($field,$meta);
        $this->generateUniqueValidation($field,$meta);
        $this->generateCompoundUniqueValidation($field,$meta);
        $this->generateForcedValidation($field,$meta);
    }
    
    private function generateForcedValidation($field,$meta){
        if(isset($meta['forced']) === false){
            return;
        }
        
        $message = $this->resolvValidationMessage('forced',$meta);
        
        $this->validate[$field]['forced_rule'] = [
            'rule' => [ 'forcedValidator',$field ],
            'message' => $message
        ];
    }
    
    public function forcedValidator($data,$field){
        if(isset($data[$field]) === false){
            return false;
        }
        
        $schema = $this->provideWritableSchema();
        
        if(isset($schema[$field]['forced']) === false){
            return false;
        }
        
        $value = $schema[$field]['forced'];
        $val = $this->readField($field);
        return $value == $val;
    }
    
    private function generateRequiredValidation($field,$meta){
        if(isset($meta['required']) === false){
            return;
        }
        
        if($meta['required'] !== true){
            return;
        }
        
        if($meta['type'] === 'file'){
            return;
        }
        
        $message = $this->resolvValidationMessage('required',$meta);
        
        $this->validate[$field]['required_rule'] = [
            'rule' => [ 'requiredValidator',$field ],
            'message' => $message
        ];
    }
    private function resolvFileValidationMessage($rule,$meta){
        $fnMessage = $this->resolvValidationMessageFn($rule,$meta);
        
        if(is_null($fnMessage) === false){
            return $fnMessage;
        }        
        
        $message = Configure::read("Message.{$rule}");
        $key = "{$rule}-message";
        
        if(isset($meta[$key])){
            $message = $meta[$key];
        }
        else if(isset($meta['subtype'])){
            $key = "{$meta['subtype']}-message";
            
            if(isset($meta[$key])){
                $message = $meta[$key];
            }
        }
        
        return $message;
    }
    
    private function resolvValidationMessage($rule,$meta){
        $fnMessage = $this->resolvValidationMessageFn($rule,$meta);
        
        if(is_null($fnMessage) === false){
            return $fnMessage;
        }
        
        $key = "{$rule}-message";
        
        if(isset($meta[$key])){
            $message = $meta[$key];
        }
        else{
            $message = Configure::read("Message.{$rule}");
        }
        
        return $message;
    }
    
    private function resolvValidationMessageFn($rule,$meta){
        $key = "{$rule}-message-fn";
        
        if(isset($meta[$key])){
            $fn = $meta[$key];
            
            if(method_exists($this,$fn)){
                return $this->{$fn}();
            }
        }
        
        return null;
    }
    
    public function validateData(){
        $id = $this->id;
        $data = $this->getData();
        
        $valid = false;
        $this->set($data);
        $this->id = $id;
        
        if($this->validates()){
            $valid = true;
        }
        
        return $valid;
    }
    
    public function requiredValidator($data,$field){
        if(isset($data[$field]) === false){
            return false;
        }
        
        $value = $data[$field];
        
        if(is_null($value) === true){
            return false;
        } 
        
        if(is_bool($value)){
            return true;
        }
        
        if(is_integer($value)){
            return true;
        }
        
        if(is_float($value)){
            return true;
        }
                
        if(trim($value) === ''){
            return false;
        }
        
        return true;
    }

    private function generateCompoundUniqueValidation($field,$meta){
        if(isset($meta['compound_unique']) === false){
            return;
        }
        
        $message = $this->resolvValidationMessage('compound_unique',$meta);
        
        $this->validate[$field]['compound_unique_rule'] = [
            'rule' => [ 'compoundUniqueValidator',$field ],
            'message' => $message
        ];
    }
    
    private function generateUniqueValidation($field,$meta){
        if(isset($meta['unique']) === false){
            return;
        }
        
        if($meta['unique'] !== true){
            return;
        }
        
        $message = $this->resolvValidationMessage('unique',$meta);
        
        $this->validate[$field]['unique_rule'] = [
            'rule' => [ 'uniqueValidator',$field ],
            'message' => $message
        ];
    }

    public function compoundUniqueValidator($data,$field){
        if(isset($data[$field]) === false){
            return true;
        }
        
        $value = $data[$field];
        
        if(is_null($value) === true){
            return true;
        }
        
        if(is_null($this->id) === false){
            $alias = $this->alias;
            $pkey = $this->primaryKey;
            $data["{$alias}.{$pkey} != "] = $this->id;
        }
        
        $schema = $this->provideWritableSchema();
        $meta = $schema[$field]['compound_unique'];
        
        foreach($meta as $f){
            $data["{$f}"] = $this->readField($f);
        }

        $class = get_class($this);
        $Self = new $class();
        
        $exists = $Self->find('first',[
            'conditions' => $data
        ]);
        
        if($exists){
            $valid = false;
        }
        else{
            $valid = true;
        }
        
        return $valid;
    }
    
    public function uniqueValidator($data,$field){
        if(isset($data[$field]) === false){
            return true;
        }
        
        $value = $data[$field];
        
        if(is_null($value) === true){
            return true;
        }
        
        if(is_null($this->id) === false){
            $alias = $this->alias;
            $pkey = $this->primaryKey;
            $data["{$alias}.{$pkey} != "] = $this->id;
        }
        
        $class = get_class($this);
        $Self = new $class();
        
        $exists = $Self->find('first',[
            'conditions' => $data
        ]);
        
        if($exists){
            $valid = false;
        }
        else{
            $valid = true;
        }
        
        return $valid;
    }
    
    private function generateTypeValidations($field,$meta){
        if(isset($meta['type']) === false){
            return;
        }
        
        $type = $meta['type'];
        $method = "{$type}TypeValidation";
        
        if(method_exists($this, $method)){
            $this->{$method}($field,$meta);
        }
    }
    
    private function gmapTypeValidation($field,$meta){
        $schema = $this->provideWritableSchema();
        $value = $this->Data[$field];
        
        if($value === ''){
            return;
        }
        
        $message = $this->resolvValidationMessage('gmap',$meta);
        
        $this->validate[$field]['gmap_rule'] = [
            'rule' => [ 'gmapValidator', $field ],
            'message' => $message
        ];
    }
    
    public function gmapValidator($data,$field){
        $value = $data[$field];        
        $coordinates = explode(';', $value);
        
        if(count($coordinates) !== 2){
            return false;
        }
        
        $lat = $coordinates[0];
        $lon = $coordinates[1];
        
        if(is_numeric($lat) === false){
            return false;
        }
        
        if(is_numeric($lon) === false){
            return false;
        }
        
        return true;
    }
    
    private function datetimeTypeValidation($field,$meta){
        if(isset($meta['subtype']) && $meta['subtype'] == 'primary'){
            return;
        }
        
        $message = $this->resolvValidationMessage('datetime',$meta);
        
        $this->validate[$field]['date_rule'] = [
            'rule' => [ 'datetimeValidator', $field ],
            'message' => $message
        ];
    }
    
    private function dateTypeValidation($field,$meta){
        if(isset($meta['subtype']) && $meta['subtype'] == 'primary'){
            return;
        }
        
        $message = $this->resolvValidationMessage('date',$meta);
        
        $this->validate[$field]['date_rule'] = [
            'rule' => [ 'dateValidator', $field ],
            'message' => $message
        ];
    }
    
    public function datetimeValidator($data,$field){
        if(strlen($data[$field]) === 16){
            $value = $data[$field] . ":00";
        }
        else{
            $value = $data[$field];
        }
        
        $time = strtotime($value);
        
        if($time === false){
            return false;
        }
        
        return true;
    }
    
    public function dateValidator($data,$field){
        $value = str_replace('/','-',$data[$field]);
        $time = strtotime($value);
        
        if($time === false){
            return false;
        }
        
        return true;
    }
    
    private function doubleTypeValidation($field,$meta){
        $message = $this->resolvValidationMessage('double',$meta);
        
        $this->validate[$field]['double_rule'] = [
            'rule' => [ 'doubleValidator', $field ],
            'message' => $message
        ];
        
        if(isset($meta['range'])){
            $message = $this->resolvValidationMessage('range',$meta);

            $this->validate[$field]['range_rule'] = [
                'rule' => [ 'rangeDoubleValidator', $field ],
                'message' => $message
            ];
        }
    }
    
    public function rangeDoubleValidator($data,$field){
        $value = (double) $data[$field];
        $schema = $this->provideWritableSchema();
        $meta = $schema[$field];
        
        $a = (double) $meta['range'][0];
        $b = (double) $meta['range'][1];
        
        return $a <= $value && $b >= $value;
    }
    
    public function doubleValidator($data,$field){
        $value = $data[$field];
        
        if(preg_match('/^[0-9]+$/',$value) === 1){
            return true;
        }
        
        if(preg_match('/^\-?[0-9]+\.[0-9]+$/',$value) === 1){
            return true;
        }
        
        return false;
    }
    
    private function intTypeValidation($field,$meta){
        if(isset($meta['subtype']) && $meta['subtype'] == 'primary'){
            return;
        }
        
        $message = $this->resolvValidationMessage('int',$meta);
        
        $this->validate[$field]['int_rule'] = [
            'rule' => [ 'intValidator', $field ],
            'message' => $message
        ];
        
        if(isset($meta['range'])){
            $message = $this->resolvValidationMessage('range',$meta);

            $this->validate[$field]['range_rule'] = [
                'rule' => [ 'rangeValidator', $field ],
                'message' => $message
            ];
        }
    }
    
    public function rangeValidator($data,$field){
        $value = (int) $data[$field];
        $schema = $this->provideWritableSchema();
        $meta = $schema[$field];
        
        $a = $meta['range'][0];
        $b = $meta['range'][1];
        
        return $a <= $value && $b >= $value;
    }
    
    public function intValidator($data,$field){
        $value = $data[$field];
        
        if(preg_match('/^[0-9]+$/',$value) !== 1){
            return false;
        }
        
        return true;
    }
    
    private function generateSubtypeValidations($field,$meta){
        if(isset($meta['subtype']) === false){
            return;
        }
        
        $subtype = $meta['subtype'];
        $method = "{$subtype}SubtypeValidation";
        
        if(method_exists($this, $method)){
            $this->{$method}($field,$meta);
        }
    }
    
    private function autocompleteSubtypeValidation($field,$meta){
        $message = $this->resolvValidationMessage('autocomplete',$meta);
        
        $this->validate[$field]['autocomplete_rule'] = [
            'rule' => [ 'autocompleteValidator', $field ],
            'message' => $message
        ];
    }
    
    public function autocompleteValidator($data,$field){
        $schema = $this->provideSchema();
        $meta = $schema[$field];
        $query = [];
        
        if(isset($meta['autocomplete']['class']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.class> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['path']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.path> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['identifier']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.identifier> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['label']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.label> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['query'])){
            $query = $meta['autocomplete']['query'];
        }
        
        $class = $meta['autocomplete']['class'];
        $path = $meta['autocomplete']['path'];
        $identifier = $meta['autocomplete']['identifier'];
        $label = $meta['autocomplete']['label'];
        
        App::uses($class,$path);
        
        if(class_exists($class) === false){
            $this->raiseBadRequestException("Model not found<class:$class> in KlezModel");
        }
        
        $Child = new $class();
        $query['conditions']["{$Child->alias}.{$identifier}"] = $this->readField($field);
        
        $raw = $Child->find('first', $query);
        
        if($raw){
            $this->writeField($field . '_autocomplete', $raw[$label]);
            return true;
        }
        
        return false;
    }
    
    private $FileIgnore = [];
    
    private function fileTypeValidation($field,$meta){
        $message = $this->resolvFileValidationMessage('file',$meta);
        
        if($this->Data[$field] === ''){
            $this->validate[$field]['file_rule'] = [
                'rule' => [ 'fileEmptyValidator', $field ],
                'message' => $message
            ];
        }
        else{
            $this->validate[$field]['file_rule'] = [
                'rule' => [ 'fileValidator', $field, $meta['mimes'] ],
                'message' => $message
            ];
        }
    }
    
    private function imageSubtypeValidation($field,$meta){
        $message = $this->resolvValidationMessage('image',$meta);
        
        if($this->Data[$field] === ''){
            $this->validate[$field]['image_rule'] = [
                'rule' => [ 'fileEmptyValidator', $field ],
                'message' => $message
            ];
        }
        else{
            $this->validate[$field]['image_rule'] = [
                'rule' => [ 'imageValidator', $field, $meta['mimes'] ],
                'message' => $message
            ];
        }
    }
    
    public function fileEmptyValidator($data,$field){
        $schema = $this->provideWritableSchema();
        $required = (bool) $schema[$field]['required'];
        
        if(empty($this->Stored) === false){
            if($this->Stored[$field] != 0){
                $this->writeField($field, $this->Stored[$field]);
                $this->FileIgnore[] = $field;
                return true;
            }
        }
        
        return ! $required;
    }
    
    private function fileToStorage($field,$action = true){
        if($action){
            $this->Files[$field] = true;
        }
        else{
            unset($this->Files[$field]);
        }
    }
    
    private function resolvMimelessBase64($data,$field){
        $explode = explode(',', $data[$field]);
        return end($explode);
    }
    
    public function fileValidator($data,$field,$mimes){
        if(isset($data[$field]) === false){
            return true;
        }
        
        $this->fileToStorage($field);
        return true;
    }
    
    public function imageValidator($data,$field,$mimes){
        if(isset($data[$field]) === false){
            return true;
        }
        
        $base64 = $this->resolvMimelessBase64($data,$field);
        $value = base64_decode($base64);
        $imgdata = getimagesizefromstring($value);
        
        if(isset($imgdata['mime']) === false){
            return false;
        }
        
        if(in_array($imgdata['mime'], $mimes) === false){
            return false;
        }
        
        $this->fileToStorage($field);
        return true;
    }
    
    public function maskSubtypeValidation($field,$meta){
        $message = $this->resolvValidationMessage('mask',$meta);
        
        $this->validate[$field]['mask_rule'] = [
            'rule' => [ 'maskValidator', $field, $meta['mask'] ],
            'message' => $message
        ];
    }
    
    public function maskValidator($data,$field,$mask){
        if(isset($data[$field]) === false){
            return true;
        }
        
        $value = $data[$field];
        $maskparts = explode('-', $mask);
        $valueparts = explode('-', $value);
        
        foreach($maskparts as $i => $maskpart){
            if(isset($valueparts[$i]) === false){
                return false;
            }
            
            $valuepart = $valueparts[$i];
            
            if(strlen($maskpart) !== strlen($valuepart)){
                return false;
            }
            
            $regex = str_replace(
                [ '9', 'a'],
                [ '\d', '\p{Latin}'],
                $maskpart
            );
            
            $fullRegex = "|^{$regex}$|";
            
            if(preg_match($fullRegex, $valuepart) !== 1){
                return false;
            }
        }
        
        return true;
    }
    
    public function emailSubtypeValidation($field,$meta){
        $message = $this->resolvValidationMessage('email',$meta);
        
        $this->validate[$field]['email_rule'] = [
            'rule' => [ 'email', false ],
            'message' => $message
        ];
    }
    
    public function writeData($data = [],$id = null){
        $schema = $this->provideWritableSchema();
        $pkey = $this->primaryKey;
        
        $this->id = $id;
        $this->writeField($pkey, $id);
        
        foreach($schema as $field => $meta){
            $value = null;
            
            if(isset($data[$field])){
                $value = $data[$field];
            }
            
            if(isset($meta['fields'])){
                $v = null;
                
                foreach($meta['fields'] as $f){
                    if(isset($data[$f])){
                        $v = $data[$f];
                    }
                    
                    $this->writeField($f, $v);
                }
            }
            
            if(is_null($id) === false){
                if(@$meta['subtype'] === 'password'){
                    if(trim($value) === ''){
                        continue;
                    }
                }
            }
            
            if(@$meta['type'] === 'boolean'){
                if(is_null($value) === true){
                    $value = 0;
                }
            }
            
            $this->writeField($field, $value);
        }
    }
    
    public function getFormData(){
        $data = $this->getWritableData();
        $schema = $this->provideSchema();
        
        foreach($schema as $field => $meta){
            if(isset($meta['fields'])){
                foreach($meta['fields'] as $f){
                    $data[$f] = $this->readField($f);
                }
            }
            
            if(isset($meta['subtype']) === false){
                continue;
            }
            
            if($meta['subtype'] === 'password'){
                $data[$field] = '';
            }
            
            if($meta['subtype'] === 'habtm'){
                $data[$field] = $this->findHabtmFormData($meta);
            }
            
            if($meta['subtype'] === 'tags'){
                $data[$field] = $this->findTagsFormData($meta);
            }
            
            if($meta['subtype'] === 'autocomplete'){
                $raw = $this->getData();
                
                if(array_key_exists($field,$raw)){
                    $data[$field . "_autocomplete"] = $this->findAutocompleteForeign($raw,$meta,$field);
                }
            }
        }

        if($this->id){
            $pkey = $this->primaryKey;
            $data[$pkey] = $this->id;
        }
        
        return $data;
    }
    
    public function tagsCurrent($meta){
        if(isset($meta['habtm']['intermediate']['class']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.class> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['path']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.path> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['identifier']['me']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.identifier.me> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['identifier']['foreign']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.identifier.foreign> in KlezModel');
        }
        
        $class = $meta['habtm']['intermediate']['class'];
        $path = $meta['habtm']['intermediate']['path'];
        $me = $meta['habtm']['intermediate']['identifier']['me'];
        $foreign = $meta['habtm']['intermediate']['identifier']['foreign'];
        
        $fclass = $meta['habtm']['foreign']['class'];
        $fpath = $meta['habtm']['foreign']['path'];
        $fidentifier = $meta['habtm']['foreign']['identifier'];
        $flabel = $meta['habtm']['foreign']['label'];
        
        App::uses($class,$path);
        App::uses($fclass,$fpath);
        
        if(class_exists($class) === false){
            $this->raiseBadRequestException("Model not found<class:$class> in KlezModel");
        }
        
        $Intermediate = new $class();
        $alias = $Intermediate->alias;
        
        $FModel = new $fclass();
        $falias = $FModel->alias;
        $ftable = $FModel->useTable;
        
        $cnd = [
            "{$alias}.{$me}" => $this->id
        ];
            
        $joins = [
            "INNER JOIN {$ftable} AS {$falias} ON {$falias}.{$fidentifier}={$alias}.{$foreign}"
        ];
            
        $raws = $Intermediate->rawFind('all',[
            'fields' => "{$falias}.{$flabel},{$falias}.{$fidentifier}",
            'conditions' => $cnd,
            'joins' => $joins
        ]);
            
        $map = [];
        
        if(is_array($raws)){        
            foreach($raws as $raw){
                $tag = $raw[$falias][$flabel];
                $id = $raw[$falias][$fidentifier];
                $map[$tag] = $id;
            }
        }
        
        return $map;
    }
    
    private function findHabtmFormData($meta){
        if(isset($meta['habtm']['intermediate']['class']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.class> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['path']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.path> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['identifier']['me']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.identifier.me> in KlezModel');
        }
        
        if(isset($meta['habtm']['intermediate']['identifier']['foreign']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:intermediate.identifier.foreign> in KlezModel');
        }
        
        $class = $meta['habtm']['intermediate']['class'];
        $path = $meta['habtm']['intermediate']['path'];
        $me = $meta['habtm']['intermediate']['identifier']['me'];
        $foreign = $meta['habtm']['intermediate']['identifier']['foreign'];
        
        App::uses($class,$path);
        
        if(class_exists($class) === false){
            $this->raiseBadRequestException("Model not found<class:$class> in KlezModel");
        }
        
        $Intermediate = new $class();
        $alias = $Intermediate->alias;
            
        $fields = "{$alias}.{$foreign}";
        $cnd = [
            "{$alias}.{$me}" => $this->id
        ];
            
        $rawdata = $Intermediate->find('all',[
            'conditions' => $cnd,
            'fields' => $fields
        ]);
        
        $data = [];
        
        foreach($rawdata as $blob){
            $data[] = $blob[$foreign];
        }
        
        return [
            0 => implode(',', $data)
        ];
    }
    
    private function findTagsFormData($meta){
        $tags = $this->tagsCurrent($meta);
        $data = [];
        
        foreach($tags as $tag => $id){
            $data[] = $tag;
        }
        
        return [
            0 => implode(',', $data)
        ];
    }
    
    public function getWritableData(){
        $data = [];
        $schema = $this->provideSchema();
        
        foreach($schema as $field => $meta){
            if($this->isWritable($field) === false){
                continue;
            }
            
            if($meta['writable'] === true){
                if(isset($meta['fields'])){
                    foreach($meta['fields'] as $f){
                        $data[$f] = $this->readField($f);
                    }
                }
            }
            else if(isset($this->Data[$field]) === false){
                continue;
            }
                
            $data[$field] = $this->readWritable($field);
        }
        
        return $data;
    }
    
    public function getData(){
        $pkey = $this->primaryKey;
        $this->Data[$pkey] = $this->id;
        
        return $this->Data;
    }
    
    public function flush(){
        $this->Data = [];
        $this->id = null;
    }
    
    public function readField($field){
        if(isset($this->Data[$field])){
            return $this->Data[$field];
        }
        
        return null;
    }
    
    public function writeField($field,$value){
        $this->Data[$field] = $value;
    }
    
    public function readWritable($field){
        $methodType = null;
        $methodSubtype = null;
        $methodBoth = null;
        
        $subtype = $this->resolvSchemaSubtype($field);
        $type = $this->resolvSchemaType($field);
        $raw = $this->readField($field);
        
        if(is_null($type)){
            return $raw;
        }
        
        $camelType = Inflector::camelize($type);
        $camelSubtype = Inflector::camelize($subtype);
        
        if(strlen($camelType) > 0){
            $methodType = "readWritable{$camelType}";
        }
        
        if(strlen($camelSubtype) > 0){
            $methodSubtype = "readWritable{$camelSubtype}";
        }
        
        if(strlen($camelType) > 0 && strlen($camelSubtype) > 0){
            $methodBoth = "readWritable{$camelType}{$camelSubtype}";
        }
                
        if(is_null($methodBoth) === false && method_exists($this, $methodBoth)){
            return $this->{$methodType}($field,$raw);
        }
        else if(is_null($methodSubtype) === false && method_exists($this, $methodSubtype)){
            return $this->{$methodSubtype}($field,$raw);
        }
        else if(is_null($methodType) === false && method_exists($this, $methodType)){
            return $this->{$methodType}($field,$raw);
        }
        
        return $raw;
    }
    
    private function resolvSchemaSubtype($field){
        $schema = $this->provideSchema();
        
        if(isset($schema[$field]) === false){
            return null;
        }
        
        if(isset($schema[$field]['subtype']) === false){
            return false;
        }
        
        $subtype = $schema[$field]['subtype'];
        return $subtype;
    }
    
    private function resolvSchemaType($field){
        $schema = $this->provideSchema();
        
        if(isset($schema[$field]) === false){
            return null;
        }
        
        if(isset($schema[$field]['type']) === false){
            return false;
        }
        
        $type = $schema[$field]['type'];
        return $type;
    }
    
    private function readWritableGmap($field,$raw){
        $schema = $this->provideWritableSchema();
        $meta = $schema[$field];
        
        if(isset($meta['fields'][0]) === false){
            throw new NotImplementedException("Invalid Schema<missing.key:field[0] in KlezModel");
        }
        
        if(isset($meta['fields'][1]) === false){
            throw new NotImplementedException("Invalid Schema<missing.key:field[1] in KlezModel");
        }
        
        $latfield = $meta['fields'][0];
        $lonfield = $meta['fields'][1];
        $lat = $this->readField($latfield);
        $lon = $this->readField($lonfield);
        
        if(is_null($lat) || $lat === ''){
            return null;
        }
        
        if(is_null($lon) ||$lon === ''){
            return null;
        }
        
        return "$lat;$lon";
    }
    
    
    private function readWritableDate($field,$raw){
        if(is_null($raw) || empty($raw)){
            return null;
        }

        $time = strtotime(str_replace('/','-',$raw));
        return date('Y-m-d',$time);
    }
    
    private function readWritableImage($field,$raw){
        return $raw;
    }
    
    private function readWritableText($field,$raw){
        $schema = $this->provideSchema();
        $meta = $schema[$field];        
        $hashed = $this->resolvHashed($raw,$meta);
        return $hashed;
    }

    private function readWritablePassword($field,$raw){
        $schema = $this->provideSchema();
        $meta = $schema[$field];
        
        if(is_null($raw) && is_null($this->id) === false){
            return $this->Stored[$field];
        }
        
        $hashed = $this->resolvHashed($raw,$meta);
        return $hashed;
    }
    
    private function resolvHashed($raw,$meta){
        if(isset($meta['hash'])){
            $algorithm = $meta['hash']['algorithm'];
            
            switch ($algorithm){
                case 'md5':
                    return $this->resolvMD5Hash($raw,$meta);
            }
        }
        
        return $raw;
    }
    
    private function resolvMD5Hash($raw,$meta){
        $salted = $this->resolvSalted($raw,$meta);
        $md5 = md5($salted);
        return $md5;
    }
    
    private function resolvSalted($raw,$meta){
        if(isset($meta['hash']['salt'])){
            switch($meta['hash']['salt']['function']){
                case 'sprintf':
                    return $this->resolvSprintfSalt($raw,$meta);
            }
        }
        
        return $raw;
    }
    
    private function resolvSprintfSalt($raw,$meta){
        if(isset($meta['hash']['salt']['entropy'])){
            return sprintf($meta['hash']['salt']['entropy'],$raw);
        }
        
        return $raw;
    }
    
    public function loadById($id){
        $this->flush();
        $pkey = $this->primaryKey;
        $alias = $this->alias;
        
        $cnd = [];
        $cnd["{$alias}.{$pkey}"] = $id;
        
        $data = $this->find('first',[
            'conditions' => $cnd
        ]);
        
        if($data){
            $this->loadArray($data);
            return true;
        }
        
        return false;
    }
    
    
    public function loadByConditions($cnd){
        $this->flush();
        $data = $this->find('first',[
            'conditions' => $cnd
        ]);
        
        if($data){
            $this->loadArray($data);
            return true;
        }
        
        return false;
    }
    
    public function findReadableById($id,$foreignScanner = false){
        $alias = $this->alias;
        $pkey = $this->primaryKey;
        $type = 'first';
        
        $cnd = [];
        $cnd["{$alias}.{$pkey}"] = $id;
        
        $query = [
            'conditions' => $cnd
        ];
        
        $raw = $this->findReadable($type,$query,$foreignScanner);
        
        return $raw;
    }
    
    private function findForeignData($raw){
        $schema = $this->provideReadableSchema();
        $foreign = [];
        
        foreach($schema as $field => $meta){
            if($this->resolvSchemaType($field) !== 'foreign'){
                continue;
            }
            
            $foreign[$field] = $this->findForeign($raw,$meta,$field); 
        }
        
        if(empty($foreign) === false){
            $raw['***foreign***'] = $foreign;
        }
        
        return $raw;
    }
    
    private function findForeign($raw,$meta,$field){
        switch($meta['subtype']){
            case 'autocomplete':
                return $this->findAutocompleteForeign($raw,$meta,$field);
            case 'habtm':
                throw new NotImplementedException();
            case 'tags':
                return $this->findTagsForeign($raw,$meta,$field);
        }
        
        return null;
    }
    
    private function findTagsForeign($raw,$meta,$field){
        $tags = $this->tagsCurrent($meta);
        $data = [];
        
        foreach($tags as $tag => $id){
            $data[] = $tag;
        }
        
        return $data;
    }
    
    private function findAutocompleteForeign($raw,$meta,$field) {
        if(array_key_exists($field,$raw) === false){
            $this->raiseBadRequestException("Invalid Data<missing.field:{$field}> in KlezModel");
        }
        
        $value = $raw[$field];
        
        $id = (int) $value;
        
        if($id <= 0){
            return null;
        }
        
        if(isset($meta['autocomplete']['class']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.class> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['path']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.path> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['label']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.label> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['identifier']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.identifier> in KlezModel');
        }
        
        $class = $meta['autocomplete']['class'];
        $path = $meta['autocomplete']['path'];
        $label = $meta['autocomplete']['label'];
        $identifier = $meta['autocomplete']['identifier'];
        
        App::uses($class,$path);
        
        if(class_exists($class) === false){
            $this->raiseBadRequestException("Model not found<class:$class> in KlezModel");
        }
        
        $Child = new $class();
        $alias = $Child->alias;
        $cnd = [];
        $cnd["{$alias}.{$identifier}"] = $id;
        
        $rawdata = $Child->find('first',[
            'conditions' => $cnd,
            'fields' => "{$alias}.{$label}"
        ]);
        
        if($rawdata){
            return $rawdata[$label];
        }
        
        return null;
    }
    
    public function findPrimaryKeyForeign($field,$resolver,$value,$cnd = []) {
        $schema = $this->provideSchema();
        $meta = $schema[$field];
                
        if(isset($meta['autocomplete']['class']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.class> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['path']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.path> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['label']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.label> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['identifier']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.identifier> in KlezModel');
        }
        
        $class = $meta['autocomplete']['class'];
        $path = $meta['autocomplete']['path'];
        $label = $meta['autocomplete']['label'];
        
        App::uses($class,$path);
        
        if(class_exists($class) === false){
            $this->raiseBadRequestException("Model not found<class:$class> in KlezModel");
        }
        
        $Child = new $class();
        $alias = $Child->alias;
        $cnd["{$alias}.{$resolver}"] = $value;
        $pk = $Child->primaryKey;
        
        $rawdata = $Child->find('first',[
            'conditions' => $cnd,
            'fields' => "{$alias}.{$pk}, {$alias}.{$label}"
        ]);
        
        if($rawdata){
            return $rawdata;
        }
        
        return null;
    }
    
    public function findWritable($type = 'first', $query = [], $foreignScanner = false){
        $fields = $this->getWritableFields();
        
        if(isset($query['fields'])){
            $this->raiseBadRequestException('Invalid Params<WillBeOverwritten:query.fields> in KlezModel');
        }
        
        $query['fields'] = $fields;
        $data = $this->find($type,$query);
        
        if($foreignScanner){
            return $this->foreignScanner($type,$data);
        }
        
        return $data;
    }
    
    public function findReadable($type = 'first', $query = [], $foreignScanner = false){
         if(isset($query['fields']) === false){ 
            $fields = $this->getReadableFields();
            $query['fields'] = $fields;
        }

        try{
            $data = $this->find($type,$query);
        }
        catch(\Exception $e){
            error_log(serialize($e->getMessage()));
        }
        
        if($foreignScanner){
            $data = $this->foreignScanner($type,$data);
        }
        
        return $this->fileScanner($type,$data);
    }
    
    private function fileScanner($type,$data){
        if(empty($data)){
            return $data;
        }
        
        switch($type){
            case 'all':
                foreach($data as $i => $raw){
                    $data[$i] = $this->fileResolver($raw);
                }
                
                break;
            case 'first':
                $data = $this->fileResolver($data);
                break;
        }
        
        return $data;
    }
    
    private function fileResolver($raw){
        $schema = $this->provideReadableSchema();
        $files = [];
        
        foreach($schema as $field => $meta){
            if($this->resolvSchemaType($field) !== 'file'){
                continue;
            }
            
            $files[$field] = $this->resolvFileMetadata($raw,$meta,$field); 
        }
        
        if(empty($files) === false){
            $raw['***files***'] = $files;
        }
        
        return $raw;
    }
    
    private function resolvFileMetadata($raw,$meta,$field){
        $exists = $raw[$field] > 0;
        $url = $this->resolvFileUrl($raw,$meta);
        
        return [
            'url' => $url,
            'exists' => $exists
        ];
    }
    
    private function resolvFileUrl($raw,$meta){
        if(array_key_exists('url',$meta)){
            $url = $meta['url'];
            
            if(array_key_exists('id',$meta['url'])){ 
                if(is_null($meta['url']['id'])){
                    unset($meta['url']['id']);
                }
            }       
            else{
                $url['id'] = $raw['id']; 
            }
            
            return Router::url($url, true);
        }
        
        return null;
    }
    
    private function foreignScanner($type,$data){
        if(empty($data)){
            return $data;
        }
        
        switch($type){
            case 'all':
                foreach($data as $i => $raw){
                    $data[$i] = $this->findForeignData($raw);
                }
                
                break;
            case 'first':
                $data = $this->findForeignData($data);
                break;
        }
        
        return $data;
    }
    
    private function getWritableFields(){
        $fields = "";
        $alias = $this->alias;
        $schema = $this->provideWritableSchema();
        
        foreach($schema as $field => $meta){
            if(@$meta['subtype'] === 'habtm'){
                continue;
            }
            
            if(@$meta['subtype'] === 'tags'){
                continue;
            }
            
            $buffer = [];
            
            if(isset($meta['fields'])){
                foreach($meta['fields'] as $f){
                    $buffer[] = $f;
                }
            }
            else{
                $buffer[] = $field;
            }
            
            foreach($buffer as $bufferField){
                if(strlen($fields) === 0){
                    $fields .= "{$alias}.{$bufferField}";
                }
                else{
                    $fields .= ", {$alias}.{$bufferField}";                
                }
            }
        }
        
        return $fields;
    }
    
    public function getReadableFields(){
        $fields = "";
        $alias = $this->alias;
        $schema = $this->provideReadableSchema();
        
        foreach($schema as $field => $meta){
            if (@$meta['subtype'] === 'habtm'){
                continue;
            }
            
            if (@$meta['subtype'] === 'tags'){
                continue;
            }
            
            $buffer = [];
            
            if(isset($meta['fields'])){
                foreach($meta['fields'] as $f){
                    $buffer[] = $f;
                }
            }
            else{
                $buffer[] = $field;
            }
            
            foreach($buffer as $bufferField){
                if(strlen($fields) === 0){
                    $fields .= "{$alias}.{$bufferField}";
                }
                else{
                    $fields .= ", {$alias}.{$bufferField}";                
                }
            }
        }
        
        return $fields;
    }
    
    public function provideWritableSchema(){
        $schema = $this->provideSanitizedSchema();
        $pkey = $this->primaryKey;
        $writable = [];
        $writable[$pkey] = $this->providePrimaryKeyMetadata();
        
        foreach($schema as $field => $meta){
            if($this->isWritable($field) === false){
                continue;
            }
            
            $writableMeta = $this->resolvWritableMeta($meta);
            $writable[$field] = $writableMeta;
        }
        
        return $writable;
    }

    private function resolvWritableMeta($meta) {
        switch($meta['type']){
            case 'foreign':
                return $this->resolvForeignWritableMeta($meta);
        }
        
        return $meta;
    }
    
    private function resolvForeignWritableMeta($meta) {
        switch($meta['subtype']){
            case 'autocomplete':
                return $this->resolvAutocompleteWritableMeta($meta);
            case 'habtm':
                return $this->resolvHabtmWritableMeta($meta);
        }
        
        return $meta;
    }
    
    private function resolvHabtmWritableMeta($meta) {
        $query = [];
        
        if(isset($meta['habtm']['foreign']['class']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:habtm.foreign.class> in KlezModel');
        }
        
        if(isset($meta['habtm']['foreign']['path']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:habtm.foreign.path> in KlezModel');
        }
        
        if(isset($meta['habtm']['foreign']['label']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:habtm.foreign.label> in KlezModel');
        }
        
        if(isset($meta['habtm']['foreign']['identifier']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:habtm.foreign.identifier> in KlezModel');
        }
        
        if(isset($meta['habtm']['foreign']['query'])){
            $query = $meta['habtm']['foreign']['query'];
        }
        
        $class = $meta['habtm']['foreign']['class'];
        $path = $meta['habtm']['foreign']['path'];
        $label = $meta['habtm']['foreign']['label'];
        $identifier = $meta['habtm']['foreign']['identifier'];
        
        App::uses($class,$path);
        
        if(class_exists($class) === false){
            $this->raiseBadRequestException("Model not found<class:$class> in KlezModel");
        }
        
        $Child = new $class();
        $rawdata = $Child->find('all', $query);
        $options = [];
        
        foreach($rawdata as $data){
            $id = $data[$identifier];
            $lbl = $data[$label];
            $options[$id] = $lbl;
        }
        
        $meta['options'] = $options;
        unset($meta['habtm']);
        
        return $meta;
    }
    
    private function resolvAutocompleteWritableMeta($meta) {
        $query = [];
        
        if(isset($meta['autocomplete']['class']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.class> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['path']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.path> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['label']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.label> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['identifier']) === false){
            $this->raiseBadRequestException('Invalid Params<missing.key:autocomplete.identifier> in KlezModel');
        }
        
        if(isset($meta['autocomplete']['query'])){
            $query = $meta['autocomplete']['query'];
        }
        
        $class = $meta['autocomplete']['class'];
        $path = $meta['autocomplete']['path'];
        $label = $meta['autocomplete']['label'];
        $identifier = $meta['autocomplete']['identifier'];
        
        App::uses($class,$path);
        
        if(class_exists($class) === false){
            $this->raiseBadRequestException("Model not found<class:$class> in KlezModel");
        }
        
//        $Child = new $class();
//        $rawdata = $Child->find('all', $query);
//        $options = [];
//        
//        foreach($rawdata as $data){
//            $id = $data[$identifier];
//            $lbl = $data[$label];
//            $options[$id] = $lbl;
//        }
//        
//        $meta['options'] = $options;
        
        unset($meta['autocomplete']);
        
        return $meta;
    }
    
    public function provideReadableSchema(){
        $schema = $this->provideSanitizedSchema();
        $pkey = $this->primaryKey;
        
        $readable = [];
        $primary = $this->providePrimaryKeyMetadata();
        
        if(isset($primary['readable']) && $primary['readable'] === true){
            $readable[$pkey] = $primary;
        }
        
        foreach($schema as $field => $meta){
            if($this->isReadable($field) === false){
                continue;
            }
            
            $readable[$field] = $meta;
        }
        
        
        return $readable;
    }
    
    public function isWritable($field){
        $schema = $this->provideSchema();
        
        if(isset($schema[$field]) === false){
            return false;
        }
        
        if(isset($schema[$field]['writable']) === false){
            return false;
        }
        
        if($schema[$field]['writable'] === true){
            return true;
        }
        
        
        return false;
    }
    
    public function isReadable($field){
        $schema = $this->provideSchema();
        
        if(isset($schema[$field]) === false){
            return false;
        }
        
        if(isset($schema[$field]['readable']) === false){
            return false;
        }
        
        if($schema[$field]['readable'] === true){
            return true;
        }
        
        return false;
    }
    public function rawFind($type = 'first', $query = []) {
        $raw = parent::find($type, $query);
        
        if($raw === false || empty($raw)){
            return false;
        }
        
        return $raw;
    }
    
    public function find($type = 'first', $query = []) {
        $raw = parent::find($type, $query);
        
        if($raw === false || empty($raw)){
            return false;
        }
        
        switch($type){
            case 'count':
                return $this->findCountCallback($raw);
            case 'first':
                return $this->findFirstCallback($raw);
            case 'all':
                return $this->findAllCallback($raw);
        }
        
        $this->raiseBadRequestException("Invalid Param <type:$type> in KlezModel");
    }
    
    protected function loadByField($field,$value){
        $alias = $this->alias;
        
        $cnd = [];
        $cnd["{$alias}.{$field}"] = $value;
        
        $opts = [];
        $opts['conditions'] = $cnd;
        
        if($this->find('first', $opts)){
            return true;
        }
        
        return false;
    }
    
    private function findFirstCallback($raw){
        $alias = $this->alias;
        
        if(isset($raw[$alias]) === false){
            return [];
        }
        
        $data = $raw[$alias];
        $this->loadArray($data);
        return $data;
    }
    
    private function findAllCallback($raw){
        $alias = $this->alias;
        $data = [];
        
        foreach($raw as $row){
            if(isset($row[$alias]) === false){
                continue;
            }
            
            $data[] = $row[$alias];
        }
        
        return $data;
    }
    
    private function findCountCallback($raw){
        return $raw;
    }
    
    public function block($key,$retry=3,$timeout = 10){        
        if($retry > 1){
            $sleep = (int)($timeout / ($retry - 1));
        }
        else{
            $sleep = 0;
        }

        do{                
            if($retry <= 0){
                return false;
            }

            $status = $this->getMutexLock($key,$timeout);

            if (!$status) {
                sleep($sleep);
            }

            $retry--;
        }
        while(!$status);

        return true;
    }
    
    public function unblock($key){        
        $status = $this->releaseMutexLock($key);
        return $status;
    }
    
    private function getMutexLock($key,$timeout=30){   
        $sql = "SELECT GET_LOCK('{$key}',$timeout) as status";

        $response = $this->query($sql);
        return @$response[0][0]['status'];
    }
    
    
    public function truncate(){
        $this->query('TRUNCATE TABLE ' . $this->useTable);
    }   

    private function releaseMutexLock($key){        
        $sql = "SELECT RELEASE_LOCK('{$key}') as status";

        $response = $this->query($sql);
        return @$response[0][0]['status'];
    }
    
    function getLock($key,$timeout=10){   
        $dbo = $this->getDataSource();
        $sql = "SELECT GET_LOCK('{$key}',$timeout) as status";
           
        $response = $dbo->query($sql);
        return @$response[0][0]['status'];
    }
    
    function releaseLock($key){        
        $dbo = $this->getDataSource();
        $sql = "SELECT RELEASE_LOCK('{$key}') as status";
        $response = $dbo->query($sql);
        return @$response[0][0]['status'];
    }
    
    public function cleanUnusedDeps(){
        foreach($this->provideWritableSchema() as $field => $meta){
            if($this->isGroup($meta)){
                if($this->isDep($meta)){
                    $v = $this->readField($field);
                    $fields = $meta['group']['dep'][$v];
                    
                    foreach($fields as $field){
                        $this->writeField($field, '');
                    }
                }
            }
        }
    }
}