<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="row">
                <?php $this->Delete->dataCards($schema, $data); ?>
            </div>
        </div>             
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        <?php $this->Delete->submit(); ?>
                    </div>
                </div>
            </div>   
        </div>             
    </div>
</div>