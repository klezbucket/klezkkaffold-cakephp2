<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class ShowHelper extends KlezkaffoldHelper {
    public function deploy($config){
        echo $this->element('main',[
            'config' => $config,
        ]);
    }
    
    public function showActions($links){
        $metalinks = $this->resolvMetalinks($links);
        
        if(empty($metalinks) === false){
            $this->dataLinks($metalinks);
        }
    }
    
    public function actionsHolder($config){
        if(isset($config['links']) === false){
            $config['links'] = [];
        }
        
        if(empty($config['links'])){
            return;
        }
        
        echo $this->element('actions', $config);
    }
    
    public function main($config){
        if(isset($config['data']) === false){
            $this->raiseInternalServerError("No Conf<Show:data> in ShowHelper");
        }
        
        $data = $config['data'];
        
        if(empty($data)){
            $this->emptyData();
        }
        else{
            $this->tableData($config);
        }
    }
    
    private function tableData($config){
        if(isset($config['page']) === null){            
            $this->raiseInternalServerError("No Conf<Show:page> in ShowHelper");
        }
        
        if(isset($config['count']) === false){   
            $this->raiseInternalServerError("No Conf<Show:count> in ShowHelper");
        }
        
        if(isset($config['schema']) === false){   
            $this->raiseInternalServerError("No Conf<Show:schema> in ShowHelper");
        }
        
        if(isset($config['limit']) === false){   
            $this->raiseInternalServerError("No Conf<Show:schema> in ShowHelper");
        }
        
        $data = $config['data'];
        $schema = $config['schema'];
        $count = $config['count'];
        $page = $config['page'];
        $limit = $config['limit'];
        
        echo $this->element('table',[
            'data' => $data,
            'schema' => $schema,
            'page' => $page,
            'count' => $count,
            'limit' => $limit,
            'c' => count($data)
        ]);
    }
    
    public function resolvPaginationUrl($page){
        $url = $this->resolvCurrentUrl();
        $purl = parse_url($url);
        
        if(isset($purl['query'])){
            parse_str($purl['query'],$purl['query']);
        }
        else{
            $purl['query'] = [];
        }
        
        $purl['query']['p'] = $page;
        $paginationUrl = $this->buildUrl($purl);
        return $paginationUrl;
    }
    
    public function pagination($page,$count,$c,$limit){
        echo $this->element('pagination',[
            'page' => $page,
            'count' => $count,
            'c' => $c,
            'limit' => $limit
        ]);
    }
    
    public function numbers($page,$count,$limit){
        $last = $this->resolvLastPage($count, $limit);
        $spread = $this->_View->Backend->feed('pagination.spread');
        $a = $page - $spread;
        
        if($a < 0){
            $spread += abs($a);
            $a = 1;
        }
        
        $b = $page + $spread;
        
        if($b > $last){
            $b = $last;
        }
        
        for($p = $a;$p <= $b;$p++){
            $url = $this->resolvPaginationUrl($p);
            $active = '';
            
            if($p === $page){
                $active = 'active';
            }
            
            if ($p == 0){
                continue;
            }
            
            echo $this->element('number',[
                'page' => $p,
                'url' => $url,
                'active' => $active,
            ]);
        }
    }
    
    public function nextButton($page,$count,$limit){
        $disabled = '';
        $url = '#';
        $lastPage = $this->resolvLastPage($count,$limit);
        
        if($page >= $lastPage){
            $disabled = 'disabled';
        }
        else{
            $url = $this->resolvPaginationUrl($page + 1);
        }
        
        echo $this->element('next',[
            'disabled' => $disabled,
            'url' => $url
        ]);
    }
    
    public function resolvLastPage($count,$limit){
        $last = ceil($count / $limit);
        return $last;
    }
    
    public function backButton($page){
        $disabled = '';
        $url = '#';
        
        if($page === 1){
            $disabled = 'disabled';
        }
        else{
            $url = $this->resolvPaginationUrl($page - 1);
        }
        
        echo $this->element('back',[
            'disabled' => $disabled,
            'url' => $url
        ]);
    }
    
    public function resolvPaginationSummary($count,$c){
        $summary = $this->_View->Backend->feed('pagination.summary');
        return sprintf($summary, $c, $count);
    }
    
    private function emptyData(){
        echo $this->element('empty');
    }
    
    public function provideLogtag() {
        return 'Show';
    }
    
    public function tableHead($schema,$links = []){
        echo $this->element('table_head',[
            'schema' => $schema,
            'links' => $links
        ]);
    }
    
    public function tableHeadCells($schema,$links){
        foreach($schema as $field => $meta){
            if($this->isListable($meta) === false){
                continue;
            }
            
            $label = $this->resolvLabel($meta);
            
            echo $this->element('table_head_cell',[
                'field' => $field,
                'label' => $label
            ]);
        }
            
        if(empty($links) === false){
            echo $this->element('table_head_cell',[
                'label' => 'Acciones'
            ]);
        }
    }
    
    public function tableBody($schema,$data){
        echo $this->element('table_body',[
            'data' => $data,
            'schema' => $schema
        ]);
    }
    
    public function tableRows($schema,$data){
        foreach($data as $datalink){
            echo $this->element('table_row',[
                'row' => $datalink['data'],
                'links' => $datalink['links'],
                'schema' => $schema
            ]);            
        }
    }
    
    public function actions($links){
        foreach($links as $link){
            $controller = $link['controller'];
            $action = $link['action'];
            $sitemap = $this->resolvSitemap($controller,$action);
            $url = Router::url($link);

            echo $this->element('action',[
                'title' => $sitemap['h1'],
                'icon' => $sitemap['icon'],
                'url' => $url
            ]);    
        }
    }
    
    public function tableCells($schema,$row,$links){
        foreach($schema as $field => $meta){
            if($this->isListable($meta) === false){
                continue;
            }
            
            $value = $this->resolvValue($field,$meta,$row);
            
            echo $this->element('table_cell',[
                'field' => $field,
                'value' => $value
            ]);            
        }
        
        if(empty($links) === false){
            echo $this->element('table_cell_actions',[
                'links' => $links
            ]);            
        }
    }
}