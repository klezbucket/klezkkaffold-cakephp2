<?php

App::uses('KlezBackendHelper', 'KlezBackend.View/Helper');

class ApiHelper extends KlezBackendHelper {
    public function provideLogtag() {
        return 'Api Feed';
    }

    public function provideVarname() {
        return API_DATA;
    }
}