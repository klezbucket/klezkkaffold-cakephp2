<?php

abstract class AuthMechanismComponent extends Component{
    private $Config;
    
    public function init($config){
        $this->Config = $config;
    }
    
    protected function getConfig() {
        return $this->Config;
    }
    
    abstract function parseConf();
    abstract function write($id,$data);
    abstract function read($id);
    abstract function delete($id);
    abstract function renew($id);
}