<div class="input-group m-t-10">
    <input 
        autocomplete="off"
        autocorrect="off"
        class="select2-input select2-default" 
        placeholder="<?=$placeholder?>"
        data-role="tagsinput" 
        name="<?=$field?>[]" 
        value="<?=$value?>" />
    
    <span class="input-group-addon"><i class="fa fa-tags"></i></span>
</div>

<style>
    .bootstrap-tagsinput{
        width:100%;
    }
    .bootstrap-tagsinput input{
        width:100%;
    }
</style>