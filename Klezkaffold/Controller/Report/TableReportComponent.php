<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class TableReportComponent extends KlezkaffoldComponent{
    private $data = [];
    private $schema = [];
    private $config = [];
    private $resolv = false;
    
    public function output() {
        $this->config['data'] = $this->data;
        $this->config['schema'] = $this->schema;
        
        return $this->config;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
    }
    
    public function process(){
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
        
        $this->schema = $this->getModel()->provideReadableSchema();
        $this->data = $this->getModel()->findReadable('all',$this->query,true);
        
        if($this->resolv === true){
            $this->resolv();
        }
    }
    
    private function resolv(){
        if(empty($this->data) === true){
            return;
        }
        
        foreach($this->data as &$row){
            if(isset($row['***foreign***'])){
                $row = array_merge($row,$row['***foreign***']);
                unset($row['***foreign***']);
            }
            
            foreach($row as $field => $value){
                $row[$field] = $this->resolvValue($field,$value);
            }
        }
    }
    
    private function resolvValue($field,$value){
        $meta = $this->schema[$field];
        
        switch($meta['type']){
            case 'boolean':
                return $this->resolvBooleanValue($meta,$value);
            case 'options':
                return $this->resolvOptionsValue($meta,$value);
        }
        
        return $value;
    }

    private function resolvOptionsValue($meta,$value){
        if(isset($meta['options'][$value])){
            return $meta['options'][$value];
        }
        
        return $value;
    }

    private function resolvBooleanValue($meta,$value){
        $v = (int) $value;
        
        if(isset($meta['boolean'][$v])){
            return $meta['boolean'][$v];
        }
        
        return $value;
        
    }

    public function parseConfig($config){
        if(isset($config['data']['query']) === false){
            $this->raiseConfigureException("No Conf <Report:data.query> in Klezkaffold Config");
        }
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        if(isset($config['links']) === true){
            $this->links = $config['links'];
        }
        
        if(isset($config['data']['resolv']) === true){
            $this->resolv = $config['data']['resolv'];
        }
        
        $this->config = $config;
        $this->query = $config['data']['query'];
        $this->loadModel($config['data']);
    }
}