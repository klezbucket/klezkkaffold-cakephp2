<script>
    $(function(){
        var master = <?=json_encode($master);?>;
        var dep = <?=json_encode($dep);?>;
        var value = <?=json_encode($value);?>;
        var holders = {};

        for(var group in dep){
            for(var i in dep[group]){
                var field = dep[group][i];
                var holder = $('.data-card-' + field);
                holders[field] = holder;
                holder.hide();
            }
        }
        
        if(typeof dep[value] !== 'undefined'){
            for(var i in dep[value]){
                var field = dep[value][i];
                var h = holders[field];
                console.debug(field)
                h.show();
            }
        }
    });
</script>