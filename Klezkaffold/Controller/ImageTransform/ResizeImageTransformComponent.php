<?php

App::uses('ImageTransformComponent','Klezkaffold.Controller/ImageTransform');

class ResizeImageTransformComponent extends ImageTransformComponent{
    private $width;
    private $height;

    public function processImage($res) {
        if($this->width <= 0){
            $this->width = $this->calcWidth($res,$this->height);
        }
        
        if($this->height <= 0){
            $this->height = $this->calcHeight($res,$this->width);
        }
        
        $h = imagesy($res);
        $w = imagesx($res);
        $dimg = imagecreatetruecolor($this->width, $this->height); 
        imagealphablending($dimg, FALSE);
        imagesavealpha($dimg, TRUE);
        imagecopyresampled($dimg, $res, 0, 0, 0, 0, $this->width, $this->height, $w, $h);
        return $dimg;
    }
    
    private function calcHeight($res,$width){
        $h = imagesy($res);
        $w = imagesx($res);
        
        return $width * $h / $w;
    }
    
    private function calcWidth($res,$height){
        $h = imagesy($res);
        $w = imagesx($res);
        
        return $height * $w / $h;
    }

    public function validateConfig($config) {
        if(isset($config['w']) === false && isset($config['h']) === false){
            $this->logimagetrans('Resize expected param<w:width target in pixels OR h:height target in pixels> in ResizeImageTransform');
        }
        
        $this->width = (int) $config['w'];
        $this->height = (int) $config['h'];
        
        if($this->width <= 0 && $this->height <= 0){
            $this->logimagetrans('Resize expected param<w:width > 0px OR h:height > 0px> in ResizeImageTransform');
        }
    }
}