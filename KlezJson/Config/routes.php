<?php

Router::connect('/json/master.html', [ 'controller' => 'jsons', 'action' => 'current','plugin' => 'KlezJson' ]);
Router::connect('/json/generate.json', [ 'controller' => 'jsons', 'action' => 'generate','plugin' => 'KlezJson' ]);
Router::connect('/json/master.json', [ 'controller' => 'jsons', 'action' => 'master','plugin' => 'KlezJson' ]);

Router::connect('/json/generate.json', [ 
    'controller' => 'server', 
    'action' => 'index', 
    'plugin' => 'KlezApi',
    'endpoint' => 'json_generate',
    'format' => 'json'
]);

Router::connect('/json/metadata.json',[ 
    'controller' => 'server', 
    'action' => 'index', 
    'plugin' => 'KlezApi',
    'endpoint' => 'json_metadata',
    'format' => 'json'
]);

Router::connect('/json/master.json',  [ 
    'controller' => 'server', 
    'action' => 'index', 
    'plugin' => 'KlezApi',
    'endpoint' => 'json_master',
    'format' => 'json'
]);