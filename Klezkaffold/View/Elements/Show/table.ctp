<div class="col-sm-12">
    <div class="card-box table-responsive">
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">            
            <?php $this->Show->tableHead($schema,$data[0]['links']); ?>
            <?php $this->Show->tableBody($schema,$data); ?>
        </table>
        
        <div class="row">
            <?php $this->Show->pagination($page,$count,$c,$limit); ?>
        </div>
    </div>
</div>