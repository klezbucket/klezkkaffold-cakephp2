<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class DashboardHelper extends KlezkaffoldHelper {
    public function deploy($config){
        echo $this->element('main',[
            'config' => $config,
        ]);
    }
    
    private $status = null;
    
    public function open(){
        if($this->status === 'close' || is_null($this->status)){
            $this->status = 'open';
            echo $this->element('open');
        }
    }
    
    public function close(){
        if($this->status === 'open' || is_null($this->status)){
            $this->status = 'close';
            echo $this->element('close');
        }
    }
    
    public function main($config){
        if(empty($config)){
            return;
        }
        
        $this->open();
        $c = 0;
        
        foreach($config as $name => $conf){
            if(isset($conf['type']) === false){
                continue;
            }
            
            $type = $conf['type'];
            $method = Inflector::camelize($type) . 'Type';
            $counter = Inflector::camelize($type) . 'Count';
            
            if(method_exists($this, $method) === false){
                continue;
            }
            
            if(method_exists($this, $counter) === false){
                continue;
            }
            
            $this->_View->viewVars[KLEZKAFFOLD_DATA] = $this->{$method}($conf);
            $c += $this->{$counter}();
            
            echo $this->element($conf['type'],[
                'name' => $name
            ]);
            
            if($c >= 4){
                $this->close();
                $c = 0;
                
                $this->open();
            }
        }
        
        $this->close();
    }
    
    private function tableCount(){
        return 4;
    }
    
    private function tableType($conf){
        print_r($conf);
        return $conf;
    }
    
    private function holdersCount(){
        return 1;
    }
    
    private function holdersType($conf){
        return $conf;
    }
    
    private function lineGraphCount(){
        return 4;
    }
    
    private function lineGraphType($conf){
        $series = [];
        
        foreach($conf['data']['series'] as $name => $serie){
           $series[] = [
               'name' => $serie['label'],
               'data' => array_reverse($serie['points'])
           ];
        }
        
        return [
            'labels' => array_reverse($conf['data']['labels']),
            'series' => $series,
            'title' => $conf['title'],
            'url' => $conf['url']
        ];
    }

    private function sumGraphCount(){
        return 4;
    }
    
    private function sumGraphType($conf){
        $series = [];
        
        foreach($conf['data']['series'] as $name => $serie){
           $series[] = [
               'name' => $serie['label'],
               'data' => array_reverse($serie['points'])
           ];
        }
        
        return [
            'labels' => array_reverse($conf['data']['labels']),
            'series' => $series,
            'title' => $conf['title'],
            'url' => $conf['url']
        ];
    }
    private function summaryNumberType($conf){
        return $conf;
    }
    
    private function summaryNumberCount(){
        return 1;
    }
    
    private function summaryPieCount(){
        return 1;
    }
    
    private function summaryPieType($conf){
        $total = $conf['data']['total'];
        $count = $conf['data']['count'];
        $perc = 0;
        
        if($total > 0){
            $perc = 100 * $count / $total;
        }
        
        $conf['data']['perc'] = $perc;
        $conf['fgcolor'] = $this->resolvFgColor($conf);
        $conf['bgcolor'] = $this->resolvBgColor($conf);
        
        return $conf;
    }
    
    private function resolvBgColor($conf){
        $perc = $conf['data']['perc'];
        
        if($perc >= 75){
            return '#AAE2C6';
        }
        else if($perc >= 50){
            return '#B8E6F4';
        }
        else if($perc >= 25){
            return '#FFE6BA';
        }
        else{
            return '#F9B9B9';
        }
    }
    
    private function resolvFgColor($conf){
        $perc = $conf['data']['perc'];
        
        if($perc >= 75){
            return '#10c469';
        }
        else if($perc >= 50){
            return '#35b8e0';
        }
        else if($perc >= 25){
            return '#ffbd4a';
        }
        else{
            return '#f05050';
        }
    }

    public function provideLogtag() {
        return 'Dashboard';
    }
}