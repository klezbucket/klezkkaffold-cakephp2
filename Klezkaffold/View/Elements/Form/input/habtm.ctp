<select
    class="select2 select2-multiple"
    multiple="multiple" 
    multiple 
    name="<?=$field?>_habtm"
    data-placeholder="<?=$placeholder?>">
    <?php $this->Form->options($options,explode(',', $value)); ?>
</select>

<input type="hidden" name="<?=$field?>[]" value="<?=$value?>" />

<script>
    $(function(){
        var field = <?=json_encode($field)?>;
        var select = $('select[name="' + field + '_habtm"]');
        var input = $('input[name="' + field + '[]"]');
        
        select.select2({
            formatNoMatches: function() {
                return 'No hay resultados';
            }
        }).on('change', function() {
            input.val($(this).val());
        });
    });
</script>