<?php

abstract class AuthEntityComponent extends Component{
    private $Config;
    
    public function init($config){
        $this->Config = $config;
    }
    
    protected function getConfig() {
        return $this->Config;
    }
    
    abstract function parseConf();
    abstract function provideData();
    abstract function loadData($data);
    abstract function resolvAuthentication($data);
    abstract function provideCommitData();
    abstract function provideUniqueIdentificator();
}