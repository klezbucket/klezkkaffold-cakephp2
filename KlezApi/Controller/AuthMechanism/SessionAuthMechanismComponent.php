<?php

App::uses('AuthMechanismComponent', 'KlezApi.Controller/AuthMechanism');

class SessionAuthMechanismComponent extends AuthMechanismComponent{
    public function parseConf() {
        $config = $this->getConfig();
        
        if(isset($config['mechanism']['key']) === false){
            throw new ConfigureException('No Conf<SessionAuthMechanism.loginField> in DatabaseAuth Config');
        }
    }
    
    private function resolvKey($id = null){
        $config = $this->getConfig();
        $key = $config['mechanism']['key'];
        return $key;
    }

    public function delete($id = null) {
        $key = $this->resolvKey($id);
        return CakeSession::delete($key);
    }

    public function read($id = null) {
        $key = $this->resolvKey($id);
        return CakeSession::read($key);
    }

    public function write($id, $data) {
        $key = $this->resolvKey($id);
        
        return CakeSession::write($key,[
            'id' => $id,
            'data' => $data
        ]);
    }

    public function renew($id = null) {
        CakeSession::renew();
    }
}