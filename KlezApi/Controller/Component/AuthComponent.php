<?php

App::uses('BaseComponent', 'KlezApi.Controller/Component');

class AuthComponent extends BaseComponent {
    private $Config;
    private $Entity;
    private $Mechanism;
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        $this->initializeLog();
    }
    
    public function load($config,$mechanismExtras = []){
        if($config === false){
            $this->logauth('Auth<NOT REQUIRED>');
        }
        else{
            $this->logauth("Auth<required:$config>");
            $this->loadConfig($config,$mechanismExtras);
            $this->mechanismToEntity();
        }
    }
    
    private function raiseConfigureException($message){
        $this->logauth($message);
        throw new ConfigureException($message);
    }
    
    public function hasSession(){
        $id = $this->Entity->provideUniqueIdentificator();
        
        if(is_null($id) || $id === false || empty($id)){
            return false;
        }
        
        return true;
    }
        
    private function mechanismToEntity(){
        $data = $this->Mechanism->read();
        
        if(is_null($data) === false){
            $this->logauth("Auth<mechanismToEntity:load>");
            $this->Entity->loadData($data);
        }
        else{
            $this->logauth("Auth<mechanismToEntity:empty>");
        }
    }
    
    private function initializeLog(){
        $file = 'klezauth';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'auth' ],
            'file' => $file
        ));    
    }
    
    public function logauth($message){
        $fullMessage = "[{$this->getLogHash()}] {$message}";
        CakeLog::write('debug', $fullMessage, 'auth');
    }
    
    public function loadConfig($config,$mechanismExtras = []){
        Configure::load('klezauth','default',false);
        
        $confiles = Configure::read('Auth.confiles');
        
        if(is_null($confiles)){            
            $this->raiseConfigureException('No Conf<Auth:confiles> in Auth Config');
        }
        
        if(is_array($confiles) === false){
            $confiles = [ $confiles ];
        }
        
        foreach($confiles as $confile){
            Configure::load($confile,'default',false);
        }
        
        $this->parseConf($config);
        $this->loadEntity($config);
        $this->loadMechanism($config,$mechanismExtras);
    }
    
    private function parseConf($conf){
        $key = "Auth.{$conf}";
        $this->Config = Configure::read($key);
        
        if(is_null($this->Config) === true){
            $this->raiseConfigureException("No Conf<AUTH> for $conf");
        }
    }
    
    private function loadEntity($conf){
        $location = 'KlezApi.Controller/AuthEntity';
        
        if(isset($this->Config['entity']['driver']) === false){           
            $this->raiseConfigureException("No Conf<$conf.entity.driver> in Auth Config");
        }
        
        if(isset($this->Config['entity']['driver-location'])){  
            $location = $this->Config['entity']['driver-location'];
        }
    
        $entity = Inflector::camelize($this->Config['entity']['driver']);
        $this->logauth("Auth<entity:$entity>");
        $class = "{$entity}AuthEntityComponent";
        
        App::uses($class, $location);
        
        if(class_exists($class) === false){
            $this->raiseConfigureException("No Class<$class> for AuthEntity");
        }
        
        $collection = new ComponentCollection();
        $this->Entity = new $class($collection);
                
        if(($this->Entity instanceof AuthEntityComponent) === false){
            $this->raiseConfigureException("Not an AuthEntityComponent<Class:$class> in Auth Config");
        }
        
        $this->Entity->init($this->Config);
        $this->Entity->parseConf();
        $this->mergeEntity();
    }
    
    private function mergeEntity(){
        $entityData = $this->Entity->provideData();
        $this->Config['entity'] = array_merge($this->Config['entity'],$entityData);
    }

    public function getConfig() {
        return $this->Config;
    }
    
    public function renew(){
        $this->Mechanism->renew();
    }
    
    public function authentication($data){
        $auth = $this->Entity->resolvAuthentication($data);
        
        if($auth){
            $this->logauth("Auth<authentication:OK>");
        }
        else{
            $this->logauth("Auth<authentication:FAIL>");
        }
        
        return $auth;
    }
    
    public function getData(){
        return [
            'id' => $this->Entity->provideUniqueIdentificator(),
            'data' => $this->Entity->provideCommitData()
        ];
    }
    
    public function setFrontendData($id,$data){
        $this->Entity->loadData([
            'id' => $id,
            'data' => $data
        ]);
        
        $this->commit();
    }
    
    public function getFrontendData(){
        $data = $this->Entity->provideCommitData();
        unset($data['password']);
        
        return [
            'id' => $this->Entity->provideUniqueIdentificator(),
            'data' => $data
        ];
    }
    
    public function getId(){
        return $this->Entity->provideUniqueIdentificator();
    }
    
    public function commit(){
        $this->loadMechanism();
        
        $id = $this->Entity->provideUniqueIdentificator();
        $data = $this->Entity->provideCommitData();
        $write = $this->Mechanism->write($id,$data);
        
        if($write){
            $this->logauth("Auth<commit:OK,id:{$id}>");
        }
        else{
            $this->logauth("Auth<commit:FAIL,id:{$id}>");
        }
        
        return $write;
    }
    
    public function release(){
        $id = $this->Entity->provideUniqueIdentificator();
        $delete = $this->Mechanism->delete($id);
        
        if($delete){
            $this->logauth("Auth<release:OK,id:{$id}>");
        }
        else{
            $this->logauth("Auth<release:FAIL,id:{$id}>");
        }
        
        return $delete;
    }
    
    private function loadMechanism($conf,$mechanismExtras = []){
        $this->Config['mechanism'] = array_merge($this->Config['mechanism'],$mechanismExtras);
        $location = 'KlezApi.Controller/AuthMechanism';
        
        if(isset($this->Config['mechanism']['driver']) === false){           
            $this->raiseConfigureException("No Conf<{$conf}.mechanism.driver> in Auth Config");
        }
        
        if(isset($this->Config['mechanism']['driver-location'])){  
            $location = $this->Config['mechanism']['driver-location'];
        }
        
        $mechanism = Inflector::camelize($this->Config['mechanism']['driver']);
        $this->logauth("Auth<mechanism:$mechanism>");
        $class = "{$mechanism}AuthMechanismComponent";
        App::uses($class, $location);
        
        if(class_exists($class) === false){
            $this->raiseConfigureException("No Class<$class> for AuthMechanism");
        }
        
        $collection = new ComponentCollection();
        $this->Mechanism = new $class($collection);
                
        if(($this->Mechanism instanceof AuthMechanismComponent) === false){
            $this->raiseConfigureException("Not an AuthMechanismComponent<Class:$class> in Auth Config");
        }
        
        $this->Mechanism->init($this->Config);
        $this->Mechanism->parseConf();
    }
    
    public function resolvLoginBounce(){
        $config = $this->Config;
        $url = $config['gateway']['bounces']['login'];
        return $url;
    }
    
    public function resolvLogoutBounce(){
        $config = $this->Config;
        $url = $config['gateway']['bounces']['logout'];
        return $url;
    }
    
    public function getEntity(){
        return $this->Entity;
    }
}