<?php

App::uses('AuthMechanismComponent', 'KlezApi.Controller/AuthMechanism');

class BypassAuthMechanismComponent extends AuthMechanismComponent{
    private $data;
    
    public function delete($id) {
        
    }

    public function parseConf() {
        $config = $this->getConfig();
        
        if(isset($config['mechanism']['data']) === false){
            throw new ConfigureException('<Invalid Config<missing.key:mechanism.data> in BypassAuth');
        }
        if(isset($config['mechanism']['id']) === false){
            throw new ConfigureException('<Invalid Config<missing.key:mechanism.id> in BypassAuth');
        }
        
        $this->data = [
            'data' => $config['mechanism']['data'],
            'id' => $config['mechanism']['id']
        ];
    }

    public function read($id = null) {
        return $this->data;
    }

    public function renew($id = null) {
        
    }

    public function write($id, $data) {
        
    }

}