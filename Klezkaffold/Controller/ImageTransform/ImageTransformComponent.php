<?php

App::uses('BaseComponent', 'KlezApi.Controller/Component');

abstract class ImageTransformComponent extends BaseComponent{
    private $Image;
    private $blob;
    private $format;
    
    public function __construct(\ComponentCollection $collection, $settings = array()) {
        parent::__construct($collection, $settings);
        $this->initializeLog();
    }
    
    private function initializeLog(){
        $file = 'klezimagetrans';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'imagetrans' ],
            'file' => $file
        ));    
    }
    
    protected function logimagetrans($message){
        $fullMessage = "[{$this->getLogHash()}] {$message}";
        CakeLog::write('debug', $fullMessage, 'imagetrans');
    }
    
    public final function process($blob,$format = ''){
        $this->blob = $blob;
        $this->format = $format;
        
        $bin = base64_decode($this->blob);
        $res = imagecreatefromstring($bin);
        $this->Image = $this->processImage($res);
    }
    
    abstract function validateConfig($config);
    abstract function processImage($res);
    
    public final function provideBlob(){ 
        ob_end_clean();
        ob_start();
        
        switch ($this->format){
            case 'image/jpeg':
                imagejpeg($this->Image); 
                break;
            case 'image/png':
            default:
                imagepng($this->Image); 
                break;
        }
        
        imagedestroy($this->Image);
        $binary = ob_get_contents();  
        ob_end_clean();
        ob_start();
        
        $blob =  base64_encode($binary);
        
        return $blob;
    }
}