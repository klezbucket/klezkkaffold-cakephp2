<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class RequestMassiveKlezkaffoldComponent extends KlezkaffoldComponent{
    private $schema;
    private $data;
    private $id = null;
    private $notFound = false;
    private $Submodule;
    private $sub;
    
    public function submoduleInput($config,$payload){
        $this->parseSubmoduleConfig($payload);
        $this->loadSubmodule();
        $this->Submodule->input($config,$payload);
    }
    
    private function parseSubmoduleConfig($payload){
        if(isset($payload['submodule']) === false){
            $this->raiseConfigureException("Invalid Payload <missingkey:submodule> in Klezkaffold Config");
        }
        
        $this->sub = $payload['submodule'];
    }
    
    private function loadSubmodule(){
        $class = Inflector::camelize($this->sub) . 'RequestFormComponent';
        $path = 'Klezkaffold.Controller/RequestForm';
        
        App::uses($class, $path);
        
        if(class_exists($class) === false){
            $this->raiseConfigureException("No Such Class <class:$class,path:$path> in RequestForm Config");
        }
        
        $collection = new ComponentCollection();
        $this->Submodule = new $class($collection);
        
        if(($this->Submodule instanceof KlezkaffoldComponent) === false){
            $this->raiseConfigureException("Invalid Class <super.needed:KlezkaffoldComponent> in RequestForm Config");
        }
        
        $this->Submodule->setAuth($this->getAuth());
        $this->Submodule->setAcl($this->getAcl());
    }
    
    public function submoduleProcess(){
        $this->Submodule->process();
    }
    
    public function submoduleOutput(){
        return $this->Submodule->output();
    }
    
    public function output() {
        if($this->isPost()){
            return $this->submoduleOutput();
        }
        
        $data = [
            'schema' => $this->schema,
            'data' => $this->data
        ];
        
        if($this->notFound === true){
            $data = [
                'exception' => 404
            ];
        }
        
        return $data;
    }

    public function input($config,$payload = null) {
        if($this->isPost()){
            $this->submoduleInput($config,$payload);
        }
        else{
            $this->parseConfig($config);

            if(isset($payload['id'])){
                $this->id = $payload['id'];
            }
        }
    }
    
    private function resolvQuery(){
        $this->query = [];
        $alias = $this->getModel()->alias;
        $pkey = $this->getModel()->primaryKey;
        
        $conditions = [
            "{$alias}.{$pkey}" => $this->id
        ];
            
        $this->query['conditions'] = $conditions;;
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
    }
    
    public function process(){
        if($this->isPost()){
            return $this->submoduleProcess();
        }
        
        $this->schema = $this->getModel()->provideWritableSchema();
        
        if($this->id > 0){
            $this->resolvQuery();
            
            if(is_null($this->query)){
                $this->notFound = true;
            }
            else if($this->getModel()->findWritable('first',$this->query,false) === false){
                $this->notFound = true;
            }
        }
        
        $this->data = $this->getModel()->getFormData();
    }
    
    public function parseConfig($config){
        $this->loadModel($config['data']);
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        $this->id = $this->resolver($config);
    }
}