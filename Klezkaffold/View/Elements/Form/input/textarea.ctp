<div class="input-group m-t-10">
    <input 
        rows="1"
        type="text"
        readonly="readonly"
        placeholder="<?=$placeholder?>" 
        class="form-control <?=$field?>-textarea"
        value="<?=$value?>"
        name="<?=$field?>-facade"/>
    
    <textarea style="display:none;" name="<?=$field?>"><?=$value?></textarea>
    
    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
</div>

<script>
    $(function(){
        var trigger = $('.<?=$field?>-textarea').parent();
        var modal = $('#<?=$field?>-textarea-modal');
        var field = $('textarea[name="<?=$field?>"]');
        var facade = $('input[name="<?=$field?>-facade"]');
        var button = modal.find('button.btn');
        var input = modal.find('textarea');
        
        function textarea2facade(){
            var v = field.val();
            
            facade.val(v.split('\n')[0]);
        }
        
        button.off('click').on('click',function(e){
            field.val(input.val());
            modal.modal('hide');
            textarea2facade();
        });
        
        trigger.off('click').on('click',function(e){
            e.preventDefault();
            input.val(field.val());
            modal.modal('show');
            
            setTimeout(function(){
                input.focus();
            },1000);
        });
        
        facade.off('focus').on('focus',function(e){
            trigger.trigger('click');
        });
        
        trigger.find('*').css({
            'cursor' : 'pointer'
        });
        
        textarea2facade();
    });
</script>

<div id="<?=$field?>-textarea-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><?=$label?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea type="text" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect waves-light">Aceptar</button>
            </div>
        </div>
    </div>
</div>