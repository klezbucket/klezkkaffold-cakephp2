<div class="input-group m-t-10">
    <input 
        class="form-control klez-datepicker" 
        placeholder="dd/mm/aaaa" 
        readonly="readonly"
        value="<?=$date?>"
        name="<?=$field?>_date_facade" 
        id="<?=$field?>_date_picker"/>
    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
</div>

<div class="input-group m-t-10">
    <input 
        placeholder="<?=$label?>" 
        class="form-control"
        readonly="readonly"
        data-show-meridian="false"
        data-minute-step="5"
        value="<?=$time?>"
        name="<?=$field?>_time_facade"
        id="<?=$field?>_time_picker">
    

    <input type="hidden" value="<?=$value?>" name="<?=$field?>" />
    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
</div>

<script>    
    $(function(){   
        var date = $("#<?=$field?>_date_picker");
        var time = $("#<?=$field?>_time_picker");
        var datetime = $('input[name="<?=$field?>"]');
        
        time.timepicker();
        
        function changeDatetime(){
            var d = date.val().split('/');
            var fd = d[2] + "-" + d[1] + "-" + d[0]; 
            var v = fd + " " + time.val();
            datetime.val(v);
        }
        
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
        };
        
        var dp = date.datepicker({
            format: "dd/mm/yyyy",
            language : 'es',
            autoclose : true,
        });
                
        dp.on('changeDate', function(e) {
            changeDatetime();
        });
        
        time.timepicker().on('changeTime.timepicker', function(e) {
            changeDatetime();
        });
        
        if(date.val() === ''){
            dp.datepicker("setDate", new Date());
        }
    });
</script>