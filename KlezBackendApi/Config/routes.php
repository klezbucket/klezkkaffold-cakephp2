<?php

Router::connect('/api/backend/:endpoint.:format', 
    [ 'controller' => 'server', 'action' => 'index', 'plugin' => 'KlezApi' ],
    [ 'endpoint', 'format' ]
);