<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class FormHelper extends KlezkaffoldHelper {
    public function deploy($config){
        echo $this->element('main',[
            'config' => $config,
        ]);
    }
    
    public function main($config){
        if(isset($config['data']) === false){
            $this->raiseInternalServerError("No Conf<Form:data> in FormHelper");
        }
        
        if(isset($config['schema']) === false){
            $this->raiseInternalServerError("No Conf<Form:schema> in FormHelper");
        }
        
        if(isset($config['messages']) === false){
            $config['messages'] = [];
        }
        
        echo $this->element('form', $config);
    }
    
    public function inputs(&$schema,$data,$messages = []){
        $i = -1;
        
        foreach($schema as $field => $meta){
            $i++;
                        
            if($this->isHidden($meta) === true){
                continue;
            }
            
            if($this->isGroup($meta) === true){
                $this->group($schema,$data,$field,$messages);
                $keys = array_keys($schema);
                $last = count($keys) - 1;
                
                if($i <= $last){
                    $this->groupEnd();
                }
                
                continue;
            }
            
            $weight = $this->resolvWeight($meta);
            
            echo $this->element('input_holder',[
                'schema' => $schema,
                'data' => $data,
                'field' => $field,
                'label' => $this->resolvLabel($meta),
                'messages' => $messages,
                'weight' => $weight
            ]);
        }
    }
    
    private function group(&$schema,$data,$master,$messages = []){
        $meta = $schema[$master];
        
        $groupMeta = $meta['group'];
        $title = $groupMeta['title'];
        $fields = $groupMeta['fields'];
        $groupSchema = [];
        
        if(isset($groupMeta['dep'])){
            $this->groupDep($master,$groupMeta['dep']);
        }
        
        $this->groupStart($title,$master);
        $groupSchema[$master] = $schema[$master];
        unset($groupSchema[$master]['group']);
        
        foreach($fields as $field){
            $groupSchema[$field] = $schema[$field];
            unset($schema[$field]);
        }
        
        $this->inputs($groupSchema,$data,$messages);
        unset($schema[$master]);
    }
    
    private function groupDep($master,$dep){
        echo $this->element('group_dep',[
            'dep' => $dep,
            'master' => $master,
        ]);
    }
    
    private function groupEnd(){
        echo $this->element('group_end');
    }
    
    private function groupStart($title,$master){
        echo $this->element('group_start',[
            'title' => $title,
            'master' => $master
        ]);
    }
    
    private function isHidden($meta){
        if(isset($meta['hidden']) === false){
            return false;
        }
        
        return $meta['hidden'] === true;
    }
    
    public function inputHolders($schema,$data,$field,$messages){
        foreach($schema as $field => $meta){
            if($this->isHidden($meta)){
                return;
            }
            
            $label = $this->resolvLabel($meta);
            $weight = $this->resolvWeight($meta);
            
            echo $this->element('input_holder',[
                'schema' => $schema,
                'data' => $data,
                'field' => $field,
                'label' => $label,
                'messages' => $messages,
                'weight' => $weight
            ]);
            
            $this->extraRendering($meta);
        }
    }
    
    private function extraRendering($meta){
        if(isset($meta['alsoRender'])){
            $alsoRender = $meta['alsoRender'];
            
            if(is_array($alsoRender) === false){
                $alsoRender = [ $alsoRender ];
            }
            
            foreach($alsoRender as $view){
                echo $this->_View->element($view);
            }
        }
    }
    
    public function input($schema,$data,$field){
        $meta = $schema[$field];
        $type = $this->resolvType($meta);
        $subtype = $this->resolvSubtype($meta);
        
        if(is_null($type)){
            $this->raiseInternalServerError("Invalid Schema<missing.key:type> in FormHelper");
        }
        
        $input = "input_{$type}";
        
        if(is_null($subtype) === false){
            $input .= "_{$subtype}";
        }
        
        $method = Inflector::camelize($input);
        
        if(method_exists($this, $method) === false){
            $this->raiseInternalServerError("Unimplemented Input<input:{$input}> in FormHelper");
        }
        
        $this->{$method}($schema,$data,$field);
    }
    
    public function messages($field,$messages = []){
        if(isset($messages[$field]) === false){
            return;
        }
        
        if(is_array($messages[$field]) === false){
            return;
        }
        
        if(empty($messages[$field]) === true){
            return;
        }
        
        echo $this->element('messages',[
            'messages' => $messages[$field]
        ]);
    }
    
    public function messageList($messages = []){
        foreach($messages as $message){
            echo $this->element('message',[
                'message' => $message
            ]);
        }
    }
    
    private function inputFile($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvLabel($meta);
        $placeholder = $this->resolvPlaceholder($meta);
        $mimes = $meta['mimes'];
        $message = $meta['file-message'];
        
        echo $this->element('input/file',[
            'field' => $field,
            'label' => $label,
            'mimes' => $mimes,
            'placeholder' => $placeholder,
            'message' => $message
        ]);
    }
    private function inputFileCsv($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvLabel($meta);
        $placeholder = $this->resolvPlaceholder($meta);
        $mimes = $meta['mimes'];
        $message = $meta['csv-message'];
        
        echo $this->element('input/csv',[
            'field' => $field,
            'label' => $label,
            'mimes' => $mimes,
            'placeholder' => $placeholder,
            'message' => $message
        ]);
    }
    
    private function inputFileImage($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvLabel($meta);
        $placeholder = $this->resolvPlaceholder($meta);
        $mimes = $meta['mimes'];
        $message = $meta['image-message'];
//        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/image',[
//            'value' => $value,
            'field' => $field,
            'label' => $label,
            'mimes' => $mimes,
            'placeholder' => $placeholder,
            'message' => $message
        ]);
    }
    
    private function inputIntPrimary($schema,$data,$field){
        $meta = $schema[$field];
        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/primary',[
            'value' => $value,
            'field' => $field,
        ]);
    }

    private function inputInt($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvLabel($meta);
        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/int',[
            'value' => $value,
            'field' => $field,
            'label' => $label,
        ]);
    }

    private function inputTime($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvLabel($meta);
        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/time',[
            'value' => $value,
            'field' => $field,
            'label' => $label,
        ]);
    }
    
    private function inputText($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvLabel($meta);
        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/text',[
            'value' => $value,
            'field' => $field,
            'label' => $label,
        ]);
    }
    
    private function inputTextEmail($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvLabel($meta);
        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/email',[
            'value' => $value,
            'field' => $field,
            'label' => $label,
        ]);
    }
    
    private function inputDatetime($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvLabel($meta);
        $value = $this->resolvInputValue($field, $meta, $data);
        $iso = $this->resolvRawValue($field,  $data);
        $datetime = preg_split('/\s+/', $value);
        $date = @$datetime[0];
        $time = substr(@$datetime[1], 0, 5);
        
        echo $this->element('input/datetime',[
            'value' => $iso,
            'field' => $field,
            'label' => $label,
            'date' => $date,
            'time' => $time
        ]);
    }
    
    private function inputDate($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvLabel($meta);
        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/date',[
            'value' => $value,
            'field' => $field,
            'label' => $label,
        ]);
    }
    
    private function inputTextMask($schema,$data,$field){
        $meta = $schema[$field];
        $mask = $this->resolv($meta,'mask');
        $label = $this->resolvLabel($meta);
        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/mask',[
            'value' => $value,
            'field' => $field,
            'label' => $label,
            'mask' => $mask
        ]);
    }
    
    private function inputBoolean($schema,$data,$field){
        $meta = $schema[$field];
        $boolean = $meta['boolean'];
        $label = $this->resolvLabel($meta);
        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/boolean',[
            'value' => $value,
            'field' => $field,
            'boolean' => $boolean,
            'checked' => $value ? 'checked' : '',
            'label' => $label,
        ]);
    }
    
    private function inputOptions($schema,$data,$field){
        $meta = $schema[$field];
        $options = $meta['options'];
        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/options',[
            'value' => $value,
            'field' => $field,
            'options' => $options,
        ]);
    }
    
    private function inputForeignTags($schema,$data,$field){
        $meta = $schema[$field];
        $value = $this->resolvInputValue($field, $meta, $data);
        $placeholder = $this->resolvPlaceholder($meta);
        
        echo $this->element('input/tags',[
            'value' => $value,
            'field' => $field,
            'placeholder' => $placeholder
        ]);
    }
    
    private function inputForeignHabtm($schema,$data,$field){
        $meta = $schema[$field];
        $value = $this->resolvInputValue($field, $meta, $data);
        $options = $this->resolvOptions($meta);
        $placeholder = $this->resolvPlaceholder($meta);
        
        echo $this->element('input/habtm',[
            'value' => $value,
            'field' => $field,
            'options' => $options,
            'placeholder' => $placeholder
        ]);
    }
    
    private function inputForeignAutocomplete($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvAutocompleteLabel($field, $data);
        $value = $this->resolvInputValue($field, $meta, $data);
        $dep = $this->resolvAutocompleteDep($meta);
        $placeholder = $this->resolvPlaceholder($meta);
        $url = Router::url(null,true);
        $icon = $this->resolvIcon($meta);
        
        echo $this->element('input/autocomplete',[
            'value' => $value,
            'field' => $field,
            'placeholder' => $placeholder,
            'url' => $url,
            'icon' => $icon,
            'label' => $label,
            'dep' => $dep,
        ]);
    }
    
    private function resolvAutocompleteDep($meta){
        if(isset($meta['autocomplete_dep'])){
            return $meta['autocomplete_dep'];
        }
        
        return null;
    }
    
    private function resolvAutocompleteLabel($field,$data){
        $label = null;
        
        if(isset($data[$field . '_autocomplete'])){
            $label = $data[$field . '_autocomplete'];
        }
        
        return $label;
    }
    
    private function resolvIcon($schema){
        if(isset($schema['icon']) === false){
            $this->raiseInternalServerError('Invalid Schema<missing.key:icon>');
        }
        
        return $schema['icon'];
    }
    
    private function inputGmap($schema,$data,$field){
        $meta = $schema[$field];
        $fields = $this->resolvGmapFields($meta);
        $defaults = $this->resolvGmapDefaults($meta);
        
        $lat = $fields[0];
        $lon = $fields[1];
        $defaultLat = $defaults[0];
        $defaultLon = $defaults[1];
        $zoom = $this->resolvZoom($meta);
        $key = $this->resolvKey($meta);
        $label = $this->resolvLabel($meta);
        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/gmap',[
            'value' => $value,
            'field' => $field,
            'label' => $label,
            'key' => $key,
            'zoom' => $zoom,
            'lat' => $lat,
            'lon' => $lon,
            'defaultLat' => $defaultLat,
            'defaultLon' => $defaultLon,
        ]);
    }
    
    private function inputTextTextArea($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvLabel($meta);
        $value = $this->resolvInputValue($field, $meta, $data);
        $placeholder = $this->resolvPlaceholder($meta);
        
        echo $this->element('input/textarea',[
            'value' => $value,
            'field' => $field,
            'label' => $label,
            'placeholder' => $placeholder,
        ]);
    }
    
    private function inputTextPassword($schema,$data,$field){
        $meta = $schema[$field];
        $label = $this->resolvLabel($meta);
        $value = $this->resolvInputValue($field, $meta, $data);
        
        echo $this->element('input/password',[
            'value' => $value,
            'field' => $field,
            'label' => $label,
        ]);
    }
    
    public function options($options,$val){
        foreach($options as $value => $label){  
            $selected = '';
            
            if(is_array($val) && in_array($value, $val)){
                $selected = 'selected="selected"';
            }
            else if(strcmp($val, $value) === 0){
                $selected = 'selected="selected"';
            }
            
            echo $this->element('input/option',[
                'value' => $value,
                'label' => $label,
                'selected' => $selected
            ]); 
        }
    }
    
    public function submit(){ 
        echo $this->element('submit'); 
    }

    public function provideLogtag() {
        return "Form";
    }
    
    public function resolvErrorHolder($error = false){
        $class = '';
        
        if($error){
            $class = 'has-error';
        }
        
        return $class;
    }
    
    public function resolvErrorInput($error = false){
        $class = '';
        
        if($error){
            $class = 'error';
        }
        
        return $class;        
    }
}