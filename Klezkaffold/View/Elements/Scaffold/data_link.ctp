<a href="<?=$url?>">
    <button class="btn btn-success waves-effect waves-light" type="button">
        <i class="zmdi zmdi-<?=$icon?>"></i> <?=$title?>
    </button>
</a>
