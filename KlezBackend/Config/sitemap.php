<?php

$config = [];
$config['Sitemap.gateway.login'] = [
    'title' => 'Login | Klez Backend'
];

$config['Sitemap.backend.home'] = [
    'h1' => 'Home'
];

$config['Sitemap.backend.logout'] = [
    
];

$config['Sitemap.backend.logout'] = [
    
];

$config['Sitemap.backend.profile_edit'] = [
    'h1' => 'Editar Mi Perfil',
    'icon' => 'edit',
    'submit' => [
        'label' => 'Editar Mi Perfil',
        'icon' => 'send'
    ]
];

$config['Sitemap.Globals'] = [
//    'avatar' => '/klez_backend/img/user-default-alpha.png',
    'avatar' => Router::url([ 'controller' => 'backend', 'action' => 'profile_photo'], true),
    'title' => 'Klez Backend',
    'footer' => '2016 © Klez',
    'home' => [ 'controller' => 'backend', 'action' => 'home', 'plugin' => 'KlezBackend'],
    'logout' => [ 'controller' => 'gateway', 'action' => 'logout', 'plugin' => 'KlezBackend'],
    'profile_edit' => [ 'controller' => 'backend', 'action' => 'profile_edit', 'plugin' => 'KlezBackend'],
];