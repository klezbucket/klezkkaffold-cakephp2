<script>
    $(function(){
        var generateUrl = <?=json_encode(Router::url([
            'controller' => 'jsons',
            'action' => 'generate',
            'plugin' => 'KlezJson'
        ]))?>;
                
        var link = $("#json-master-link");
        var container = $("#json-master");
        var empty = $("#json-master-empty");
        var data = <?=json_encode($json)?>;
        var url = <?=json_encode($url)?>;
        
        function feedJsonMeta(json,url){
            var c = 0;
            
            for(var i in json){
                var xpath = "#json-master-" + i;
                $(xpath).html(json[i]);
                c++;
            }
            
            link.attr('href',url);
            link.html(url);
            
            if(c > 0){
                container.show();
                empty.hide();
            }
            else{
                container.hide();
            }
        }
                        
        function failure(){
            toastr["error"]("Falló la generación del JSON");

            toastr.options = {
              "closeButton": false,
              "debug": false,
              "newestOnTop": false,
              "progressBar": false,
              "positionClass": "toast-top-right",
              "preventDuplicates": true
            };
        }        
                        
        function success(data){
            toastr["success"]("JSON generado correctamente");

            toastr.options = {
              "closeButton": false,
              "debug": false,
              "newestOnTop": false,
              "progressBar": false,
              "positionClass": "toast-top-right",
              "preventDuplicates": true
            };
            
            feedJsonMeta(data.json,data.url);
            $('html,body').animate({ scrollTop: 0},'slow');
        }
            
        $('#generate-confirm').click(function () {
            swal({
                title: "Desea generar un nuevo Master?",
                text: "Las aplicaciones se alimentarán de la nueva configuración al reabrirse y descargar el nuevo JSON",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Generar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: true
            }, function () {
                $('.json-loading').modal('show');
                
                $.ajax({
                    type: "POST",
                    url: generateUrl,
                    dataType : 'json'
                }).done(function(data) {
                    if(typeof data.payload !== 'undefined'){
                        if(typeof data.payload.success !== 'undefined'){
                            if(data.payload.success === false){
                                failure();
                                return;
                            }
                        }
                    }
                    
                    success(data);
                }).error(function(data) {
                    failure();
                }).always(function(data) {
                    $('.json-loading').modal('hide');
                });
            });
        }); 
        
        feedJsonMeta(data,url);
        $("#generate-container").show();
    });
</script>

<div id="generate-container" style="display:none;" class="col-sm-12">
    <div class="card-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <button id="generate-confirm" class="btn btn-success waves-effect waves-light" type="button">
                    <i class="zmdi zmdi-upload"></i> Generar Nuevo Master    </button>                    
                </div>
            </div>
        </div>   
    </div>             
</div>

<div class="modal fade json-loading" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="json-loading" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <p align="center">
                    <i class="fa fa-4x fa-spin fa-spinner"></i><br/><br/>
                    Generando el JSON, esto puede tardar...
                </p>
            </div>
        </div>
    </div>
</div>