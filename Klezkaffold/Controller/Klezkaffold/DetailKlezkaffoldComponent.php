<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class DetailKlezkaffoldComponent extends KlezkaffoldComponent{
    private $schema = [];
    private $data = [];
    private $links = [];
    private $notFound = false;
    
    public function output() {
        $data = [ 
            'data' => $this->data,
            'links' => $this->links,
            'schema' => $this->schema
        ];
        
        if($this->notFound === true){
            $data['exception'] = 404;
        }
        
        return $data;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
        $this->parseActions($config);
        $this->parsePayload($payload);
    }
    
    public function process(){
        $this->resolvQuery();
        $this->resolvData();
        $this->resolvDataLinks();
    }
    
    private function resolvDataLinks(){
        $this->links = $this->resolvLinks($this->data);
    }
    
    private function resolvQuery(){
        $this->query = [];
        $alias = $this->getModel()->alias;
        $pkey = $this->getModel()->primaryKey;
        
        $conditions = [
            "{$alias}.{$pkey}" => $this->id
        ];
            
        $this->query['conditions'] = $conditions;
        
        if(empty($this->prequery) === false){
            $this->prequeryProcess();
        }
    }
    
    private function resolvData(){
        if(is_null($this->query) === false){
            $this->data = $this->getModel()->findReadable('first',$this->query,true);
            $this->schema = $this->getModel()->provideReadableSchema();
        }
        
        if($this->data === false){
            $this->notFound = true;
        }
    }
    
    private function parsePayload($payload){
        if(isset($payload['id'])){
            $this->id = $payload['id'];
        }
    }
    
    private function parseConfig($config){
        $this->loadModel($config['data']);
        
        if(isset($config['data']['prequery']) === true){
            $this->prequery = $config['data']['prequery'];
        }
        
        $this->id = $this->resolver($config);
    }
}