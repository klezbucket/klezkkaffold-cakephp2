<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="google" content="notranslate">
        <meta http-equiv="Content-Language" content="es">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="<?=$this->Html->url('/klez_backend/assets/images/favicon.ico')?>">

        <title><?=$this->Backend->feed('title')?></title>
        
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')?>" rel="stylesheet" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/multiselect/css/multi-select.css')?>"  rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/select2/dist/css/select2.css')?>" rel="stylesheet" type="text/css">
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/select2/dist/css/select2-bootstrap.css')?>" rel="stylesheet" type="text/css">
        
        <link href="<?=$this->Html->url('/klez_backend/assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/core.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/components.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/icons.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/pages.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/menu.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/css/responsive.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klezkaffold/css/dashboard.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/switchery/switchery.min.css')?>" rel="stylesheet" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/datatables/jquery.dataTables.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/datatables/buttons.bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/datatables/responsive.bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/datatables/scroller.bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=$this->Html->url('/css/custom.css')?>" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?=$this->Html->url('/klez_backend/assets/plugins/chartist/dist/chartist.min.css')?>">
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')?>" rel="stylesheet">
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/custombox/dist/custombox.min.css')?>" rel="stylesheet">
        <link href="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-sweetalert/sweet-alert.css')?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?=$this->Html->url('/klez_backend/assets/js/modernizr.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/chartist/dist/chartist.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/chartist/dist/chartist-plugin-tooltip.min.js')?>"></script>
    </head>

    <body class="fixed-left">
        <div id="wrapper">
            <div class="topbar">
                <div class="topbar-left">
                    <a href="<?=$this->Backend->feedUrl('home')?>" class="logo"><span>Klez<span>Backend</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h1 class="page-title"><?=$this->Backend->feed('h1')?></h1>
                            </li>
                        </ul>

<!--                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <div class="notification-box">
                                    <ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>
                        </ul>-->
                    </div>
                </div>
            </div>

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div class="user-box">
                        <div class="user-img">
                            <img src="<?=$this->Backend->feedUrl('avatar')?>" title="<?=$this->Api->feed('auth.data.email')?>" class="img-circle img-thumbnail img-responsive">
                            <!--<div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>-->
                        </div>
                        <h5><a href="#"><?=$this->Api->feed('auth.data.email')?></a> </h5>
                        <ul class="list-inline">
                            <li>
                                <a href="<?=$this->Backend->feedUrl('profile_edit')?>" >
                                    <i class="zmdi zmdi-settings"></i>
                                </a>
                            </li>

                            <li>
                                <a href="<?=$this->Backend->feedUrl('logout')?>" class="text-custom">
                                    <i class="zmdi zmdi-power"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    
                    <div id="sidebar-menu">
                        <?php $this->Menu->deploy($this->Api->feed('menu')); ?>
<!--                        <ul>
                            <li class="text-muted menu-title">Navigation</li>

                            <li>
                                <a href="index.html" class="waves-effect active"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a>
                            </li>

                            <li>
                                <a href="typography.html" class="waves-effect"><i class="zmdi zmdi-format-underlined"></i> <span> Typography </span> </a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> User Interface </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="ui-buttons.html">Buttons</a></li>
                                    <li><a href="ui-cards.html">Cards</a></li>
                                    <li><a href="ui-draggable-cards.html">Draggable Cards</a></li>
                                    <li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>
                                    <li><a href="ui-material-icons.html">Material Design Icons</a></li>
                                    <li><a href="ui-font-awesome-icons.html">Font Awesome</a></li>
                                    <li><a href="ui-themify-icons.html">Themify Icons</a></li>
                                    <li><a href="ui-modals.html">Modals</a></li>
                                    <li><a href="ui-notification.html">Notification</a></li>
                                    <li><a href="ui-range-slider.html">Range Slider</a></li>
                                    <li><a href="ui-components.html">Components</a>
                                    <li><a href="ui-sweetalert.html">Sweet Alert</a>
                                    <li><a href="ui-treeview.html">Tree view</a>
                                    <li><a href="ui-widgets.html">Widgets</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-collection-text"></i><span class="label label-warning pull-right">7</span><span> Forms </span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="form-elements.html">General Elements</a></li>
                                    <li><a href="form-advanced.html">Advanced Form</a></li>
                                    <li><a href="form-validation.html">Form Validation</a></li>
                                    <li><a href="form-wizard.html">Form Wizard</a></li>
                                    <li><a href="form-fileupload.html">Form Uploads</a></li>
                                    <li><a href="form-wysiwig.html">Wysiwig Editors</a></li>
                                    <li><a href="form-xeditable.html">X-editable</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-view-list"></i> <span> Tables </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                	<li><a href="tables-basic.html">Basic Tables</a></li>
                                    <li><a href="tables-datatable.html">Data Table</a></li>
                                    <li><a href="tables-responsive.html">Responsive Table</a></li>
                                    <li><a href="tables-editable.html">Editable Table</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-chart"></i><span> Charts </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="chart-flot.html">Flot Chart</a></li>
                                    <li><a href="chart-morris.html">Morris Chart</a></li>
                                    <li><a href="chart-chartist.html">Chartist Charts</a></li>
                                    <li><a href="chart-chartjs.html">Chartjs Chart</a></li>
                                    <li><a href="chart-other.html">Other Chart</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="calendar.html" class="waves-effect"><i class="zmdi zmdi-calendar"></i><span> Calendar </span></a>
                            </li>

                            <li>
                                <a href="inbox.html" class="waves-effect"><i class="zmdi zmdi-email"></i><span class="label label-purple pull-right">New</span><span> Mail </span></a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-collection-item"></i><span> Pages </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="page-starter.html">Starter Page</a></li>
                                    <li><a href="page-login.html">Login</a></li>
                                    <li><a href="page-register.html">Register</a></li>
                                    <li><a href="page-recoverpw.html">Recover Password</a></li>
                                    <li><a href="page-lock-screen.html">Lock Screen</a></li>
                                    <li><a href="page-confirm-mail.html">Confirm Mail</a></li>
                                    <li><a href="page-404.html">Error 404</a></li>
                                    <li><a href="page-500.html">Error 500</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-layers"></i><span>Extra Pages </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="extras-projects.html">Projects</a></li>
                                    <li><a href="extras-tour.html">Tour</a></li>
                                    <li><a href="extras-taskboard.html">Taskboard</a></li>
                                    <li><a href="extras-taskdetail.html">Task Detail</a></li>
                                    <li><a href="extras-profile.html">Profile</a></li>
                                    <li><a href="extras-maps.html">Maps</a></li>
                                    <li><a href="extras-contact.html">Contact list</a></li>
                                    <li><a href="extras-pricing.html">Pricing</a></li>
                                    <li><a href="extras-timeline.html">Timeline</a></li>
                                    <li><a href="extras-invoice.html">Invoice</a></li>
                                    <li><a href="extras-faq.html">FAQ</a></li>
                                    <li><a href="extras-gallery.html">Gallery</a></li>
                                    <li><a href="extras-email-template.html">Email template</a></li>
                                    <li><a href="extras-maintenance.html">Maintenance</a></li>
                                    <li><a href="extras-comingsoon.html">Coming soon</a></li>
                                </ul>
                            </li>

                        </ul>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <?=$this->fetch('content'); ?>
                    </div>
                </div>

                <footer class="footer text-right">
                    <?=$this->Backend->feed('footer') ?>
                </footer>
            </div>

<!--            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="zmdi zmdi-close-circle-o"></i>
                </a>
                <h4 class="">Notifications</h4>
                <div class="notification-list nicescroll">
                    <ul class="list-group list-no-border user-list">
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-2.jpg" alt="">
                                </div>
                                <div class="user-desc">
                                    <span class="name">Michael Zenaty</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">2 hours ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-info">
                                    <i class="zmdi zmdi-account"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">New Signup</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">5 hours ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-pink">
                                    <i class="zmdi zmdi-comment"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">New Message received</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">1 day ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a href="#" class="user-list-item">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-3.jpg" alt="">
                                </div>
                                <div class="user-desc">
                                    <span class="name">James Anderson</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">2 days ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-warning">
                                    <i class="zmdi zmdi-settings"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">Settings</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">1 day ago</span>
                                </div>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>-->
        </div>
        
        <script>
            var resizefunc = [];
        </script>
        
        <script src="<?=$this->Html->url('/klez_backend/assets/js/bootstrap.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/detect.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/fastclick.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.slimscroll.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.blockUI.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/waves.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.nicescroll.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.scrollTo.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/toastr/toastr.min.js')?>"></script>

        <!--[if IE]>
        <script type="text/javascript" src="<?=$this->Html->url('/klez_backend/assets/plugins/jquery-knob/excanvas.js')?>"></script>
        <![endif]-->
                
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')?>"></script>
        <script type="text/javascript" src="<?=$this->Html->url('/klez_backend/assets/plugins/multiselect/js/jquery.multi-select.js')?>"></script>
        <script type="text/javascript" src="<?=$this->Html->url('/klez_backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/select2/dist/js/select2.min.js')?>" type="text/javascript"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/switchery/switchery.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/waypoints/lib/jquery.waypoints.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/counterup/jquery.counterup.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/jquery-knob/jquery.knob.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.core.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/js/jquery.app.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')?>" type="text/javascript"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')?>"></script>
        <script src="<?=$this->Html->url('/klez_backend/assets/plugins/timepicker/bootstrap-timepicker.min.js')?>"></script>

        <?php $this->Flash->deploy(); ?>
    </body>
</html>