<?php

App::uses('Controller', 'Controller');

if(!defined('BACKEND_DATA')){
    define('BACKEND_DATA',  'backend_data_' . substr(md5(uniqid()),0,8));
}

if(!defined('LOG_HASH')){
    define('LOG_HASH',  'log_hash' . substr(md5(uniqid()),0,8));
}

class KlezBackendAppController extends Controller {
    public $components = [ 'KlezBackend.Web', 'Session' ];
    public $helpers = [ 
        'KlezBackend.Api', 
        'KlezBackend.Backend',
        'KlezBackend.Menu',
        'KlezBackend.Error', 
        'KlezBackend.Flash', 
    ];
    
    private $Data;
    
    public function beforeFilter() {
        parent::beforeFilter();
        
        $this->layout = 'KlezBackend.backend';
        $this->loadConfig();
        
        if($this->isXHR()){
            $action = $this->request->params['action'];
            $xhrAction = "{$action}_xhr";
                
            if(method_exists($this, $xhrAction)){
                $this->request->params['action'] = $xhrAction;
            }
            else{
                throw new NotFoundException('No XHR method for ' . $action);
            }
        }
    }
    
    private function isXHR(){
        return @$_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }
    
    public function read($key){
        if(isset($this->Data[$key])){
            return $this->Data[$key];
        }
        
        return null;
    }
    
    public function write($key,$val){
        $this->Data[$key] = $val;
    }
    
    public function beforeRender() {
        if($this->name == 'CakeError') {
            $this->errorRequest();
        }
        
        $this->setBackendData();
        return parent::beforeRender();
    }
    
    private function errorRequest(){
        $code = $this->response->statusCode();
        $codes = $this->response->httpCodes();
        
        $error = [
            'code' => $code,
            'name' => $codes[$code]
        ];
        
        $this->write('error',$error);
        $this->layout = 'KlezBackend.error';
    }
    
    private function setBackendData(){
        $this->set(BACKEND_DATA,$this->Data);
        $this->set(LOG_HASH,$this->Web->getLogHash());
    }
    
    private function loadConfig(){
        Configure::load('klezbackend','default',false);
        
        $confiles = Configure::read('Backend.confiles');
        
        if(is_null($confiles)){            
            throw new ConfigureException('No Conf<Backend:confiles> in Backend Cofig');
        }
        
        if(is_array($confiles) === false){
            $confiles = [ $confiles ];
        }
        
        foreach($confiles as $confile){
            Configure::load($confile,'default',false);
        }
        
        $this->mergeGlobals();
        $this->parseConf();
    }
    
    private function parseConf(){
        $this->parseSitemap();
    }
    
    private function mergeGlobals(){
        $key = 'Sitemap.Globals';
        $conf = Configure::read($key);
        
        if(is_array($conf)){
            foreach($conf as $key => $val){
                $this->write($key, $val);
            }
        }
    }
    
    private function parseSitemap(){
        $controller = $this->params['controller'];
        $action = $this->params['action'];
        $key = "Sitemap.{$controller}.{$action}";
        
        $conf = Configure::read($key);
        
        if(is_null($conf)){
            throw new ConfigureException("No Conf<Sitemap:$key> in Auth Config");
        }
        
        foreach($conf as $key => $val){
            $this->write($key, $val);
        }
    }
    
    public function flashToast($class,$message){
        $this->Session->write('Backend.flash', [
            'class' => $class,
            'message' => $message
        ]);
    }
    
    public function get($key,$default){
        $value = $default;
        
        if(isset($this->params->query[$key])){
            $value = $this->params->query[$key];
        }
        
        return $value;
    }
    
    public function route($key){
        if(isset($this->params[$key]) === false){
            throw new ConfigureException("No Conf<key:$key> in Routed Params");
        }
        
        $value = $this->params[$key];
        return $value;
    }
}