<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class TableHelper extends KlezkaffoldHelper {
    public function deploy($config){
        echo $this->element('main',[
            'config' => $config,
        ]);
    }
    
    public function main($config){
        if(isset($config['data']) === false){
            $this->raiseInternalServerError("No Conf<Table:data> in TableHelper");
        }
        
        $data = $config['data'];
        
        if(empty($data)){
            $this->emptyData();
        }
        else{
            $this->tableData($config);
        }
    }
    
    private function tableData($config){
        $data = $config['data'];
        $schema = $config['schema'];
        
        echo $this->element('table',[
            'data' => $data,
            'schema' => $schema,
        ]);
    }
    
    public function provideLogtag() {
        return 'Table';
    }
    
    public function tableHead($schema){
        echo $this->element('table_head',[
            'schema' => $schema,
        ]);
    }
    
    public function tableHeadCells($schema){
        foreach($schema as $field => $meta){
            if($this->isListable($meta) === false){
                continue;
            }
            
            $label = $this->resolvLabel($meta);
            
            echo $this->element('table_head_cell',[
                'field' => $field,
                'label' => $label
            ]);
        }
    }
    
    public function tableBody($schema,$data){
        echo $this->element('table_body',[
            'data' => $data,
            'schema' => $schema
        ]);
    }
    
    public function tableRows($schema,$data){
        foreach($data as $row){
            echo $this->element('table_row',[
                'row' => $row,
                'schema' => $schema
            ]);            
        }
    }
    
    public function tableCells($schema,$row){
        foreach($schema as $field => $meta){
            if($this->isListable($meta) === false){
                continue;
            }
            
            $value = $this->resolvValue($field,$meta,$row);
            
            echo $this->element('table_cell',[
                'field' => $field,
                'value' => $value
            ]);            
        }
    }
}