<button class="btn btn-warning waves-effect waves-light" type="button" onclick="window.history.back()">
    <i class="fa fa-backward m-r-5"></i> <span>Cancelar</span>
</button>

<button class="btn btn-success waves-effect waves-light" type="submit">
    <i class="fa fa-<?=$this->Backend->feed('confirm.icon')?> m-r-5"></i> <span><?=$this->Backend->feed('confirm.label')?></span>
</button>