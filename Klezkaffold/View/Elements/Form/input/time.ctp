<div class="input-group m-t-10">
    <input 
        placeholder="<?=$label?>" 
        class="form-control"
        readonly="readonly"
        data-show-meridian="false"
        data-minute-step="5"
        value="<?=$value?>"
        name="<?=$field?>"
        id="<?=$field?>_picker">
    
    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
</div>

<script>
    $(function(){
        var picker = jQuery('#<?=$field?>_picker');
        picker.timepicker();
    });
</script>