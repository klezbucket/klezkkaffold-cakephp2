<?php

App::uses('KlezBackendAppController', 'KlezBackend.Controller');

class BackendController extends KlezBackendAppController {
    public $components = [ 
        'KlezBackend.Web',
        'KlezBackend.KlezkaffoldWeb', 
        'Session' 
    ];
    
    public $helpers = [
        'KlezBackend.Api', 
        'KlezBackend.Backend',
        'KlezBackend.Flash',
        'Klezkaffold.Form',
        'Klezkaffold.Delete',
    ];
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function home(){
        $this->Web->home();
    }
    
    public function profile_edit(){
        if($this->request->is('POST')){
            $this->KlezkaffoldWeb->profileEdit($this->data,[
                'messages' => [
                    'success' => Configure::read('Sitemap.backend.profile_edit.flash.success'),
                    'error' => Configure::read('Sitemap.backend.profile_edit.flash.error')
                ]
            ],[
                'controller' => 'backend',
                'action' => 'home',
            ]);
        }
        else{
            $this->KlezkaffoldWeb->requestProfileForm();    
        }
    }
}