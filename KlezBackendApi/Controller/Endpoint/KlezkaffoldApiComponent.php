<?php

App::uses('KlezkaffoldBaseApiComponent', 'KlezBackendApi.Controller/Endpoint');
App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class KlezkaffoldApiComponent extends KlezkaffoldBaseApiComponent {
    public function dashboard($data){
        $this->start($data,'dashboard');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        if(isset($data['exception'])){
            $this->setException($data['exception']);
        }
        
        if(is_array($this->Config)){
            foreach($this->Config as $name => $conf){
                if(isset($conf['url']) === true){
                    $url = $conf['url'];
                    
                    if($this->isUrlAllowed($url) === false){
                        continue;
                    }
                }
                
                $this->Klezkaffold->preinput($conf,$payload);
                $this->Klezkaffold->input($conf,$payload);
                $this->Klezkaffold->process();
                        
                $conf['data'] = $this->Klezkaffold->output();
                $this->write($name, $conf);
            }
        }
    }
    
    public function report($data){
        $this->start($data,'report');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function show($data){
        $this->start($data,'show');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function profile_photo($data){
        $data['payload']['id'] = $this->getAuth()->getId();
        $this->start($data,'image');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if(isset($output['exception'])){
            if($output['exception'] === 304){
                $this->setException($output['exception']);
                return;
            }
            
            $default = Configure::read('Profile.defaultPhoto');
            $webroot = WWW_ROOT;
            $full = $webroot . $default;
            
            $this->Klezkaffold->proxy($full);
            $output = $this->Klezkaffold->output();
        }
        
        $this->overwrite($output);
    }
    
    public function image($data){
        $this->start($data,'image');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function detail($data){
        $this->start($data,'detail');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function requestDelete($data){
        $this->start($data,'request_delete');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function requestForm($data){
        $this->start($data,'request_form');
        $payload = null;

        if(isset($data['payload'])){
            $payload = $data['payload'];
        }

        if($this->isPost()){
            $this->setSimple(true);
        }

        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();

        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function requestMassive($data){
        $this->start($data,'request_massive');
        $payload = null;

        if(isset($data['payload'])){
            $payload = $data['payload'];
        }

        if($this->isPost()){
            $this->setSimple(true);
        }

        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();

        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function massive($data){
        $this->start($data,'massive');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if($this->isPut()){
            if($this->Klezkaffold->isSuccess()){
                $url = $this->Klezkaffold->getRedirect();
                $this->setRedirect($url);
            }
        }
        
        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function add($data){
        $this->start($data,'add');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if($this->Klezkaffold->isSuccess()){
            $url = $this->Klezkaffold->getRedirect();
            $this->setRedirect($url);
        }
        
        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function edit($data){
        $this->start($data,'edit');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if($this->Klezkaffold->isSuccess()){
            $url = $this->Klezkaffold->getRedirect();
            $this->setRedirect($url);
        }
        
        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function delete($data){
        $this->start($data,'delete');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if($this->Klezkaffold->isSuccess()){
            $url = $this->Klezkaffold->getRedirect();
            $this->setRedirect($url);
        }
        
        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function profile_edit($data){
        $id = $this->getAuth()->getId();
        $data['payload']['data']['id'] = $id;
        $this->start($data,'edit');
        $payload = null;
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if($this->Klezkaffold->isSuccess()){
            $data = $this->Klezkaffold->getModel()->getData();
            $this->getAuth()->setFrontendData($id,$data);
            $url = $this->Klezkaffold->getRedirect();
            $this->setRedirect($url);
        }
        
        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
    
    public function profile($data){
        $data['payload']['id'] = $this->getAuth()->getId();
        
        $this->start($data,'request_form');
        $payload = null;
        
        if($this->isPost()){
            $this->setSimple(true);
        }
        
        if(isset($data['payload'])){
            $payload = $data['payload'];
        }
        
        $this->Klezkaffold->preinput($this->Config,$payload);
        $this->Klezkaffold->input($this->Config,$payload);
        $this->Klezkaffold->process();
        $output = $this->Klezkaffold->output();
        
        if(isset($output['exception'])){
            $this->setException($output['exception']);
        }
        else{
            $this->overwrite($output);
        }
    }
}