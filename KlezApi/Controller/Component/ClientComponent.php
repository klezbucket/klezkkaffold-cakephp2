<?php

App::uses('BaseComponent', 'KlezApi.Controller/Component');

class ClientComponent extends BaseComponent{
    private static $CookieSessionKey = 'ClientComponent.CookieKey';
    private $cookie;
    private $curlHooks = [];
    private $headerHooks = [ 'size', 'type'];
    private $responseHeaders = [];
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        
        $this->initializeLog();
    }
    
    public function getResponseHeaders(){
        return $this->responseHeaders;
    }
    
    public function getHeaders(){
        return $this->headers;
    }
    
    private function initializeLog(){
        $file = 'klezapicli';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'apicli' ],
            'file' => $file
        ));    
    }
    
    protected function logapicli($message){
        $fullMessage = "[{$this->getLogHash()}] {$message}";
        CakeLog::write('debug', $fullMessage, 'apicli');
    }
    
    private $verb;
    private $data;
    private $url;
    private $headers = [];
    private $response;
    private $contentType;
    private $statusCode;
    
    public function putHeader($name,$val){
        $this->headers[$name] = $val;
        return $this;
    }

    public function setVerb($verb) {
        $this->verb = $verb;
        return $this;
    }

    public function setData($data) {
        $this->data = $data;
        return $this;
    }

    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
    
    public function cookieStorage(){
        $this->curlHooks[] = 'cookie_setup';
        $this->headerHooks[] = 'cookie';
    }
    
    private function loadCookie(){
        $this->cookie = CakeSession::read(self::$CookieSessionKey);
    }
    
    public function putHeaderHook($name){
        $this->headerHooks[] = $name;
    }
    
    private function curlHookCookieSetup($ch){
        $this->loadCookie();
        $this->logapicli("CookieUpload<{$this->cookie}>");
        curl_setopt($ch, CURLOPT_COOKIE, $this->cookie);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    }
    
    private function curlHeaderHookCookie($ch,$line){
        $cookie = [];
        
        if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $line, $cookie) === 1) {
            $this->cookie = $cookie[1];
            $this->logapicli("CookieDownload<{$this->cookie}>");
            CakeSession::write(self::$CookieSessionKey,$this->cookie);
        }          
    }
    
    public function jsonPayload(){
        $this->data = json_encode($this->data);
        $this->putHeader('Content-Type', 'application/json');
        $this->putHeader('Content-Length', strlen($this->data));
        $this->request('json');
    }
    
    public function request($dataType = false){
        $verb = strtoupper($this->verb);
        $this->logapicli("Request<{$verb},{$this->url}>");
        
        if($dataType !== false){
            $this->logapicli("Data<{$dataType}:{$this->data}>");
        }
        
        $ch = curl_init();
        
        if (Configure::read('BasicAuth.enabled') === true){
            $this->putHeader('Authorization', 'Basic ' . Configure::read('BasicAuth.hash'));
        }
        
        $this->processHeaders($ch);
        $this->processHooks($ch);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $verb);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);       
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, array($this,'headersCallback'));
        
        $this->response  = curl_exec($ch);  
        $this->contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        $this->statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        if($this->isBinary($this->contentType)){
            $logdata = 'BINARY/NON-PRINTABLE';
        }
        else{
            $json = json_decode($this->response,true);
            
            if(isset($json['payload']['blob'])){
                $json['payload']['blob'] = '<MD5>' . md5($json['payload']['blob']);
            }
            
            $logdata = json_encode($json);
        }
        
        $this->logapicli("Response<{$this->statusCode}:{$this->contentType},{$logdata}>");
    }
    
    public function flashRewrite($message,$kind,$format = false){
        $data = $this->getHttpOkResponse($format);
        
        $data['flash'] = [
            'message' => $message,
            'kind' => $kind
        ];
        
        $this->logapicli("FlashRewrite<$kind:$message>");
        $this->rewrite($data,$format);
    }
    
    protected function redirectRewrite($url,$format = false){
        $data = $this->getHttpOkResponse($format);
        $data['url'] = $url;
        $purl = serialize($url);
        
        $this->logapicli("RedirectRewrite<url:$purl>");
        $this->rewrite($data,$format);
    }
    
    private function rewrite($data,$format = false){
        switch($format){
            case 'json':
                $this->response = json_encode($data);
                break;
            default:
                $this->response = $data;
                break;
        }
    }
    
    public function getHttpOkResponse($format = false){
        if($this->statusCode === 200){
            return $this->getUnserializedResponse($this->response,$format);
        }
        else{
            http_response_code($this->statusCode);
            throw new InternalErrorException("Api Status Code = {$this->statusCode}",$this->statusCode);
        }
    }
    
    private function processHeaderHooks($ch,$line){        
        foreach($this->headerHooks as $hook){
            $camelized = Inflector::camelize($hook);
            $method = "curlHeaderHook{$camelized}";
            
            if(method_exists($this, $method)){
                $this->{$method}($ch,$line);
            }
        } 
    }
    
    private function headersCallback($ch,$line){
        $this->processHeaderHooks($ch,$line);
        
        if (preg_match('/^(.*):\s*([^;]*)/mi', $line, $header) === 1) {
            $name = $header[1];
            $value = $header[2];
            $this->responseHeaders[$name] = $value;
        }          
        
        return strlen($line);
    }

    
    private function getUnserializedResponse($data,$format = false){
        if($format === false){
            return $data;
        }
        
        switch($format){
            case 'json':
                return json_decode($data,true);
        }
        
        return $data;
    }
    
    public function getStatusCode(){
        return $this->statusCode;
    }
    
    public function getResponse($format = false){
        return $this->getUnserializedResponse($this->response,$format);
    }
    
    public function getContentType(){
        return $this->contentType;
    }
    
    private function processHooks($ch){
        foreach($this->curlHooks as $hook){
            $camelized = Inflector::camelize($hook);
            $method = "curlHook{$camelized}";
            
            if(method_exists($this, $method)){
                $this->{$method}($ch);
            }
        }
    }
    
    private function processHeaders($ch){
        $normalized = [];
        
        foreach($this->headers as $header => $val){
            $h = "{$header}: {$val}";
            $normalized[] = $h;
            $this->logapicli("Header<{$header},{$val}>");
        }
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $normalized);
    }
    
    private $type;
    private $size;
    
    private function curlHeaderHookType($ch,$line){
        $type = [];
        
        if (preg_match('/^Content-Type:\s*([^;]*)/mi', $line, $type) === 1) {
            $this->type = trim($type[1]);
            $this->logapicli("Type<{$this->type}>");
        }          
    }
    
    private function curlHeaderHookSize($ch,$line){
        $size = [];
        
        if (preg_match('/^Content-Length:\s*([^;]*)/mi', $line, $size) === 1) {
            $this->size = trim($size[1]);
            $this->logapicli("Size<{$this->size}>");
        }          
    }
    
    function getType() {
        return $this->type;
    }

    function getSize() {
        return $this->size;
    }
}