<?php

App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');

abstract class BackendComponent extends EndpointComponent{
    private $Auth;
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        
        $this->Auth = $controller->Auth;
    }
    
    private $flash = null;
    private $redirect = null;
    private $exception = null;
    private $payload = [];
    private $menu = [];
    private $simple = false;
    private $raw = false;
    
    public function setSimple($b){
        $this->simple = $b;
    }
    
    function setException($exception) {
        $this->exception = $exception;
    }
    
    public function getAuth(){
        return $this->Auth;
    }
        
    private function loadMenu(){
        Configure::load('klezbackend','default',false);
        $confiles = Configure::read('Backend.confiles');
        
        if(is_array($confiles)){
            foreach($confiles as $confile){
                Configure::load($confile,'default',false);
            }
        }
        
        $menu = Configure::read('Menu');
        $clearMenu = $this->aclMenu($menu);
        $this->menu = $clearMenu;
    }
    
    private function aclMenu($menu){
        foreach($menu as $name => &$entry){
            $allowed = true;
            
            if(isset($entry['url'])){
                $url = $entry['url'];
                $allowed = $this->isUrlAllowed($url);
            }
            else if(isset($entry['childs'])){
                $entry['childs'] = $this->aclMenu($entry['childs']);
            }
            
            if($allowed === false){
                unset($menu[$name]);
            }
            else if(isset($entry['childs']) && empty($entry['childs'])){
                unset($menu[$name]);
            }
        }

        return $menu;
    }
    
    public function setRedirect($url){
        $this->redirect = $url;
    }
    
    public function write($key,$val){
        $this->payload[$key] = $val;
    }
    
    public function overwrite($val){
        $this->payload = $val;
    }
    
    public function read($key){
        if(isset($this->payload[$key])){
            return $this->payload[$key];
        }
        
        return null;
    }
    
    public function readAll(){
        return $this->payload;
    }
    
    public function setFlash($message,$kind){
        $this->flash = [
            'message' => $message,
            'kind' => $kind
        ];
    }
    
    public function json(){
        $data = $this->resolvData();
        
        if($this->raw){
            return $data;
        }
        else{
            return json_encode($data);
        }
    }
    
    public function rawJson($json){
        $this->raw = true;
        $this->setSimple(true);
        $this->payload = $json;
    }
    
    private function resolvData(){
        if($this->simple){
            return $this->payload;
        }
        
        $this->loadMenu();
        
        $data = [
            'payload' => $this->payload,
            'menu' => $this->menu,
            'auth' => $this->Auth->getFrontendData()
        ];
        
        if(is_null($this->flash) === false){
            $data['flash'] = $this->flash;
        }
        
        if(is_null($this->redirect) === false){
            $data['redirect'] = $this->redirect;
        }
        
        if(is_null($this->exception) === false){
            $data['exception'] = $this->exception;
        }
        
        return $data;
    }
    
    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        return 'application/internet';
    }
    
    protected function getPayload() {
        return $this->payload;
    }
}