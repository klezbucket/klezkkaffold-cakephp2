<?php

App::uses('WebComponent', 'KlezBackend.Controller/Component');

class KlezkaffoldWebComponent extends WebComponent{
    private $Controller;
    
    public function initialize(\Controller $controller) {
        $this->Controller = $controller;
        parent::initialize($controller);
    }
    
    public function downloadReport($module){
        $this->setUrl($this->getApi('report'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->setData([
            'module' => $module
        ]);
        $this->jsonPayload();
        $this->fileDump('json');
    }
    
    public function report($module){
        $this->setUrl($this->getApi('report'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->setData([
            'module' => $module
        ]);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function dashboard($module){
        $this->setUrl($this->getApi('dashboard'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->setData([
            'module' => $module
        ]);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function show($module,$page = 0,array $extra = null){
        $payload = [];
        
        if(is_null($extra) === false){
            $payload = $payload + $extra;
        }
        
        $payload['page'] = $page;
        $this->setUrl($this->getApi('show'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->setData([
            'module' => $module,
            'payload' => $payload
        ]);
        
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function image($module,$id = null,$params = []){
        $this->setUrl($this->getApi('image'));
        $this->setVerb('GET');
        $this->cookieStorage();
        
        $payload = [
            'params' => $params,
            'cache' => [
                'If-Modified-Since' => @$_SERVER['HTTP_IF_MODIFIED_SINCE']
            ]
        ];
                
        if(is_null($id) === false){
            $payload['id'] = $id;
        }
        
        $this->setData([
            'module' => $module,
            'payload' => $payload
        ]);
        
        $this->jsonPayload();
        $this->fileDump('json');
    }
    
    
    public function profilePhoto($module,$params = []){
        $this->setUrl($this->getApi('profile_photo'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->setData([
            'module' => $module,
            'payload' => [
                'params' => $params,
                'cache' => [
                    'If-Modified-Since' => @$_SERVER['HTTP_IF_MODIFIED_SINCE']
                ]
            ]
        ]);
        
        $this->jsonPayload();
        $this->fileDump('json');
    }
    
    public function detail($module,$id = null){
        $payload = [];
        
        if(is_null($id) === false){
            $payload['id'] = $id;
        }
        
        $this->setUrl($this->getApi('detail'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->setData([
            'module' => $module,
            'payload' => $payload
        ]);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function delete($module,$data,$conf){
        $this->setUrl($this->getApi('delete'));
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData([
            'module' => $module,
            'payload' => [
                'data' => $data
            ]
        ]);
        
        $this->jsonPayload();
        $this->interceptRequest($conf);
        $this->feed('json');
    }
    
    public function add($module,$data,$conf){
        $this->setUrl($this->getApi('add'));
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData([
            'module' => $module,
            'payload' => [
                'data' => $data
            ]
        ]);
        
        $this->jsonPayload();
        $this->interceptRequest($conf);
        $this->feed('json');
    }
    
    public function massive($module,$data,$conf){
        $this->setUrl($this->getApi('massive'));
        $this->setVerb('POST');
        $this->cookieStorage();
        
        if(empty($data['formdata'])){
            $this->setVerb('PUT');
        }
        
        $this->setData([
            'module' => $module,
            'payload' => [
                'data' => $data
            ]
        ]);
        
        $this->jsonPayload();
        $this->interceptRequest($conf);
        
        $response = $this->getHttpOkResponse('json');
        
        if(empty($data['formdata'])){
            if($response['payload']['success']){
                $this->flashRewrite(sprintf($conf['confirm']['success'],$response['payload']['records']), 'success', 'json');
            }
            else{
                $this->flashRewrite($conf['confirm']['error'], 'error', 'json');
            }
        }
        else if(empty($response['payload']['massive'])){
            $this->flashRewrite($conf['messages']['empty'], 'warning', 'json');
        }
        
        $this->feed('json');
    }
    
    public function edit($module,$data,$conf){
        $this->setUrl($this->getApi('edit'));
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData([
            'module' => $module,
            'payload' => [
                'data' => $data
            ]
        ]);
        
        $this->jsonPayload();
        $this->interceptRequest($conf);
        $this->feed('json');
    }
    
    public function requestProfileForm(){
        $this->setUrl($this->getApi('profile'));
        $this->setVerb('GET');
        $this->cookieStorage();
        $this->setData([
            'module' => 'profile',
        ]);
        $this->jsonPayload();
        $this->feed('json');
    }
    
    private function resolvProfileEditData($formdata,$redir){
        if(is_null($redir)){
            $redir = [
                'controller' => 'backend',
                'action' => 'home'
            ];
        }
        
        $data = [
            'formdata' => $formdata,
            'id' => 0,
            'redirect' => $redir
        ];
        
        return $data;
    }
    
    private function resolvProfileEditIntercept($conf){
        if(is_null($conf)){
            $conf = [
                'messages' => [
                    'success' => 'Perfil editado con éxito',
                    'error' => 'No se pudo editar el Perfil'
                ]
            ];
        }
        
        return $conf;
    }
    
    public function profileEdit($formdata,$conf = null,$redir = null){
        $data = $this->resolvProfileEditData($formdata,$redir);
        $this->setUrl($this->getApi('profile_edit'));
        $this->setVerb('POST');
        $this->cookieStorage();
        $this->setData([
            'module' => 'profile',
            'payload' => [
                'data' => $data
            ]
        ]);
        
        $this->jsonPayload();
        $this->interceptRequest($this->resolvProfileEditIntercept($conf));
        $this->feed('json');
    }
    
    public function requestAutocompleteProfileForm($module){
        $this->setUrl($this->getApi('profile'));
        $this->setVerb('POST');
        $this->cookieStorage();
        
        $this->setData([
            'module' => $module,
            'payload' => [
                'submodule' => 'autocomplete',
                'data' => $_REQUEST
            ]
        ]);
        
        $this->jsonPayload();
        $this->dump('json');
    }
    
    public function requestAutocompleteForm($module,$extra = []){
        $this->setUrl($this->getApi('request_form'));
        $this->setVerb('POST');
        $this->cookieStorage();
        
        $this->setData([
            'module' => $module,
            'payload' => [
                'submodule' => 'autocomplete',
                'data' => $_REQUEST,
                'extra' => $extra
            ]
        ]);
        
        $this->jsonPayload();
        $this->dump('json');
    }
    
    public function requestMassive($module,$id = null,$extras = []){
        $this->setUrl($this->getApi('request_massive'));
        $this->setVerb('GET');
        $this->cookieStorage();
        
        $payload = $extras;
        
        if(is_null($id) === false){
            $payload['id'] = $id;
        }
        
        $this->setData([
            'module' => $module,
            'payload' => $payload
        ]);
        
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function requestForm($module,$id = null,$extras = []){
        $this->setUrl($this->getApi('request_form'));
        $this->setVerb('GET');
        $this->cookieStorage();
        
        $payload = $extras;
        
        if(is_null($id) === false){
            $payload['id'] = $id;
        }
        
        $this->setData([
            'module' => $module,
            'payload' => $payload
        ]);
        
        $this->jsonPayload();
        $this->feed('json');
    }
    
    public function requestDelete($module,$id = null,$extras = []){
        $this->setUrl($this->getApi('request_delete'));
        $this->setVerb('GET');
        $this->cookieStorage();
        
        $payload = $extras;
                
        if(is_null($id) === false){
            $payload['id'] = $id;
        }
        
        $this->setData([
            'module' => $module,
            'payload' => $payload
        ]);
        $this->jsonPayload();
        $this->feed('json');
    }
}
