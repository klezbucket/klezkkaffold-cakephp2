<?php

App::uses('KlezkaffoldHelper', 'Klezkaffold.View/Helper');

class ReportHelper extends KlezkaffoldHelper {
    public function deploy($config){
        echo $this->element('main',[
            'config' => $config,
        ]);
    }
    
    public function actionsHolder($config){
        if(isset($config['links']) === false){
            $config['links'] = [];
        }
        
        if(empty($config['links'])){
            return;
        }
        
        echo $this->element('actions', $config);
    }
    
    public function reportActions($links){
        $metalinks = $this->resolvMetalinks($links);
        
        if(empty($metalinks) === false){
            $this->dataLinks($metalinks);
        }
    }
    
    private function emptyData(){
        echo $this->element('empty');
    }
    
    public function main($config){
        if(isset($config['type']) === false){
            $this->raiseInternalServerError('Invalid Config<missing.key:type> in ReportHelper');
        }
        
        if(isset($config['data']) === false){
            $this->raiseInternalServerError('Invalid Config<missing.key:data> in ReportHelper');
        }
        
        if(isset($config['schema']) === false){
            $this->raiseInternalServerError('Invalid Config<missing.key:schema> in ReportHelper');
        }
        
        if(empty($config['data'])){
            $this->emptyData();
            return;
        }
        
        $type = $config['type'];
        $method = $type . 'Type';
        
        if(method_exists($this, $method) === false){
            $this->raiseInternalServerError("No Such Method<method:$method> in ReportHelper");
        }
        
        $this->{$method}($config);
    }
    
    private function tableType($config){
        echo $this->_View->Table->deploy($config);
    }

    public function provideLogtag() {
        return 'Report';
    }
}