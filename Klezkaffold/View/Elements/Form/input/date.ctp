<div class="input-group m-t-10">
    <input 
        class="form-control klez-datepicker" 
        placeholder="dd/mm/aaaa" 
        id="datepicker"
        value="<?=$value?>"
        name="<?=$field?>" />
    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
</div>

<script>
    $(function(){   
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
        };
        
        $('.klez-datepicker').datepicker({
            format: "dd/mm/yyyy",
            language : 'es',
            autoclose : true
        });
    });
</script>