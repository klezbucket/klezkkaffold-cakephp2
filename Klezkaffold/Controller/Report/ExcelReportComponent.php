<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');
App::uses('ExcelComponent','KlezData.Controller/DataDrivers');

class ExcelReportComponent extends KlezkaffoldComponent {
    private $Excel;
    private $sheets = [];
    private $fileName;
    private $styles = [];
    
    public function output() {
        $output = $this->Excel->output();
        $output['blob'] = base64_encode($output['blob']);
        $output['file'] = $this->fileName;
        
        return $output;
    }

    public function input($config,$payload = null) {
        $this->parseConfig($config);
        $this->loadExcel();
    }
    
    private function loadExcel(){
        $collection = new ComponentCollection();
        $this->Excel = new ExcelComponent($collection);
        $this->Excel->initialize(new Controller());
    }
    
    public function process(){
        $this->schema = [];
        $this->Excel->setStyles($this->styles);
        
        foreach($this->sheets as $i => $config){
            if(isset($config['name']) === false){
                $this->raiseConfigureException("No Conf <Sheet:name> in Klezkaffold Config");
            }

            if(isset($config['data']) === false){
                $this->raiseConfigureException("No Conf <Sheet:data> in Klezkaffold Config");
            }

            if(isset($config['data']['query']) === false){
                $this->raiseConfigureException("No Conf <Sheet:data.query> in Klezkaffold Config");
            }
            
            $this->loadModel($config['data']);
            $this->query = $config['data']['query'];
            
            if(isset($config['data']['prequery']) === true){
                $this->prequery = $config['data']['prequery'];
            }
            
            if(empty($this->prequery) === false){
                $this->prequeryProcess();
            }
            
            $schema = $this->getModel()->provideReadableSchema();
            $data = $this->getModel()->findReadable('all',$this->query,true);
            
            $this->Excel->newSheet();
            $this->Excel->sheet($config['name'],$i);
            $this->Excel->table($schema,$data);
        }
    }
    
    public function parseConfig($config){
        if(isset($config['excel']['styles']) === false){
            $this->raiseConfigureException("No Conf <Report:excel.styles> in Klezkaffold Config");
        }
        
        if(isset($config['download']['file']) === false){
            $this->raiseConfigureException("No Conf <Report:download.file> in Klezkaffold Config");
        }
        
        if(isset($config['excel']['sheets']) === false){
            $this->raiseConfigureException("No Conf <Report:excel.sheets> in Klezkaffold Config");
        }
        
        $this->sheets = $config['excel']['sheets'];
        $this->fileName = $config['download']['file'];
        $this->styles = $config['excel']['styles'];
    }
}