<?php

App::uses('BaseComponent', 'KlezApi.Controller/Component');
App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');

class ServerComponent extends BaseComponent{
    private $Params = [];
    private $Endpoint;
    private $Format;
    private $Config;
    private $Component;
    private $Request;
    private $Data;
    private $Controller;
    private $Method;
    
    public function initialize(\Controller $controller) {
        ob_start();
        
        parent::initialize($controller);
        $this->initializeLog();
        $this->Request = $controller->request;
        $this->Params = $controller->params;
        $this->Controller = $controller;
        
        $url = $controller->request->here;
        $this->logapi("Request<{$_SERVER['REQUEST_METHOD']}:{$url}>");
    }
    
    private function initializeLog(){
        $file = 'klezapi';
        
        CakeLog::config($file, array(
            'engine' => 'FileLog',
            'types' => [ 'debug' ],
            'scopes' => [ 'api' ],
            'file' => $file
        ));    
    }
    
    private function logapi($message){
        $fullMessage = "[{$this->getLogHash()}] {$message}";
        CakeLog::write('debug', $fullMessage, 'api');
    }
    
    private function raiseConfigureException($message){
        $this->logapi("Exception: '$message'");
        throw new ConfigureException($message);
    }
    
    public function load(){
        $this->loadConfig();
        $this->loadEnpoint();
        $this->parseConfig();
        $this->loadComponent();
    }
    
    public function dispatch(){
        
        try{
            $this->input();
            $this->process();
            $this->output();
        }
        catch (Exception $e){
            $message = $e->getMessage();
            error_log("Exception: '$message'");
            throw $e;
        }
    }
    
    private function parseConfig(){
        $endpoint = Inflector::camelize($this->Endpoint);
        
        if(isset($this->Config['component']) === false){
            $this->raiseConfigureException("No Conf<Endpoint.$endpoint.component> in Endpoint Config");
        }
        
        if(isset($this->Config['path']) === false){
            $this->raiseConfigureException("No Conf<Endpoint.$endpoint.path> in Endpoint Config");
        }
        
        if(isset($this->Config['method']) === false){
            $this->raiseConfigureException("No Conf<Endpoint.$endpoint.method> in Endpoint Config");
        }
        
        if(isset($this->Config['auth']) === false){
            $this->raiseConfigureException("No Conf<Endpoint.$endpoint.acl.auth> in Endpoint Config");
        }
        
        if(isset($this->Config['acl']) === false){
            $this->raiseConfigureException("No Conf<Endpoint.$endpoint.acl> in Endpoint Config");
        }
    }
    
    public function redirect($path,$component,$method){
        $this->logapi("Redirecting<path:{$path},component:{$component},method:{$method}()>");
        
        $this->Config['path'] = $path;
        $this->Config['component'] = $component;
        $this->Config['method'] = $method;
        $this->Controller->params['endpoint'] = $component;
        
        $this->parseConfig();
        $this->loadComponent();
    }
    
    private function loadComponent(){
        $component = $this->Config['component'];
        $path = $this->Config['path'];
        $method = $this->Config['method'];
        $format = $this->Format;
        
        App::uses($component, $path);
        $this->logapi("Component<class:{$component},method:{$method}()>");
        
        if(class_exists($component) === false){
            $this->raiseConfigureException("Not Found<Class:$component> in Endpoint Config");
        }
        
        $collection = new ComponentCollection();
        $this->Component = new $component($collection);
        $this->Component->initialize($this->Controller);
        
        if(($this->Component instanceof EndpointComponent) === false){
            $this->raiseConfigureException("Not an EndpointComponent<Class:$component> in Endpoint Config");
        }
        
        $this->resolvMethod();
        
        if(method_exists($this->Component,$format) === false){
            $this->raiseConfigureException("Format Method Not Found<Method:$component::$format()> in Endpoint Config");
        }
    }
    
    private function resolvMethod(){
        $http = $this->Request->method();
        $method = $this->Config['method'];
        $httpMethod = "{$method}_{$http}";
        
        if(method_exists($this->Component,$httpMethod)){
            $this->Method = $httpMethod;
            return;
        }
        
        if(method_exists($this->Component,$method)){
            $this->Method = $method;
            return;
        }
        
        $component = $this->Config['component'];
        $this->raiseConfigureException("Not Found<Method:$component::$httpMethod()> in Endpoint Config");
    }
    
    private function loadEnpoint(){
        $format = $this->Params['format'];
        $this->Format = $format;
        
        $this->Endpoint = $this->Params['endpoint'];
        $endpoint = Inflector::camelize($this->Endpoint);
        
        $key = "Endpoint.{$endpoint}";
        $this->Config = Configure::read($key);
        
        if(is_null($this->Config)){
            $this->raiseConfigureException("No Conf<Endpoint.$endpoint> in Endpoint Config");
        }
        
        $this->logapi("Endpoint<camelized:{$endpoint},format:{$format}>");
    }
    
    private function loadConfig(){
        Configure::load('klezendpoints','default',false);
        
        $confiles = Configure::read('Endpoint.confiles');
        
        if(is_null($confiles)){            
            $this->raiseConfigureException('No Conf<Endpoint:confiles> in Endpoint Config');
        }
        
        if(is_array($confiles) === false){
            $confiles = [ $confiles ];
        }
        
        foreach($confiles as $confile){
            Configure::load($confile,'default',false);
        }
    }
    
    private function input(){
        $this->filterVerb();
        
        if(is_null($this->Data) === true){
            $this->gatherData();
        }
    }
    
    public function gatherData(){
        $endpoint = Inflector::camelize($this->Endpoint);
        
        if(isset($this->Config['input']) === false){            
            $this->raiseConfigureException("No Conf<Endpoint.$endpoint.input> in Endpoint Config");
        }
        
        $input = Inflector::camelize($this->Config['input']);
        $gatherer = "gatherer{$input}";
        
        if(method_exists($this, $gatherer) === false){
            $this->raiseConfigureException("No Gatherer<Input:$input,ServerComponent:$gatherer()> in Endpoint Config");
        }
        
        $this->{$gatherer}();
    }
    
    private function gathererBlank(){
        $this->Data = null;
        $this->logapi("Input<blank>");
    }
    
    private function gathererJsonPayload(){
        $data = file_get_contents("php://input");
        $this->Data = json_decode($data,true);
        $this->logapi("Input<json:{$data}>");
    }
    
    private function filterVerb(){
        $endpoint = Inflector::camelize($this->Endpoint);
        
        if(isset($this->Config['verb']) === false){            
            $this->raiseConfigureException("No Conf<Endpoint.$endpoint.method> in Endpoint Config");
        }
        
        $method = strtolower($_SERVER['REQUEST_METHOD']);
        $verbs = $this->Config['verb'];
        $valid = false;
        
        if(is_array($verbs) === false){
            $verbs = [ $verbs ];
        }
        
        if(in_array($method, $verbs)){
            $valid = true;
        }
        
        if($valid === false){
            $this->raiseConfigureException("Invalid Http Method<{$method}> in Endpoint Config");
        }
    }
    
    private function process() {
        $this->Component->{$this->Method}($this->Data);
    }
    
    private function output($status = 200){
        $this->preoutput();
        $data = $this->Component->{$this->Format}();
        $type = $this->Component->getContentType($this->Format);
        $size = strlen($data);
        
        header('Content-Type:' . $type);
        header('Content-Length:' . $size);
        echo $data;
        
        if($this->isBinary($type)){
            $logdata = 'BINARY/NON-PRINTABLE';
        }
        else{
            $json = json_decode($data,true);
            
            if(isset($json['payload']['blob'])){
                $json['payload']['blob'] = '<MD5>' . md5($json['payload']['blob']);
            }
            
            $logdata = json_encode($json);
        }
        
        $this->logapi("Output<{$status}:{$type},size:{$size},{$logdata}>");
        http_response_code($status);
        $this->postoutput();
    }
    
    private function preoutput(){
        $buffer = ob_get_clean();  
        $this->bufferHeader($buffer);
        
        ob_end_clean();
        ob_start();
    }
    
    private function postoutput(){
        if(Configure::read('debug') > 0){
            $bob = microtime(true);
            header('X-Benchmark-Bob: ' . $bob);
        }
        
        ob_end_flush();
        
        exit;
    }
    
    public function notImplemented(Exception $e){
        $this->exceptionHandler($e,501);
    }
    
    public function forbidden(Exception $e){
        $this->exceptionHandler($e,403);
    }
    
    public function unathorized(Exception $e){
        $this->exceptionHandler($e,401);
    }
    
    public function badRequest(Exception $e){
        $this->exceptionHandler($e,400);
    }
    
    public function proxyStatusCode(Exception $e){
        $code = http_response_code();
        $this->exceptionHandler($e,$code);
    }
    
    public function internalServerError(Exception $e){
        $this->exceptionHandler($e,500);
    }
    
    private function exceptionHandler(Exception $e,$status){
        $this->preoutput();
        http_response_code($status);
        $this->exceptionHeader($e);
        $this->postoutput();
    }
    
    private function exceptionHeader(Exception $e){
        if(Configure::read('debug') > 0){
            header('X-KlezApi-Exception: ' . $e->getMessage());
        }
    }
    
    private function bufferHeader($buffer){
        $plainBuffer = strip_tags($buffer);
        $c = strlen($plainBuffer);
        $b = $plainBuffer;
        
        if($c === 0){
            return;
        }
        
        if($c > 100){
            $b = substr($plainBuffer,0,100);
        }
        
        if(Configure::read('debug') > 0){
            header('X-KlezApi-Buffer: ' . urlencode($b));
        }
        
        $this->logapi("Buffer<stream:{$plainBuffer}>");
    }
    
    public function getConfig(){
        return $this->Config;
    }
    
    function getComponent() {
        return $this->Component;
    }
    
    function getData() {
        return $this->Data;
    }
}