<?php

App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

class AddKlezkaffoldComponent extends KlezkaffoldComponent{
    private $success = false;
    private $schema = [];
    private $data = [];
    private $id = null;
    private $redirect = null;
    private $url;
    
    public function output() {
        $schema = $this->resolvFormSchema();
        
        return [
            'schema' => $schema,
            'data' => $this->getModel()->getData(),
            'id' => $this->id,
            'success' => $this->success,
            'messages' => $this->getModel()->validationErrors,
        ];
    }
    
    public function isSuccess() {
        return $this->success;
    }
    
    public function getRedirect(){
        return $this->redirect;
    }
    
    public function input($config,$payload = null) {
        $this->parseConfig($config);
        
        if(array_key_exists('formdata',$payload['data']) === false){
            $this->raiseBadRequestException("Invalid Payload <missing.key:data.formdata> in AddKlezkaffold");
        }
        
        if(array_key_exists('redirect',$payload['data']) === false){
            $this->raiseBadRequestException("Invalid Payload <missing.key:data.redirect> in AddKlezkaffold");
        }
        
        $this->url = $payload['data']['redirect'];
        $this->data = $this->formData($payload['data']['formdata']);
    }
    
    public function process(){
        $this->schema = $this->getModel()->provideWritableSchema();
        $this->getModel()->prepareForStore($this->data);
        
        if($this->getModel()->validateData() === false){
            $this->failureCallback();
            return;
        }
        
        if($this->saveData() === false){
            $this->failureCallback();
            return;
        }
        
        $this->successCallback();
    }
    
    private function successCallback(){
        $this->id = $this->getModel()->id;
        $this->success = true;
        $this->redirect = $this->resolvRedirect($this->getModel()->getData(), $this->url);
    }
    
    private function failureCallback(){
        $this->id = null;
        $this->success = false;
        $this->redirect = null;
    }
    
    public function parseConfig($config){
        $this->loadModel($config['data']);
    }
}