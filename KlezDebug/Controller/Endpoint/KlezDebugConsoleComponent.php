<?php

App::uses('BackendComponent', 'KlezBackendApi.Controller/Endpoint');
App::uses('Json', 'KlezJson.Model');

class KlezDebugConsoleComponent extends BackendComponent{
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
    }
    
    public function main($payload){
        Configure::load('klezdebug');
        $this->overwrite(Configure::read('KlezDebug'));
    }

    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        throw new NotImplementedException("Unknown format:$format");
    }
}