<?php

App::uses('KlezJsonAppModel', 'KlezJson.Model');

class Json extends KlezJsonAppModel{
    private $schema = [
        'created' => [
            'type' => 'datetime',
            'writable' => true,
            'readable' => true,
            'label' => 'Fecha Creación',
        ],
        'size' => [
            'type' => 'int',
            'writable' => true,
            'readable' => true,
            'label' => 'Tamaño',
        ],
        'etag' => [
            'type' => 'text',
            'writable' => true,
            'readable' => true,
            'label' => 'Hash MD5',
        ],
        'master' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Master',
        ],
        'administrator_id' => [
            'type' => 'boolean',
            'writable' => true,
            'readable' => true,
            'label' => 'Master',
        ],
    ];
    
    public function providePrimaryKeyMetadata() {
        return [
            'type' => 'int',
            'default' => 'Asignación Automática',
            'subtype' => 'primary',
            'label' => 'JSON-ID',
            'listable' => false,
            'readable' => false
        ];
    }

    public function provideSchema() {
        return $this->schema;
    }
    
    public function fetchMaster(){
        $cnd = [];
        $cnd['Json.master'] = true;
        $order = 'Json.created DESC';
        
        return $this->find('first',[
            'conditions' => $cnd,
            'order' => $order
        ]);
    }
    
    public function beforeGenerate(){
        return true;
    }
}