<script>
    var $FCM = {
        lines : [],
        root : null,
        value : null,
        node : null,
        container : null,
        plus : null,
        input : null,
        more : null,
        submit :  null,
        request : null,
        response : null,
        tab : 20,
        padding : function(deep){
            return (this.tab * (deep + 1)) + 'px';
        },
        start : function(){
            this.root = $('#fcmroot');
            this.value = $('#fcmvalue');
            this.node = $('#fcmnode');
            this.input = $('#fcminput');
            this.more = $('#fcmmore');
            this.submit = $('#fcmsubmit');
            this.request = $('#fcmrequest');
            this.response = $('#fcmresponse');
            this.container = this.root.find('.container');
            this.root.css('paddingLeft',this.padding(0));
            this.roll(this.container,this.lines,1);
            this.events();
        }, 
        events : function(){
            this.eventValues();
            this.eventLabels();
            this.eventSubmit();
        },
        eventSubmit(){
            this.submit.off('click').on('click',function(e){
                e.preventDefault();
                $('.fcmloading').modal('show');
                
                var requestContainer = $FCM.request.find('#fcmrequestcontainer');
                var responseContainer = $FCM.response.find('#fcmresponsecontainer');
                requestContainer.html('');
                responseContainer.html('');
                
                var data = $FCM.gatherData($FCM.container);

                $.ajax({
                    type: "POST",
                    data : {
                        payload : data,
                        serverkey : $('input[name="api_key"]').val()
                    },
                    url: <?=json_encode(Router::url(null,true))?>,
                    dataType : 'json'
                }).done(function(data) {
                    toastr["info"]("Push finalizado");
                    
                    $FCM.responseProcess(data.response);
                    $FCM.requestProcess(data.request);

                    toastr.options = {
                      "closeButton": false,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": true,
                    };
                }).error(function(data) {
                    $FCM.failure();
                }).always(function(data) {
                    $('.fcmloading').modal('hide');
                });
            });
        },
        failure : function(){
            toastr["error"]("Falló la ejecucion del Push");

            toastr.options = {
              "closeButton": false,
              "debug": false,
              "newestOnTop": false,
              "progressBar": false,
              "positionClass": "toast-top-right",
              "preventDuplicates": true
            };
        },
        requestProcess : function(data){
            var requestContainer = this.request.find('#fcmrequestcontainer');
            var code = $('<code></code>');
            code.html(data.verb + " " + data.url);
            requestContainer.append(code);
            requestContainer.append($('<br/>'));
                
            for(var i in data.headers){
                var code = $('<code></code>');
                code.html(i + ": " + data.headers[i]);
                
                requestContainer.append(code);
                requestContainer.append($('<br/>'));
            }
        
            requestContainer.append($('<br/>'));
            requestContainer.append($('<br/>'));
            
            var pre = $('<pre></pre>');
            var jsonPretty = JSON.stringify(data.payload,null,2);
            pre.html(jsonPretty);
            requestContainer.append(pre);
            this.request.show();
        },
        responseProcess : function(data){
            var responseContainer = this.response.find('#fcmresponsecontainer');
            var code = $('<code></code>');
            code.html(data.status);
            
            responseContainer.append(code);
            responseContainer.append($('<br/>'));
                
            for(var i in data.headers){
                var code = $('<code></code>');
                code.html(i + ": " + data.headers[i]);
                
                responseContainer.append(code);
                responseContainer.append($('<br/>'));
            }
        
            responseContainer.append($('<br/>'));
            responseContainer.append($('<br/>'));
            
            var pre = $('<pre></pre>');
            
            var jsonPretty = JSON.stringify(data.payload,null,2);
            pre.html(jsonPretty);
            responseContainer.append(pre);
            
            this.response.show();
            
            $('html,body').animate({ scrollTop: this.response.offset().top},'slow');
        },
        cleanString : function(str){
            return str.replace(/\"/g,'');
        },
        gatherData : function(container){
            var labels = container.children('.fcmline').children('.fcmlabel');
            var data = {};
            
            labels.each(function(){
                var label = $(this);
                var key = $FCM.cleanString(label.html().trim());
                
                if($FCM.isExpandible(label)){
                    var container = label.siblings('.container');
                    data[key] = $FCM.gatherData(container);
                }
                else{
                    var value = label.siblings('.fcmvalue');
                    var value = value.html();
                    
                    data[key] = $FCM.cleanString(value);
                }
            });
            
            return data;
        },
        isExpandible : function(label){
            var value = label.siblings('.fcmvalue');
            return value.length == 0;
        },
        eventValues : function(){
            $('.fcmvalue').css('cursor','pointer');
            $('.fcmvalue').off('click').on('click',function(e){
                e.preventDefault();
                
                var element = $(this);
                var input = $FCM.input.clone().removeAttr('id').show();
                element.after(input);
                element.hide();
                
                $FCM.eventInputValues(element,input);
                input.val($FCM.cleanString(element.html()));
                input.focus();
            });
        },
        eventLabels : function(){
            $('.fcmlabel').css('cursor','pointer');
            $('.fcmlabel').off('click').on('click',function(e){
                e.preventDefault();
                
                var element = $(this);
                var input = $FCM.input.clone().removeAttr('id').show();
                element.after(input);
                element.hide();
                
                $FCM.eventInputLabels(element,input);
                input.val($FCM.cleanString(element.html()));
                input.focus();
            });
        },
        eventInputLabels : function(element,input){
            input.off('keyup').on('keyup',function(e){
                if(e.keyCode === 13){
                    e.preventDefault();
                    $FCM.eventInputLabelsCallback(element,input);
                }

                return false;
            });
            
            input.off('blur').on('blur',function(e){
                e.preventDefault();
                $FCM.eventInputLabelsCallback(element,input);
                return false;
            });
        },
        eventInputLabelsCallback : function(element,input){
            element.html($FCM.cleanString(input.val()));
            element.show();
            input.remove();
        },
        eventInputValues : function(element,input){
            input.off('keyup').on('keyup',function(e){
                if(e.keyCode === 13){
                    e.preventDefault();
                    $FCM.eventInputValuesCallback(element,input);
                }

                return false;
            });
            
            input.off('blur').on('blur',function(e){
                e.preventDefault();
                $FCM.eventInputValuesCallback(element,input);
                return false;
            });
        },
        eventInputValuesCallback : function(element,input){
            element.html("\"" + $FCM.cleanString(input.val()) + "\"");
            element.show();
            input.remove();
        },
        roll : function(container,lines,deep){
            for(var i in lines){
                var line = lines[i];
                
                if(line.expandible){
                    this.expandible(container,line,deep);
                }
                else{
                    this.literal(container,line,deep);
                }
            }
            
            //this.remover(container);
            this.appender(container,1);
        },
        remover : function(container){
            var minus = container.siblings('.fcmdelete');
            
            minus.off('click').on('click',function(e){
                e.preventDefault();
                container.parent().remove();
            });
        },
        appender : function(container,deep){
            var plus = container.siblings('.fcmplus');
            
            plus.off('click').on('click',function(e){
                e.preventDefault();
                
                var container = $(this).siblings('.container');
                var more = container.children(".fcmmore");
                
                if(more.length === 0){
                    $FCM.appendMore(container,deep);
                }
                else{
                    more.remove();
                }
                
                return false;
            });
        },
        appendMore : function(container,deep){
            var more = this.more.clone().show().css('paddingLeft',this.padding(deep)).removeAttr('id');
            var input = more.find('input').show();
            this.appendMoreEvents(more,deep);
            container.append(more);
            input.focus();
        },
        appendMoreEvents : function(more,deep){
            var input = more.find('input');
            var select = more.find('select');  
            
            select.off('change').on('change',function(){
                input.focus();
            });
            
            input.off('keyup').on('keyup',function(e){
                if(e.keyCode === 13){
                    e.preventDefault();
                    $FCM.appendMoreInputEventCallback(more,deep);
                }

                return false;
            });
        },
        appendMoreInputEventCallback : function(more,deep){ 
            var input = more.find('input');
            var select = more.find('select');  
            var name = input.val();
            var container = more.parent();
            
            if(name !== ''){
                switch(select.val().toLowerCase()){
                    case 'nodo':
                        $FCM.expandible(container,{
                            name : name,
                            default : [
                                    
                            ],
                            expandible : true,
                        },deep);
                        break;
                    case 'literal':
                        $FCM.literal(container,{
                            name : name,
                            default : "",
                            expandible : false,
                        },deep);
                        break;
                }
                
                container.find('.fcmmore').remove();
                this.events();
            }
        },
        expandible : function(container,line,deep){
            var node = this.node.clone().show().css('paddingLeft',this.padding(deep));
            this.common(node,line,deep);
            container.append(node);
            node.find('.fcmlabel').html(line.name);
            this.remover(container);
            
            var container = node.find('.container');
            this.appender(container,deep);
            this.remover(container);
            this.roll(container,line.default,deep + 1);
        },
        literal : function(container,line,deep){
            var value = this.value.clone();
            this.common(value,line,deep);
            container.append(value);
            
            var fcmvalue = value.find('.fcmvalue');
            var fcmlabel = value.find('.fcmlabel');
            
            this.remover(fcmvalue);
            fcmlabel.html(line.name);
            fcmvalue.html('"' + line.default +'"');
        },
        common : function(element,line,deep){
            element.show().css('paddingLeft',this.padding(deep)).removeAttr('id');
        }
    };
    
    $(function(){
        $FCM.start();
    });
</script>

<style>
    .fcmlabel{
        display: inline;
    }
    
    .fcmvalue{
        display: inline;
        width:200px !important;
    }
    
    .fcmdelete{
        cursor:pointer;
    }
    
    .fcmline{
        display: none;
    }
    
    .fcmplus{
        cursor:pointer;
    }
    
    .fcminput{
        display:none;
        width:200px;
    }
</style>