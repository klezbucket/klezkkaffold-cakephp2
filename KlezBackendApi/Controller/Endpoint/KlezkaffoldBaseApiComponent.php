<?php

App::uses('BackendComponent', 'KlezBackendApi.Controller/Endpoint');
App::uses('KlezkaffoldComponent','Klezkaffold.Controller/Klezkaffold');

abstract class KlezkaffoldBaseApiComponent extends BackendComponent{
    private $Controller;
    protected $Klezkaffold;
    protected $Config;
    protected $Format;
    
    public function initialize(\Controller $controller) {
        parent::initialize($controller);
        $this->Controller = $controller;
        $this->Format = $controller->params['format'];
        $this->loadConfig();
    }
    
    private function loadConfig(){
        Configure::load('klezkaffold','default',false);
        
        $confiles = Configure::read('Klezkaffold.confiles');
        
        if(is_null($confiles)){       
            $this->raiseConfigureException('No Conf<Klezkaffold:confiles> in Klezkaffold Config');
        }
        
        if(is_array($confiles) === false){
            $confiles = [ $confiles ];
        }
        
        foreach($confiles as $confile){
            Configure::load($confile,'default',false);
        }
    }
    
    private $Payload = [];
    
    protected function start($data,$type){   
        if(isset($data['payload'])){
            $this->Payload = $data['payload'];
        }
        
        $this->parseModule($data);
        $module = $data['module'];
        $this->loadModule($module,$type);
        $this->loadKlezkaffold($type);
        
        if($this->scaffoldFilter($type, $data) === false){
            throw new ForbiddenException();
        }
    }
    
    private function raiseConfigureException($message){
        $this->logendpoint($message);
        throw new ConfigureException($message);
    }
    
    private function raiseBadRequestException($message){
        $this->logendpoint($message);
        throw new BadRequestException($message);
    }
    
    private function loadKlezkaffold($type){
        Configure::load('KlezBackendApi.klezkaffold_wrappers','default',false);
        $key = "KlezkaffoldWrapper.{$type}";
        
        $conf = Configure::read($key);
        
        if(is_null($conf)){
            $this->raiseConfigureException("No Conf<KlezkaffoldWrapper:{$type}> in KlezkaffoldWrapper Config");
        }
        
        if(isset($conf['component']) === false){
            $this->raiseConfigureException("No Conf<KlezkaffoldWrapper:{$type}.component> in KlezkaffoldWrapper Config");               
        }
        
        if(isset($conf['path']) === false){
            $this->raiseConfigureException("No Conf<KlezkaffoldWrapper:{$type}.path> in KlezkaffoldWrapper Config");    
        }
        
        $component = $conf['component'];
        $path = $conf['path'];
        
        App::uses($component, $path);
        
        if(class_exists($component) === false){
            $this->raiseConfigureException("No Class<$component> for KlezkaffoldApi");
        }
        
        $collection = new ComponentCollection();
        $this->Klezkaffold = new $component($collection);
        
        if(($this->Klezkaffold instanceof KlezkaffoldComponent) === false){
            $this->raiseConfigureException("Not an KlezkaffoldComponent<Class:$component> in KlezkaffoldWrapper Config");
        }
        
        $this->Klezkaffold->initialize($this->Controller);
        $this->Klezkaffold->setPayload($this->Payload);
    }
    
    private function parseModule($data){
        if(isset($data['module']) === false){
            $this->raiseBadRequestException('Invalid Json Payload <missing:module> in KlezkaffoldApi');
        }
    }
    
    private function loadModule($module,$type){
        $key = "Klezkaffold.{$type}.{$module}";
        $conf = Configure::read($key);
        
        if(is_null($conf)){
            $this->raiseConfigureException("No Conf <Klezkaffold:{$type}.{$module}> in Klezkaffold Config");
        }
        
        $this->Config = $conf;
    }
    
    public function scaffoldFilter($type,$input){
        $action = Inflector::camelize($type);
        $module = Inflector::underscore($input['module']);
        $key = "Klezkaffold.{$type}.{$module}";
        $scaffoldConf = Configure::read($key);
        
        if(is_null($scaffoldConf) === true){
            $this->logendpoint("No Scaffold Filter<method=$type,module=$module> in KlezkaffoldFilter");
            return true;
        }
        
//        if(isset($scaffoldConf['data']['class']) === false){
//            foreach($scaffoldConf as $conf){
//                if($this->modelJudgement($conf,$action,$input) === false){
//                    return false;
//                }
//            }
//            
//            return true;
//        }
        
        return $this->modelJudgement($scaffoldConf,$action,$input,$module);
    }
    
    private function modelJudgement($config,$action,$input,$module){
        $judgement = true;
        
        if($this->modelFilter($config,$input) === false){
            $judgement = false;
        }
        
        $actionFilter = $this->modelActionFilter($config,$action,$input);
        
        if(is_bool($actionFilter)){
            $judgement = $actionFilter;
        }
        
        $actionModuleFilter = $this->modelActionModuleFilter($config,$action,$input,$module);
        
        if(is_bool($actionModuleFilter)){
            $judgement = $actionModuleFilter;
        }
        
        if($judgement){
            $this->logendpoint("ACL<status:grant> in KlezkaffoldFilter ");
        }
        else{
            $this->logendpoint("ACL<status:forbidden> in KlezkaffoldFilter ");
        }
        
        return $judgement;
    }   
    
    private function modelActionModuleFilter($config,$action,$input,$module){
        if(isset($config['data']['class']) === false){
            $this->logendpoint("No Class Found<missing.key:data.class> in KlezkaffoldFilter");
            return null;
        }
        
        $module = Inflector::camelize($module);
        $class = $config['data']['class'];
        $key = "KlezkaffoldFilter.{$class}@$action::$module";
        $filterConf = Configure::read($key);
        
        if(is_null($filterConf) === true){
            $this->logendpoint("No ModelActionModule Filter<class=$class,action=$action,module=$module> in KlezkaffoldFilter");
            return null;
        }
                
        if($this->Acl->aclFiltersData($filterConf['filters']) === false){
            $this->logendpoint("ACL Filters Data <class=$class@$action::$module,status:forbidden> in KlezkaffoldFilter ");
            return false;
        }
        
        if($this->customFilters($config,$filterConf,$input) === false){
            return false;
        }
        
        $this->logendpoint("ACL Filters Data <class=$class@$action::$module,status:grant> in KlezkaffoldFilter ");
        return true;
    }
    
    private function modelActionFilter($config,$action,$input){
        if(isset($config['data']['class']) === false){
            $this->logendpoint("No Class Found<missing.key:data.class> in KlezkaffoldFilter");
            return null;
        }
        
        $class = $config['data']['class'];
        $key = "KlezkaffoldFilter.{$class}@$action";
        $filterConf = Configure::read($key);
        
        if(is_null($filterConf) === true){
            $this->logendpoint("No ModelAction Filter<class=$class,action=$action> in KlezkaffoldFilter");
            return null;
        }
                
        if($this->Acl->aclFiltersData($filterConf['filters']) === false){
            $this->logendpoint("ACL Filters Data <class=$class@$action,status:forbidden> in KlezkaffoldFilter ");
            return false;
        }
        
        if($this->customFilters($config,$filterConf,$input) === false){
            return false;
        }
        
        $this->logendpoint("ACL Filters Data <class=$class@$action,status:grant> in KlezkaffoldFilter ");
        return true;
    }
    
    private function modelFilter($config,$input){
        if(isset($config['data']['class']) === false){
            $this->logendpoint("No Class Found<missing.key:data.class> in KlezkaffoldFilter");
            return null;
        }
        
        $class = $config['data']['class'];
        $key = "KlezkaffoldFilter.{$class}";
        $filterConf = Configure::read($key);
        
        if(is_null($filterConf) === true){
            $this->logendpoint("No Model Filter<class=$class> in KlezkaffoldFilter");
            return null;
        }
        
        if($this->Acl->aclFiltersData($filterConf['filters']) === false){
            $this->logendpoint("ACL Filters Data <class=$class,status:forbidden> in KlezkaffoldFilter ");
            return false;
        }
        
        if($this->customFilters($config,$filterConf,$input) === false){
            return false;
        }
        
        $this->logendpoint("ACL Filters Data <class=$class,status:grant> in KlezkaffoldFilter ");
        return true;
    }
    
    private function customFilters($config,$filterConf,$input){
        if(isset($filterConf['filters']['custom']) === false){
            return null;
        }
        
        $custom = $filterConf['filters']['custom'];
        
        if(is_array($custom) === false){
            $custom = [ $custom ];
        }
        
        if(isset($config['data']['path']) === false){
            $this->logendpoint("Invalid Data Config<missing.key:data.path> in KlezkaffoldFilter");
            return null;
        }
        
        $class = $config['data']['class'];
        $path = $config['data']['path'];
        App::uses($class, $path);
        
        if(class_exists($class) === false){
            $this->logendpoint("No Such Model<class:$class> in KlezkaffoldFilter");
        }
        
        $Model = new $class();
        $authdata = $this->getAuth()->getData();
        
        foreach($custom as $method){
            if(method_exists($Model, $method) === false){
                $this->logendpoint("No Such Method<class:$class,method:$method> in KlezkaffoldFilter");
            }
            else if($Model->{$method}($authdata,$input) !== true){
                $this->logendpoint("ACL Custom Filters Data <class=$class,method=$method,status:forbidden> in KlezkaffoldFilter ");
                return false;
            }
        }
        
        $this->logendpoint("ACL Custom Filters Data <class=$class,method=$method,status:grant> in KlezkaffoldFilter ");
        return true;
    }
}