<?php

App::uses('BaseComponent', 'KlezApi.Controller/Component');

class DummyComponent extends BaseComponent{    
    function __construct($collection) {
        parent::__construct($collection);
    }
}