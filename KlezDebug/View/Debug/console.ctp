<?php $payload = $this->Api->feed('payload') ;?>
<script>
    var data = <?=  json_encode($payload); ?>;
    
    $(function(){
        var payloadContainer = $('#payload-container');
        var endpoint = $('#endpoint');
        var submit = $('#console-debug-submit');
        var response = $('#console-debug-response');
        var responseContainer = $('#console-debug-response-container');
        var request = $('#console-debug-request');
        var requestContainer = $('#console-debug-request-container');
        var requestFields = $('#request-fields');
        var server = $('#server');
        var basic = $('#basic');
        var storage = {};
        
        server.off('change').on('change',function(e){
            var i = $(this).val();
            
            if(typeof data.servers[i] !== 'undefined'){
                basic.val(data.servers[i].basic);
            }
        });
        
        submit.off('click').on('click',function(e){
            e.preventDefault();
            response.hide();
            responseContainer.html('');
            request.hide();
            requestContainer.html('');
            
            var json = { };
            var inputs = $('#console-debug-form').find('input, select');
            
            inputs.each(function(){
                var name = $(this).attr('name');
                var fieldName = name.replace(']','').split('[');
                var i = fieldName[0];
                var j = fieldName[1];
                
                if(typeof json[i] === 'undefined'){
                    json[i] = {};
                }
                
                json[i][j] = $(this).val();
            });
            
            json['request']['endpoint'] = data['endpoints'][json['request']['endpoint']]['url'];
            $('.console-loading').modal('show');

            $.ajax({
                type: "POST",
                data : json,
                url: <?=json_encode(Router::url(null,true))?>,
                dataType : 'json'
            }).done(function(data) {
                responseProcess(data.response);
                requestProcess(data.request);
                
                toastr["info"]("Request finalizado");

                toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": true,
                };
            }).error(function(data) {
                failure();
                
            }).always(function(data) {
                $('.console-loading').modal('hide');
            });
            
            return false;
        });
        
        function requestProcess(data){
            var code = $('<code></code>');
            code.html(data.verb + " " + data.url);
            requestContainer.append(code);
            requestContainer.append($('<br/>'));
                
            for(var i in data.headers){
                var code = $('<code></code>');
                code.html(i + ": " + data.headers[i]);
                
                requestContainer.append(code);
                requestContainer.append($('<br/>'));
            }
        
            requestContainer.append($('<br/>'));
            requestContainer.append($('<br/>'));
            
            var pre = $('<pre></pre>');
            var jsonPretty = JSON.stringify(data.payload,null,2);
            pre.html(jsonPretty);
            requestContainer.append(pre);
            request.show();
        }
        
        function responseProcess(data){
            var code = $('<code></code>');
            code.html(data.status);
            
            responseContainer.append(code);
            responseContainer.append($('<br/>'));
                
            for(var i in data.headers){
                var code = $('<code></code>');
                code.html(i + ": " + data.headers[i]);
                
                responseContainer.append(code);
                responseContainer.append($('<br/>'));
            }
        
            responseContainer.append($('<br/>'));
            responseContainer.append($('<br/>'));
            
            var pre = $('<pre></pre>');
            
            var jsonPretty = JSON.stringify(data.payload,null,2);
            pre.html(jsonPretty);
            responseContainer.append(pre);
            
            response.show();
            
            $('html,body').animate({ scrollTop: response.offset().top},'slow');
        }
        
        function failure(){
            toastr["error"]("Falló la ejecucion del Request");

            toastr.options = {
              "closeButton": false,
              "debug": false,
              "newestOnTop": false,
              "progressBar": false,
              "positionClass": "toast-top-right",
              "preventDuplicates": true
            };
        }
    
        endpoint.off('change').on('change',function(){
            requestFields.find('input').each(function(){
                var name = $(this).attr('name');
                storage[name] = $(this).val();
            });            
            
            payloadContainer.find('input').each(function(){
                var name = $(this).attr('name');
                storage[name] = $(this).val();
            });            
            
            requestFields.find('.request-header').remove();
            payloadContainer.html('');
            var i = endpoint.val();
            
            if(typeof data.endpoints[i] !== 'undefined'){
                var fields = data.endpoints[i].fields;
                
                if(typeof fields !== 'undefined'){
                    for(var field in fields){
                        var clone = $('#payload-field-sample').clone().appendTo('#payload-container');
                        var meta = fields[field];
                        var name = 'payload[' + field + ']';

                        if(typeof storage[name] !== 'undefined'){
                            clone.find('input').val(storage[name]);
                        }

                        clone.removeAttr('id');
                        clone.find('label').html(meta.label)
                        clone.find('input').attr('placeholder',meta.hint);
                        clone.find('input').attr('name',name);
                        clone.show();
                    }
                }
                
                var headers = data.endpoints[i].headers;
                
                if(typeof headers !== 'undefined'){
                    for(var header in headers){
                        var clone = $('#payload-field-sample').clone().appendTo('#request-fields');
                        var meta = headers[header];
                        var name = 'header[' + header + ']';
                        clone.addClass('request-header');

                        if(typeof storage[name] !== 'undefined'){
                            clone.find('input').val(storage[name]);
                        }

                        clone.removeAttr('id');
                        clone.find('label').html(meta.label);
                        clone.find('input').attr('placeholder',meta.hint);
                        clone.find('input').attr('name',name);
                        clone.show();
                    }
                }
            }
        });
        
        endpoint.trigger('change');
        server.trigger('change');
    });
</script>

<div class="col-sm-12">
    <form id='console-debug-form' class="form-horizontal" role="form">
        
        <div class="row">
            <div class='col-lg-6'>
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Request Builder</h4>

                    <div class="row">
                        <div class="col-lg-12" id='request-fields'>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Server</label>
                                <div class="col-sm-10">
                                    <select id='server' name="request[server]" class="form-control">
                                        <?php foreach($payload['servers'] as $server => $serverData){ ?>
                                            <option><?=$server?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Endpoint</label>

                                <div class="col-sm-10">
                                    <select id='endpoint' name="request[endpoint]" class="form-control">
                                        <?php foreach($payload['endpoints'] as $i => $endpoint){ ?>
                                            <option value='<?=$i?>'><?=$endpoint['url']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Verbo</label>

                                <div class="col-sm-10">
                                    <select name="request[verb]" class="form-control">
                                        <option>POST</option>
                                        <option>GET</option>
                                        <option>PUT</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Basic Auth</label>
                                <div class="col-sm-10">
                                    <input id='basic' class="form-control" name="auth[basic]" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class='col-lg-6'>
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Payload</h4>

                    <div class="row">
                        <div class="col-lg-12" id='payload-container'>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-4 control-label" style=""></label>

                        <div class="col-sm-8">
                            <button id='console-debug-submit' type="submit" class="btn btn-success waves-effect waves-light">EJECUTAR</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class='col-lg-6' id='console-debug-request' style='display:none;'>
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Request</h4>

                    <div class="row">
                        <div class="col-lg-12" id='console-debug-request-container'>

                        </div>
                    </div>
                </div>
            </div>


            <div class='col-lg-6' style='display:none;' id='console-debug-response'>
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">Response</h4>

                    <div class="row">
                        <div class="col-lg-12" id='console-debug-response-container'>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="form-group" style="display:none;" id='payload-field-sample'>
    <label class="col-sm-2 control-label" style=""></label>
    
    <div class="col-sm-10">
        <input class="form-control" name="" type="text">
    </div>
</div>

<div class="modal fade console-loading" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="json-loading" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <p align="center">
                    <i class="fa fa-4x fa-spin fa-spinner"></i><br/><br/>
                    Ejecutando Request...
                </p>
            </div>
        </div>
    </div>
</div>