<?php

App::uses('EndpointComponent', 'KlezApi.Controller/Endpoint');
App::uses('AuthComponent', 'KlezApi.Controller/Component');

class ConfigComponent extends EndpointComponent{
    private $data = [];
    private $Auth;
    
    public function initialize(\Controller $controller) {
        $this->Auth = $controller->Auth;
        
        parent::initialize($controller);
    }
    
    public function fetch(){
        $this->data['current_timestamp'] = time();
    }
    
    public function fetchLogin(){
        $this->data = $this->Auth->getConfig();
    }
    
    public function json(){
        return json_encode($this->data);
    }

    public function getContentType($format) {
        switch ($format){
            case 'json':
                return 'application/json';
        }
        
        return 'application/internet';
    }
}